# Nummerreeks Application

## Synopsis

The _Nummerreeks_ (number series) application reserves and prints numbers for collection
objects and storage locations. It guarantees that numbers are never dispensed more than
once for any given sub-collection (e.g. ZMA.MOLL),

## Developer Setup Instructions

### Create a New Label Template

1. Add a new constant to
   the [LayoutTemplate](src/main/java/nl/naturalis/bcd/model/LayoutTemplate.java) enum.
   The constant must be _appended_ to the existing enum constants, since they are saved to
   the database using their ordinal number.
2. Add a new constant to
   the [LabelUsage](src/main/java/nl/naturalis/bcd/model/LabelUsage.java) enum.
3. In [LayoutResource](src/main/java/nl/naturalis/bcd/resource/LayoutResource.java),
   [PrintSession](src/main/java/nl/naturalis/bcd/print/PrintSession.java) and
   [LayoutDataFactory](src/main/java/nl/naturalis/bcd/print/LayoutDataFactory.java),
   expand the `switch` statements with the new enum constant.
4. Now comes an awkward step. We have as yet no CREATE functionality for an entirely new
   label template. Use a tool like DBeaver or phpMyAdmin to copy an existing row in
   the `bcd_layout` table. Set the `template` column of the new row to the ordinal number
   of the new `LayoutTemplate` constant, and set the `labelUsage` column of the new row to
   the ordinal number of the new `LabelUsage` constant. Also provide a unique name for
   the `layoutName` column.
5. Restart the application. If you now go to http://localhost:8080/layout/0, the new label
   template should be present in the list box. Select it to edit the label configuration.
6. Create a copy of one of the sub-packages of `nl.naturalis.bcd.print`, each of which
   implements a separate label template. Choose the one that most closely resembles the
   new label template. For the rest you are on your own.

## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks). The
pipeline is configured to scan on leaked secrets.

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

- `pre-commit autoupdate`
- `pre-commit install`
