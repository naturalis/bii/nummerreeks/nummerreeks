#!/bin/bash

# Directory containing all jars. Adjust as appropriate
libdir='/home/ayco/git-repos/naturalis/nummerreeks/nummerreeks/make/dependencies'
# Path to bcd-config.yaml. Adjust as appropriate
cfgfile='/home/ayco/Projects/nummerreeks/nummerreeks-config.yaml'

java --version

java -cp "${libdir}/*" \
        '-Dfile.encoding=UTF-8' \
        'nl.naturalis.bcd.BcdServer' \
        'server' \
        $cfgfile
