#!/bin/sh

set -eu

TAG=$1
FULL_IMAGE_NAME="${CI_REGISTRY_IMAGE}:${TAG}"
CRTICAL=0

echo "Trivy: $(trivy --version)"
trivy clean --all --cache-dir .trivycache/ # cache cleanup is needed when scanning images with the same tags, it does not remove the database
trivy image --download-db-only --no-progress --cache-dir .trivycache/ # update vulnerabilities db

echo "Scanning docker image: ${FULL_IMAGE_NAME}"
# Builds report and puts it in the default workdir $CI_PROJECT_DIR, so `artifacts:` can take it from there
trivy image --exit-code 0 --no-progress --cache-dir .trivycache/ --format template --template "@/contrib/gitlab.tpl" --output "$CI_PROJECT_DIR/gl-container-scanning-report.json" "$FULL_IMAGE_NAME"
# Prints full report
trivy image --exit-code 0 --no-progress --cache-dir .trivycache/ "$FULL_IMAGE_NAME"
if ! trivy image --exit-code 1 --severity CRITICAL "$FULL_IMAGE_NAME"; then
    let "CRTICAL+=1"
    curl -i -X POST -H 'Content-Type: application/json' -d '{"text": "WARNING: docker image `${FULL_IMAGE_NAME}` contains CRITCAL vulnerabilities!"}' "${MM_WEBHOOK}"
fi

if [ "$CRTICAL" -eq "0" ]; then
    echo "No critical vulnerabilities were found"
fi
exit $CRTICAL
