package nl.naturalis.bcd.print;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static nl.naturalis.bcd.print.LabelUtil.*;

public class LabelUtilTest {

  @Test
  public void test00() {
    int i = Integer.MAX_VALUE;
    System.out.println("Integer.MAX_VALUE: " + i);
    System.out.println("Num digits:       " + String.valueOf(i).length());
    System.out.println("log10(Integer.MAX_VALUE): " + (int) Math.log10(i));
    assertEquals(String.valueOf(i).length(), 1 + (int) Math.log10(i));
    System.out.println("log10(1): " + (int) Math.log10(9));
  }

  @Test
  public void test00a() {
    // Hm, interesting, didn't know this doesn't generate an NPE
    System.out.println(">>> " + (null instanceof Boolean));
  }

  @Test
  @Disabled
  public void test01() {
    // Just to see how much precision matters when converting millimeters to points
    System.out.println(1 / (10 * 2.54f) * 72); // quodlibet library's calculation of points-per-mm
    System.out.println(72 / (10 * 2.54f)); // this should yield a more accurate value
    // At first face (from the sysouts) it doesn't seem so, but it _is_ true:
    BigDecimal bd0 = new BigDecimal(72d);
    BigDecimal bd1 = new BigDecimal(10 * 2.54);
    BigDecimal bd2 = bd0.divide(bd1, 55, RoundingMode.HALF_UP);
    System.out.println(bd2.toString());
    bd2 = BigDecimal.ONE.divide(bd1, 55, RoundingMode.HALF_UP);
    bd2 = bd2.multiply(bd0);
    System.out.println(bd2.toString());
  }

  @Test
  public void test02() {
    // Suddenly not sure whether I even understand operator precedence any longer
    System.out.println("XXX: " + (6D / 4D * 3D));
    System.out.println("XXX: " + (6D / (4D * 3D)));
  }

  /*
   * Make sure we don't have any non-linearities when calculating relative string widths.
   */
  @Test
  public void test03() {
    double f0 = LabelUtil.getAbsoluteMeasure(10000, 4);
    System.out.println("Font size  4: " + f0);
    double f1 = LabelUtil.getAbsoluteMeasure(10000, 8);
    System.out.println("Font size 8: " + f1);
    assertEquals(2 * f0, f1);
    double f2 = LabelUtil.getAbsoluteMeasure(10000, 12);
    System.out.println("Font size 12: " + f2);
    assertEquals(3 * f0, f2);
    double f3 = LabelUtil.getAbsoluteMeasure(10000, 15);
    System.out.println("Font size 15: " + f3);
    assertEquals(15f / 4f * f0, f3);
  }

  /*
   * That's really realy nice: it seems like all digits in the helvetica font (even '1') have
   * exactly the same width (contrary to 'l', 'i' and 'w' and 'W');
   */
  @Test
  @Disabled
  public void test12() throws IOException {
    PDFont font = PDType1Font.HELVETICA;
    for (int i = 0; i < 10; ++i) {
      System.out.println(i + ": " + font.getWidth(i + 48));
    }
    System.out.println(".: " + font.getWidth('.'));
    System.out.println("l: " + font.getWidth('l'));
    System.out.println("i: " + font.getWidth('i'));
    System.out.println("w: " + font.getWidth('w'));
    System.out.println("W: " + font.getWidth('W'));
  }

  @Test
  public void test13() {
    // StringInputStream, which we use, is an ancient class, that still takes a
    // string for the encoding
    assertEquals("UTF-8", StandardCharsets.UTF_8.toString());
  }

  @Test
  public void test14() {
    BigDecimal bd = BigDecimal.ONE.divide(new BigDecimal("2.54"), 6, RoundingMode.HALF_DOWN);
    System.out.println(bd.toPlainString());
    System.out.println(bd.doubleValue());
    System.out.println(1D / 2.54D);
  }

  @Test
  public void test15() throws IOException {
    double boxHeight = LABEL_FONT.getBoundingBox().getHeight();
    double ascent = LABEL_FONT.getFontDescriptor().getAscent();
    double capHeight = LABEL_FONT.getFontDescriptor().getCapHeight();
    double descent = -LABEL_FONT.getFontDescriptor().getDescent();
    System.out.println("XXX bounding box height: " + boxHeight);
    System.out.println("XXX              ascent: " + ascent);
    System.out.println("XXX          cap height: " + capHeight);
    System.out.println("XXX             descent: " + descent);
  }

  @Test
  public void test016() {
    String s =
        "Beste collega s  Graag 3000 BE-nummers voor gebruik in BRD-CRUS. S.v.p. reserveren en registreren in BRD.  Alvast dank Sabinav";
    System.out.println(">>>>>>>>>>> " + s.length());
  }
}
