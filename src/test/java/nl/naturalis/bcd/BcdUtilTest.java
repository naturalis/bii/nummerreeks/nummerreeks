package nl.naturalis.bcd;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class BcdUtilTest {

  @Test
  public void extractTopDeskId00() {
    String topdeskId = "M12345";
    assertEquals(topdeskId, BcdUtil.extractTopDeskId(topdeskId));
  }

  @Test
  public void extractTopDeskId01() {
    String topdeskId = "X12345";
    assertEquals("INVALID:X12345", BcdUtil.extractTopDeskId(topdeskId));
  }

  @Test
  public void extractTopDeskId02() {
    String topdeskId = "M12345 (1)";
    assertEquals("M12345", BcdUtil.extractTopDeskId(topdeskId));
  }

  @Test
  public void extractTopDeskId03() {
    String topdeskId = "M12345 (13)";
    assertEquals("M12345", BcdUtil.extractTopDeskId(topdeskId));
  }

  @Test
  public void isValidTopDeskId00() {
    assertTrue(BcdUtil.isValidTopDeskId("M12345"));
  }

  @Test
  public void isValidTopDeskId01() {
    assertFalse(BcdUtil.isValidTopDeskId("M"));
  }

  @Test
  public void isValidTopDeskId02() {
    assertTrue(BcdUtil.isValidTopDeskId("M12345 (1)"));
  }

  @Test
  public void isValidTopDeskId03() {
    assertFalse(BcdUtil.isValidTopDeskId("M12345(1)"));
  }

  @Test
  public void isValidTopDeskId04() {
    assertFalse(BcdUtil.isValidTopDeskId("M12345 1"));
  }

  @Test
  public void testAddPathToUrl() {
    assertEquals("http://localhost:8080/logoff", BcdUtil.addPathToUrl("http://localhost:8080", "logoff"));
    assertEquals("http://localhost:8080/logoff", BcdUtil.addPathToUrl("http://localhost:8080/", "logoff"));
    assertEquals("http://localhost:8080/logoff", BcdUtil.addPathToUrl("http://localhost:8080/", "/logoff"));
    assertEquals("http://localhost:8080/", BcdUtil.addPathToUrl("http://localhost:8080", "/"));
    assertThrows(IllegalArgumentException.class, () -> BcdUtil.addPathToUrl(null, "str"));
    assertThrows(IllegalArgumentException.class, () -> BcdUtil.addPathToUrl("str", null));
    assertThrows(IllegalArgumentException.class, () -> BcdUtil.addPathToUrl("", "str"));
    assertThrows(IllegalArgumentException.class, () -> BcdUtil.addPathToUrl("str", ""));
  }
}
