select 
  a.Topdesknummer 
, b.InstitutionCode 
, b.CollectionCode 
, a.Eerstenummerreeks
, a.Laatstenummerreeks 
, a.Hoeveelheid 
, c.IndienerNaam
, d.AfdelingNaamstraat 
, e.Omgevingnaam 
, a.DatumIndiening 
, a.DatumUitvoering 
from nbc_nummerregistratie a
left join nbc_prefix b on (a.PrefixID = b.PrefixID) 
left join nbc_indiener c on (a.Indienercode = c.Indienercode)
left join nbc_afdelingstraat d on (a.Afdelingcode = d.Afdelingcode)
left join nbc_omgeving e on (a.OmgevingID = e.OmgevingID)

