"use strict";

/***********************************************/
/************** SEARCH AND PAGING **************/
/***********************************************/

// Used by all list pages.

function curPageNo() {
	return SEARCH_REQUEST.pageNo === undefined? 0 : SEARCH_REQUEST.pageNo;
}

// Copies data from the SEARCH_REQUEST to the search box
// and possibly other form controls that contribute to
// the search being executed (like the "My requests only"
// checkbox)
function readSearchRequest() {
	$(".search-input").each(function() {
		const elem = $(this);
		const name = elem.attr("name");
		if(isCheckBox(elem)) {
			elem.prop("checked", SEARCH_REQUEST[name]);
		} else {
			elem.val(SEARCH_REQUEST[name]);
		}
	});
}

// Copies form controls back to SEARCH_REQUEST.
function updateSearchRequest() {
	$(".search-input").each(function() {
		const elem = $(this);
		const name = elem.attr("name");
		if(name === "pageNo" || name === "pageSize") {
			return;
		} else if(isCheckBox(elem)) {
			SEARCH_REQUEST[name] = elem.prop("checked");
		} else {
			const val = elem.val().trim();
			if(val.length === 0) {
				delete SEARCH_REQUEST[name];
			} else {
				SEARCH_REQUEST[name] = elem.val();
			}
		}
	});
}

function firstPage() {
	if(curPageNo() == 0) {
		showWarning("Op eerste pagina");
	} else {
		delete SEARCH_REQUEST.pageNo;
		searchSimple();
	}
}

function prevPage() {
	if(curPageNo() == 0) {
		showWarning("Op eerste pagina");
	} else {
		SEARCH_REQUEST.pageNo = curPageNo() - 1;
		if(SEARCH_REQUEST.pageNo == 0) {
			delete SEARCH_REQUEST.pageNo;
		}
		searchSimple();
	}
}

function nextPage() {
	if(curPageNo() === SEARCH_REQUEST.lastPage) {
		showWarning("Op laatste pagina");
	} else {
		SEARCH_REQUEST.pageNo = curPageNo() + 1;
		searchSimple();
	}
}

function lastPage() {
	if(curPageNo() === SEARCH_REQUEST.lastPage) {
		showWarning("Op laatste pagina");
	} else {
		SEARCH_REQUEST.pageNo = SEARCH_REQUEST.lastPage;
		searchSimple();
	}
}

function showPage(pageNo) {
	SEARCH_REQUEST.pageNo = pageNo;
	searchSimple();
}

function searchSimple() {

	// If the search criteria have changed, we are going to reset
	// the page number to 0 (first page).
	
	const oldSearch = JSON.parse(JSON.stringify(SEARCH_REQUEST));	
	updateSearchRequest();	
	const newSearch = JSON.parse(JSON.stringify(SEARCH_REQUEST));	
	let changed = false;	
	for(const prop in oldSearch) {
		// Don't use !== for the property comparison b/c  updateSearchRequest
		// copies <select> values back as strings, even if they were originally
		// ints (e.g. IDs)
		if(prop !== "pageNo" && oldSearch[prop] != newSearch[prop]) {
			changed = true;
			break;
		}
	}	
	if(changed) {
		SEARCH_REQUEST.pageNo = 0;
	}	
	const encoded = encodeURIComponent(JSON.stringify(SEARCH_REQUEST));
	goto("?searchRequest=" + encoded);
}
