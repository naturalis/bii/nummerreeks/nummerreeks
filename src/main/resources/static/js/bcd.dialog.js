// noinspection JSUnusedGlobalSymbols

"use strict";

/*********************************/
/* Modal dialogs using Bootstrap */
/*********************************/

// See also /html/include/BSDialog.html

function bsHideDialog() {
	$("#bsDialog").modal('hide');
}

function bsDialog(message, borderColor, okAction) {
	const okButton = $("#bsDialogOK");
	okButton.off("click");
	if(typeof okAction === "function") {
		okButton.click(okAction);
	} else {
		okButton.click(bsHideDialog);
	}
	$("#bsDialogCancel").css("display", "none");
	$("#bsDialogContent").css("border", "3px solid " + borderColor);
	$("#bsDialogBody").html(message);
	$("#bsDialog").modal();
}

function showInfo(message, okAction) {
	bsDialog(message, "#1266F1", okAction);
}

function showSuccess(message, okAction) {
	bsDialog(message, "#00B74A", okAction);
}

function showWarning(message, okAction) {
	bsDialog(message, "#FFA900", okAction);
}

function showError(message, okAction) {
	bsDialog(message, "#F93154", okAction);
}

function bsConfirmDialog(message, onConfirm, borderColor) {
	const okButton = $("#bsDialogOK");
	okButton.off("click");
	if(typeof onConfirm === "function") {
		okButton.click(onConfirm);
	} else {
		okButton.click(bsHideDialog);
	}
	const msgContainer = $("#bsDialogBody");
	if(message instanceof jQuery) {
		msgContainer.empty();
		message.appendTo(msgContainer);
	} else {
		msgContainer.html(message);
	}
	$("#bsDialogCancel").css("display", "inline");
	$("#bsDialogContent").css("border", "3px solid " + borderColor);
	$("#bsDialog").modal();
}

function bsConfirm(message, onConfirm) {
	bsConfirmDialog(message, onConfirm, "#1266F1");
}

function bsConfirmWarning(message, onConfirm) {
	bsConfirmDialog(message, onConfirm, "#FFA900");
}

function bsConfirmError(message, onConfirm) {
	bsConfirmDialog(message, onConfirm, "#F93154");
}
