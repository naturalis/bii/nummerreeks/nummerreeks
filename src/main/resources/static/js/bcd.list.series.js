"use strict";

function deleteSeries(id, description) {
	const msg = "Nummerreeks " + description + " definitief verwijderen?";
	bsConfirmWarning(msg, function() {
		$.ajax({
			url: BASE_PATH + id,
			method: "DELETE",
			success: function(req) {
				searchSimple();
			}
		});
	});
}