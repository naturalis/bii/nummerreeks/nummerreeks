"use strict";

/******************************************************/
/************** SHARED BCD FUNCTIONALITY **************/
/******************************************************/

let formFields = null;

function getFormFields() {
	if(formFields === null) {
		const array = [];
		$("#input-container input,select,textarea").each(function() {
			array.push($(this));
		});
		formFields = array;
	}
	return formFields;
}

function action(relativePath) {
	const rp = (typeof relativePath === "number")? relativePath.toString() : relativePath;
	const x = rp.indexOf("?");
	goto(BASE_PATH + rp + getSearchRequestQueryParam(x === -1));
}

function showEditPage(id) {
	action(id + "/edit");
}

function showListPage() {
	action("");
}

// Only applicable for number series (requests) and storage locations
function showViewPage(id) {
	action(id);
}

/**
 * Checks if there is a SEARCH_REQUEST variable floating around and, if so,
 * returns it as a query parameter. However, the SEARCH_REQUEST variable
 * _must_ be a string, which it will be in the edit pages and edit-ish
 * pages. In the list pages it will be a JSON object. If the SEARCH_REQUEST
 * is a string, it has no use for the page itself. It's only there to be
 * ping-ponged between the client and the server, so that when the user
 * returns to the list page his/her latest search criteria can be applied
 * again.
 */
function getSearchRequestQueryParam(isFirstQueryParam) {
	const start = isFirstQueryParam ? "?" : "&";
	if(typeof SEARCH_REQUEST === "string") {
		return start + "searchRequest=" + encodeURIComponent(SEARCH_REQUEST);
	} else if(typeof SEARCH_REQUEST === "object") {
		return start + "searchRequest=" + encodeURIComponent(JSON.stringify(SEARCH_REQUEST));
	}
	return "";
}

// Show number series for a particular entity (e.g. a department or a cms).
// Used in all list pages except of course the list page for number series
// itself.
function showRequests(entityId,entityNameOrCode,instituteCode) {
	if(instituteCode !== undefined) {
		// Then we are called from the Collections list page
		if(entityNameOrCode === "N/A" || entityNameOrCode === instituteCode) {
			entityNameOrCode = instituteCode;
		} else {
			entityNameOrCode = instituteCode + "." + entityNameOrCode;
		}
	}
	goto(BASE_PATH + "show-requests/"
		+ "?id=" + entityId
	+ "&name=" + encodeURIComponent(entityNameOrCode));
}

// Initializations required for all list pages.
function initListPage() {

	const h1 = $("h1");
	recenter(h1);
	$(window).resize(function() {
		recenter(h1);
	});

	const simpleSearch = $("#simpleSearchTextInput");
	// Match height of search input to search button
	simpleSearch.outerHeight($("#simpleSearchButton").outerHeight());
	// Search on ENTER in search box
	simpleSearch.keypress(function(event) {
		if(event.which === KEY_ENTER) {
			searchSimple();
		}
	});

	readSearchRequest();

	const rowCount = SEARCH_REQUEST.rowCount;
	if(rowCount === 1) {
		$('#row-count-display').text("1 zoekresultaat");
	} else {
		$('#row-count-display').text(rowCount + " zoekresultaten");
	}

	$('button[curPage="true"]').each(function() {
		$(this).removeClass('btn-outline-primary').addClass('btn-primary');
	});

	$(".th-main").each(function() {
		const th = $(this);
		if(th.attr("onclick") === undefined) { // Allow local switch-off
			th.mouseenter(function() {
				th.css("cursor", "pointer").css("color", "#007bff");
			});
			th.mouseleave(function() {
				th.css("cursor", "auto").css("color", "#000");
			});
			makeSortable(th);
		}
	});

}

function makeSortable(th) {
	const mySortCol = th.attr("data-sortcol");
	if(mySortCol === SEARCH_REQUEST.sortColumn) {
		if(SEARCH_REQUEST.sortOrder === "DESC") {
			$("#sortDesc").appendTo(th).css("display", "inline");
		} else {
			$("#sortAsc").appendTo(th).css("display", "inline");
		}
	}
	th.click(function() {
		SEARCH_REQUEST.pageNo = 0;
		if(mySortCol === SEARCH_REQUEST.sortColumn) {
			if(SEARCH_REQUEST.sortOrder === "DESC") {
				SEARCH_REQUEST.sortOrder = "ASC";
			} else {
				SEARCH_REQUEST.sortOrder = "DESC";
			}
		} else {
			SEARCH_REQUEST.sortColumn = mySortCol;
			delete SEARCH_REQUEST.sortOrder;
		}
		searchSimple();
	});
}

// Initializations required for all edit pages.
function initEditPage() {
	const h1 = $("h1");
	recenter(h1);
	$(window).resize(function() {
		recenter(h1);
	});
	readModel();
	// Add keyup/change listeners that undo red coloration of input
	// controls that must not be left empty by the user, but were.		
	$("#input-container input").keyup(function() {
		const elem = $(this);
		if(!isCheckBox(elem) && elem.val() !== "") {
			elem.css("border", "1px solid #000").css("backgroundColor", "#fff")
		}
	});

	$("#input-container select").change(function() {
		const elem = $(this);
		if(elem.val() !== "") {
			elem.css("border", "1px solid #000").css("backgroundColor", "#fff")
		}
	});
	// Hide the search box
	$("#simpleSearchTextInput").css("display", "none");	
	$("#simpleSearchButton").css("display", "none");	
}

// Extra initializations for all list pages except for the series list page.
function initAdminListPage(entityName) {
	$(".main-table-row").each(function() {
		const tr = $(this);
		const requestCount = parseInt(tr.attr("data-request-count"));
		
		let button = tr.find(".show-requests-button");
		if(requestCount === 0) {
			button.prop("disabled", true);
		}
		
		const td = tr.find(".active-column");
		td.text(td.text()==="1"? "Ja" : "Nee");
		
		button = tr.find(".delete-button").first();
		const span1 = button.children("span:first-child");
		const span2 = button.children("span:last-child");
		if(requestCount === 0) {
			button.attr("title", entityName + " verwijderen");
			span1.css("display", "inline");
		} else {
			button.attr("title", entityName + " op non-actief zetten");
			span2.css("display", "inline");
			const active = parseInt(button.attr("data-active"));
			if(active === 0) {
				button.prop("disabled", true);
			}
		}
	});
}

// Copies data from the model object to the form fields.
function readModel() {
	if(typeof MODEL_BEAN === "undefined") {
		// Some pages in the app (very few) don't have their form fields
		// populated from a globally defined JSON object named "MODEL_BEAN".
		return;
	}
	const skip = arguments === undefined? [] : Array.from(arguments);
	for(const elem of getFormFields()) {
		const name = elem.attr("name");
		if(!skip.includes(name) && !isNull(MODEL_BEAN[name])) {
			if(isCheckBox(elem)) {
				if(MODEL_BEAN[name] !== true && MODEL_BEAN[name] !== false) {
					alert("BUG! Checkbox *must* be backed by boolean property");
				}
				elem.prop("checked", MODEL_BEAN[name]);
			} else if (isSelectRoleElement(elem)) {
				elem.val(MODEL_BEAN.role).change();
			} else {
				const v = e2n(MODEL_BEAN[name]);
				if(v !== null) {
					elem.val(v);
				}
			}
		}
	}
}

// Checks whether any changes have been made by the user.
function isDirty() {
	if(typeof MODEL_BEAN === "undefined") {
		alert("BUG: dirty check not possible without MODEL_BEAN object");
		return false;
	}
	const skip = arguments === undefined? [] : Array.from(arguments);
	for(const elem of getFormFields()) {
		const name = elem.attr("name");
		if(skip.includes(name)) {
			continue;
		} else if(isCheckBox(elem)) {
			const checked = elem.prop("checked");
			if(isNull(MODEL_BEAN[name]) && checked) {
				return true;
			} else if(MODEL_BEAN[name] !== checked) {
				return true;
			}
		} else if(isRadio(elem)) {
			if(elem.prop("checked") && MODEL_BEAN[name] !== elem.val()) {
				return true;
			}
		} else if(isFileInput(elem) && !isEmpty(elem)) {
			return true;
		} else if(e2n(MODEL_BEAN[name]) !== e2n(elem)) {
			return true;
		}
	}
	return false;
}

// Copies form data back to the model object and keeps track of the number of
// properties that were changed by the user.
function updateModel() {
	// function replacer(match,offset,string) {
	// 	return "[" + match.charCodeAt(0) + "]";
	// }
	const skip = arguments === undefined? [] : Array.from(arguments);
	let counter = 0;
	for(const elem of getFormFields()) {
		const name = elem.attr("name");
		if(skip.includes(name)) {
			continue;
		} else if(isCheckBox(elem)) {
			const checked = elem.prop("checked");
			if(isNull(MODEL_BEAN[name]) && checked) {
				++counter;
				MODEL_BEAN[name] = true;
			} else if(MODEL_BEAN[name] !== checked) {
				++counter;
				MODEL_BEAN[name] = checked;
			}
		} else if(isRadio(elem)) {
			if(elem.prop("checked")) {
				if(MODEL_BEAN[name] !== elem.val()) {
					++counter;
					MODEL_BEAN[name] = elem.val();
				}
			}
		} else {
			const oldVal = e2n(MODEL_BEAN[name])
			const newVal = e2n(elem);
			if(oldVal !== newVal) {
				++counter;
				if(newVal === null) {
					delete MODEL_BEAN[name];
				} else {
					MODEL_BEAN[name] = newVal;
				}
			}
		}
	}
	return counter;
}

/**
 * Ensures all required fields contain a non-empty value. You can pass in
 * an arbitrary amount of arguments, which are taken to be the names of
 * <select> and <input> elements that are **NOT** required, or <textarea>
 * elements that **ARE** required.
 */
function checkRequiredFields() {
	const args = arguments === undefined? [] : Array.from(arguments);
	let counter = 0;
	for(const elem of getFormFields()) {
		const name = elem.attr("name");
		if(elem.is("textarea") && !args.includes(name)) {
			continue;
		} else if(!elem.is("textarea") && args.includes(name)) {
			continue
		} else if(elem.prop("disabled")) {
			continue;
		} else if(isCheckBox(elem)) {
			continue;
		} else if(isRadio(elem)) {
			continue;
		} else if(elem.attr("type") === "hidden") {
			continue;
		} else if(e2n(elem) === null) {
			elem.css("border", "1px solid #f00").css("backgroundColor", "#fdd");
			++counter;
		}
	}
	return counter;
}

function saveOrUpdate() {
	const entity = encodeURIComponent(JSON.stringify(MODEL_BEAN));
	action("save-or-update/?entity=" + entity);
}

function deleteRecord(type, id, name) {
	let msg;
	if(name === undefined) {
		msg = type + " definitief verwijderen?";
	} else {
		 msg = type + " " + name + " definitief verwijderen?";
	}
	if(type === "Instituut") {
		 msg += " Hiermee worden ook alle collecties van dit instituut definitief verwijderd.";
	}
	bsConfirmWarning(msg, function() {
		$.ajax({
			url: BASE_PATH + id,
			method: "DELETE",
			success: function(req) {
				if(typeof searchSimple === "function") {
					// Then we find ourselves in a list page with a ready-made
					// search request that we shouldn't waste
					searchSimple();
				} else {
					// We find ourselves in an edit page and must return to the
					// list page (since the record has been removed from under
					// our feet)
					if(typeof onRecordDeleted === "function") {
						onRecordDeleted();
					} else {
						showListPage();
					}
				}
			}
		});
	});
}

function deactivate(type, id, name, active) {	
	if(!active) {
		showWarning(name + " is al gedeactiveerd");
	} else {
		const msg = type + " " + name + " op non-actief zetten?";
		bsConfirm(msg, function() {
			$.ajax({
				url: BASE_PATH + id + "/deactivate",
				method: "POST",
				success: function(req) {
					searchSimple();
				}
			});
		});
	}
}

function deleteOrDeactivate(type, id, name, active, requestCount) {
	if(requestCount === 0) {
		deleteRecord(type, id, name);
	} else {
		deactivate(type, id, name, active);
	}
}


// Number Series list and view page only:
function printSeries(requestId, dispenseCount, dateFirstClosed) {
	if(dispenseCount !== 0) {
	 		bsConfirmWarning("Pas op! Deze nummerreeks is al eens uitgegeven.  "
	 			+ "Aantal uitgiftes: " + dispenseCount
	 			+ ". Datum eerste uitgifte: " + dateFirstClosed
	 			+ ". Nogmaals printen?",
	 			function() {
	 				doPrintSeries(requestId);
	 			});
	} else {
		doPrintSeries(requestId);
	}
}

function doPrintSeries(requestId) {
	$.ajax({
		url: "/layout/options/?inputType=NUMBER_SERIES",
		method: "GET",
		success: function(htmlOptions) {
			printLabels(requestId, htmlOptions);
		}		
	});
}

// Storage Location list and view page only:
function printStorage(storageLocationId) {
	$.ajax({
		url: "/layout/options/?inputType=STORAGE_LOCATION",
		method: "GET",
		success: function(htmlOptions) {
			printLabels(storageLocationId, htmlOptions);
		}		
	});
}

// Not sure whether, as with Java, variable referenced within
// closure needs to be effectively final, but that's why it
// looks this way.
const LAYOUT_REF = {
	"value" : -1
};

function printLabels(requestId, htmlOptions) {
	const listbox = '<select id="layout" onchange="LAYOUT_REF.value=parseInt(this.value);">'
		+ htmlOptions
		+ '</select>';		
	const message = "<b>Selecteer een formaat:</b>&nbsp;" + listbox;
	bsConfirm(message, function() {
		bsHideDialog();
		if(LAYOUT_REF.value === -1) {
			showError("Selecteer eerst een formaat", function() {
				printSeries(requestId);
			});
		} else {
			action(requestId + "/print/" + LAYOUT_REF.value);
		}
	});
}




