/**
 * Save button click handler.
 */
function prepareSaveOrUpdate() {
	let emptyCount;
	if(isEmpty($("input[name=storageLocationId]"))) {
		emptyCount = checkRequiredFields();
	} else {
		// When updating an existing record the file input field is
		// no longer a required field
		emptyCount = checkRequiredFields("file");
	}
	if(emptyCount !== 0) {
		showError("Niet alle verplichte velden zijn ingevuld");
	} else if(updateModel() === 0) {
		showWarning("Formulier nog niet ingevuld / gewijzigd");
	} else {
		validateAndSave();
	}
}

function validateAndSave() {
	$.ajax({
	    type: "POST",
	    url: BASE_PATH + "unique",
	    data: JSON.stringify(MODEL_BEAN),
	    contentType: "application/json; charset=utf-8",
	    dataType: "json",
	    success: function(isUnique) {
			if(isUnique) {
				$("#form0").submit();
			} else {
				showError("Dit TOPDesk nummer bestaat al");
			}
	    }
	});
}

function lockStorageLocation(id) {
 	// Don't check file input field
	if(isDirty("file")) {
		showError("Wijzigingen moeten eerst worden opgeslagen");
	} else {
		action(id + "/lock");
	}
}

