/**
 * Save button click handler.
 */
function prepareSaveOrUpdate() {
	const emptyCount = checkRequiredFields("collectionCode");
	if(emptyCount !== 0) {
		showError("Niet alle verplichte velden zijn ingevuld");
	} else {
		const changedCount = updateModel();
		if(changedCount === 0) {
			showWarning("Formulier nog niet ingevuld / gewijzigd");
		} else {
			validateAndSave();
		}
	}
};

function validateAndSave() {
	$.ajax({
	    type: "POST",
	    url: BASE_PATH + "unique",
	    data: JSON.stringify(MODEL_BEAN),
	    contentType: "application/json; charset=utf-8",
	    dataType: "json",
	    success: function(isUnique) {
			if(isUnique) {
				saveOrUpdate("Collectie");
			} else {
				showError("Deze collectie bestaat al");
			}
	    }
	});
};

