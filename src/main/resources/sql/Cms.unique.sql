SELECT COUNT(*)
  FROM bcd_cms
 WHERE UPPER(cmsName) = UPPER(:cmsName)
   AND (ISNULL(:cmsId) OR cmsId != :cmsId)