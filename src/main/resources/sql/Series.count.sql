SELECT COUNT(*)
  FROM bcd_request a
  JOIN bcd_collection b ON a.collectionId = b.collectionId
  JOIN bcd_institute c ON b.instituteId = c.instituteId
  JOIN bcd_requester d ON a.requesterId = d.requesterId
  JOIN bcd_department e ON a.departmentId = e.departmentId
  ~%cmsJoin%
  ~%whereClause%