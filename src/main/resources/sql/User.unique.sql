SELECT COUNT(*)
FROM bcd_user
WHERE LOWER(upn) = LOWER(:upn)
AND (ISNULL(:userId) OR userId != :userId);
