INSERT INTO bcd_layout
   SET layoutName   = :layoutName,
       template     = :template,
       inputType    = :inputType,
       labelUsage   = :labelUsage,
       dateModified = :dateModified,
       importedFrom = :importedFrom,
       jsonConfig   = :jsonConfig
