SELECT a.*, b.requesterName
  FROM bcd_storage_location a
  LEFT JOIN bcd_requester b ON (a.requesterId = b.requesterId)
 WHERE storageLocationId = :id