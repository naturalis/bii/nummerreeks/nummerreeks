SELECT a.*, b.requesterName
  FROM bcd_storage_location a
  LEFT JOIN bcd_requester b ON (a.requesterId = b.requesterId)
 WHERE ISNULL(:normalizedSearch)
    OR INSTR(UPPER(a.prefixes), :normalizedSearch) != 0
    OR INSTR(UPPER(a.topDeskId), :normalizedSearch) != 0
    OR INSTR(UPPER(b.requesterName), :normalizedSearch) != 0
    OR INSTR(UPPER(a.fileName), :normalizedSearch) != 0
 ORDER BY ~%sortColumn% ~%sortOrder%
 LIMIT :offset, :limit
