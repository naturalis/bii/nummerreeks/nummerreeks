INSERT INTO bcd_request
   SET topDeskId         = :topDeskId,
       requesterId       = :requesterId,
       departmentId      = :departmentId,
       firstNumber       = :firstNumber,
       lastNumber        = :lastNumber,
       amount            = :amount,
       comments          = :comments,
       collectionId      = :collectionId,
       cmsId             = :cmsId,
       status            = :status,
       dispenseCount     = :dispenseCount,
       dateCreated       = :dateCreated,
       dateModified      = :dateModified,
       modifiedBy        = :modifiedBy
	
	