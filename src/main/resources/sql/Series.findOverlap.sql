SELECT a.topDeskId, a.firstNumber, a.lastNumber, b.collectionCode, c.instituteCode
  FROM bcd_request a
  JOIN bcd_collection b ON (a.collectionId = b.collectionId)
  JOIN bcd_institute c ON (b.instituteId = c.instituteId)
 WHERE a.collectionId = :collectionId
   AND a.firstNumber <= :lastNumber
   AND a.lastNumber >= :firstNumber 
   AND (ISNULL(:requestId) OR a.requestId != :requestId)