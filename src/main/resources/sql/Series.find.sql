SELECT a.*
,	b.instituteId
,	b.collectionCode
,	c.instituteCode
FROM bcd_request a
JOIN bcd_collection b ON a.collectionId = b.collectionId
JOIN bcd_institute c ON b.instituteId = c.instituteId
WHERE a.requestId = :id