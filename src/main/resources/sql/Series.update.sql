UPDATE bcd_request
   SET requesterId       = :requesterId,
       departmentId      = :departmentId,
       amount            = :amount,
       firstNumber       = :firstNumber,
       lastNumber        = :lastNumber,
       comments          = :comments,
       status            = :status,
       collectionId      = :collectionId,
       cmsId             = :cmsId,
       topDeskId         = :topDeskId,
       dateModified      = :dateModified,
       modifiedBy        = :modifiedBy,
       dateFirstClosed   = :dateFirstClosed,
       lastUsedLayout    = :lastUsedLayout,       
       dispenseCount     = :dispenseCount
WHERE requestId = :requestId