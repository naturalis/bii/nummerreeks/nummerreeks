SELECT COUNT(*)
  FROM bcd_storage_location
 WHERE topDeskId = UPPER(:topDeskId)
   AND (ISNULL(:storageLocationId) OR storageLocationId != :storageLocationId)