SELECT a.*,
       COUNT(c.requestId) as requestCount,
       IFNULL(MAX(c.lastNumber), 0) as maxNumber,
       IFNULL(SUM(c.amount), 0) as numberCount
  FROM bcd_institute a
  LEFT JOIN bcd_collection b ON a.instituteId = b.instituteId
  LEFT JOIN bcd_request c ON b.collectionId = c.collectionId 
 WHERE ISNULL(:normalizedSearch)
    OR INSTR(UPPER(a.instituteCode), :normalizedSearch) != 0
    OR INSTR(UPPER(a.instituteName), :normalizedSearch) != 0
 GROUP BY a.instituteId
 ORDER BY ~%sortColumn% ~%sortOrder%
 LIMIT :offset, :limit
