SELECT IFNULL(MAX(a.lastNumber), 0) + 1
  FROM bcd_request a
 WHERE a.collectionId = :collectionId
   AND (ISNULL(:firstNumber) OR a.lastNumber < :firstNumber)