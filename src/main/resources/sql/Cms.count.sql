SELECT COUNT(*)
  FROM bcd_cms
 WHERE ISNULL(:normalizedSearch) OR INSTR(UPPER(cmsName), :normalizedSearch) != 0
