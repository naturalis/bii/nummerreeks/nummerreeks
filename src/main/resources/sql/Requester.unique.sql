SELECT COUNT(*)
  FROM bcd_requester
 WHERE UPPER(requesterName) = UPPER(:requesterName)
   AND (ISNULL(:requesterId) OR requesterId != :requesterId)