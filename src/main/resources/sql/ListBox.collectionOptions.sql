SELECT collectionId AS optionValue,
       IFNULL(collectionCode, 'n.v.t.') AS optionText,
       active AS 'data-active'
  FROM bcd_collection
 WHERE instituteId = :instituteId
   AND (active = 1 OR collectionId = :collectionId)
