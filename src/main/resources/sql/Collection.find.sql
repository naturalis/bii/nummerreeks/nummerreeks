SELECT a.*, b.instituteCode
  FROM bcd_collection a
  JOIN bcd_institute b ON a.instituteId = b.instituteId
 WHERE a.collectionId = :id