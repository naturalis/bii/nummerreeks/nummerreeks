INSERT INTO bcd_user SET
	upn  	    = :upn,
	firstName   = :firstName,
	lastName    = :lastName,
	role		= :role,
	active      = :active
