SELECT COUNT(*)
FROM bcd_institute
WHERE (UPPER(instituteCode) = UPPER(:instituteCode) OR UPPER(instituteName) = UPPER(:instituteName))
AND (ISNULL(:instituteId) OR instituteId != :instituteId)