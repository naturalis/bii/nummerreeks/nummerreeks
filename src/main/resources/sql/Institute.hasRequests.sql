SELECT COUNT(*)
FROM bcd_request a
JOIN bcd_collection b ON a.collectionId = b.collectionId
WHERE b.instituteId = :id
