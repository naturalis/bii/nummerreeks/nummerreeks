SELECT layoutId AS optionValue,
       layoutName AS optionText,
       labelUsage AS optgroup
  FROM bcd_layout
 WHERE ISNULL(:inputType) OR inputType = :inputType
 ORDER BY labelUsage, layoutName