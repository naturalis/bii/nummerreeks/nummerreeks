SELECT *
  FROM bcd_print_config
 WHERE layout = :layout
   AND (collectionId = 0 OR collectionId = :collectionId)