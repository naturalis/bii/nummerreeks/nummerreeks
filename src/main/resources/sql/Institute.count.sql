SELECT COUNT(*)
  FROM bcd_institute a
 WHERE ISNULL(:normalizedSearch)
    OR INSTR(UPPER(a.instituteCode), :normalizedSearch) != 0
    OR INSTR(UPPER(a.instituteName), :normalizedSearch) != 0
