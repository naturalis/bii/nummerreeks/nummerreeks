SELECT COUNT(*)
  FROM bcd_request
 WHERE topDeskId = UPPER(:topDeskId)
   AND (ISNULL(:requestId) OR requestId != :requestId)