SELECT a.cmsId
,	a.cmsName
,	a.active
,	COUNT(b.requestId) as requestCount
,	IFNULL(SUM(b.amount), 0) as numberCount
FROM bcd_cms a
LEFT JOIN bcd_request b ON a.cmsId = b.cmsId
WHERE ISNULL(:normalizedSearch) OR INSTR(UPPER(cmsName), :normalizedSearch) != 0
GROUP BY a.cmsId
ORDER BY ~%sortColumn% ~%sortOrder%
LIMIT :offset, :limit