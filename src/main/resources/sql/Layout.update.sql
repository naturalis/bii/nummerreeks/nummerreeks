UPDATE bcd_layout
   SET jsonConfig = :jsonConfig,
       dateModified = :dateModified,
       importedFrom = :importedFrom
 WHERE layoutId = :layoutId