UPDATE bcd_institute SET
	instituteName  	= :instituteName,
	instituteCode  	= :instituteCode,
	active      	= :active
WHERE instituteId = :instituteId