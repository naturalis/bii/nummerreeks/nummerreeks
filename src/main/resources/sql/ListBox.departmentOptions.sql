SELECT departmentId AS optionValue,
       departmentName AS optionText,
       active AS 'data-active'
  FROM bcd_department
 WHERE active = 1 or departmentId = :id
 ORDER BY departmentName