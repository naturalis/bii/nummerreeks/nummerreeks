UPDATE bcd_user SET
    firstName   = :firstName,
    lastName    = :lastName,
    upn         = :upn,
	role		= :role,
	active		= :active
WHERE userId    = :userId