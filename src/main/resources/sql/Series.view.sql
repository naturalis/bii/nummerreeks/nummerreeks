SELECT a.*,
       c.requesterName,
       d.departmentName,
       e.cmsName,
       f.collectionCode,
       g.instituteCode,
       h.userName
  FROM bcd_request a
  LEFT JOIN bcd_requester c ON a.requesterId = c.requesterId
  LEFT JOIN bcd_department d ON a.departmentId = d.departmentId
  LEFT JOIN bcd_cms e ON a.cmsId = e.cmsId
  LEFT JOIN bcd_collection f ON a.collectionId = f.collectionId
  LEFT JOIN bcd_institute g ON f.instituteId = g.instituteId
  LEFT JOIN bcd_user h ON a.modifiedBy = h.userId
 WHERE a.requestId = :id