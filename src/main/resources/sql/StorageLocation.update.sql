UPDATE bcd_storage_location SET
       topDeskId          = :topDeskId,
       fileName           = IFNULL(:fileName,fileName),
       requesterId        = :requesterId,
       prefixSegmentCount = :prefixSegmentCount,
       prefixes           = :prefixes,
       status             = :status,
       comments           = :comments,
       dateCreated        = :dateCreated,
       dateModified       = :dateModified,
       dateFirstClosed    = :dateFirstClosed,
       modifiedBy         = :modifiedBy,
       lastUsedLayout     = :lastUsedLayout,
       dispenseCount      = :dispenseCount
 WHERE storageLocationId = :storageLocationId
