INSERT INTO bcd_storage_location SET
     topDeskId          = :topDeskId
,    fileName           = :fileName
,    requesterId        = :requesterId
,    prefixes           = :prefixes
,    prefixSegmentCount = :prefixSegmentCount
,    status             = :status
,    comments           = :comments
,    dateCreated        = :dateCreated
,    dateModified       = :dateModified
,    modifiedBy         = :modifiedBy