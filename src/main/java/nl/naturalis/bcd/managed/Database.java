package nl.naturalis.bcd.managed;

import static nl.naturalis.common.StringMethods.isNotBlank;
import static nl.naturalis.common.check.Check.fail;
import static nl.naturalis.common.check.CommonChecks.NULL;
import static nl.naturalis.common.check.CommonChecks.illegalState;
import static nl.naturalis.common.check.CommonChecks.notNull;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import io.dropwizard.lifecycle.Managed;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Properties;

import nl.naturalis.bcd.DbConfig;
import nl.naturalis.bcd.exception.BcdConfigurationException;
import nl.naturalis.bcd.model.LabelUsage;
import nl.naturalis.bcd.model.Layout;
import nl.naturalis.bcd.model.LayoutInputType;
import nl.naturalis.bcd.model.LayoutTemplate;
import nl.naturalis.common.check.Check;
import org.klojang.db.SQL;
import org.klojang.db.SQLQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Connection pool, managed (started/stopped) by DropWizard.
 */
public class Database implements Managed {

  private static final Logger LOG = LoggerFactory.getLogger(Database.class);
  private static final String ERR_NOT_READY = "Database not ready yet";

  private static Database INSTANCE;

  public static Database instance() {
    return Check.on(illegalState(), INSTANCE).is(notNull(), ERR_NOT_READY).ok();
  }

  public static HikariDataSource pool() {
    Check.on(illegalState(), INSTANCE).is(notNull(), ERR_NOT_READY);
    return Check.on(illegalState(), INSTANCE.pool).is(notNull(), ERR_NOT_READY).ok();
  }

  public static void ping() throws SQLException {
    try (Connection conn = pool().getConnection()) {
      try (PreparedStatement stmt = conn.prepareStatement("SELECT 42")) {
        stmt.setQueryTimeout(5);
        try (ResultSet rs = stmt.executeQuery()) {
          rs.next();
        }
      }
    }
  }

  /**
   * Creates and returns a new, ad-hoc JDBC connection, not retrieved from the connection
   * pool. The connection is not managed. You must close it yourself.
   *
   * @return
   * @throws SQLException
   * @throws BcdConfigurationException
   */
  public static Connection connect() throws SQLException {
    Check.on(illegalState(), INSTANCE).is(notNull(), ERR_NOT_READY);
    Properties props = new Properties();
    if (isNotBlank(INSTANCE.config.user)) {
      props.put("user", INSTANCE.config.user);
    }
    if (isNotBlank(INSTANCE.config.password)) {
      props.put("password", INSTANCE.config.password);
    }
    try {
      return DriverManager.getConnection(INSTANCE.config.getJdbcUrl(), props);
    } catch (BcdConfigurationException e) {
      return fail(illegalState(), e.getMessage());
    }
  }

  /**
   * Sets up a fully "manual" connection. Used to connect to legacy database when
   * migrating.
   */
  public static Connection connect(String description, DbConfig config)
        throws SQLException, BcdConfigurationException {
    Check.notNull(description, "description");
    Check.notNull(config, "Missing configuration for " + description);
    Properties props = new Properties();
    if (isNotBlank(config.user)) {
      props.put("user", config.user);
    }
    if (isNotBlank(config.password)) {
      props.put("password", config.password);
    }
    return DriverManager.getConnection(config.getJdbcUrl(), props);
  }

  private final DbConfig config;

  private HikariDataSource pool;

  public Database(DbConfig config) {
    Check.on(illegalState(), INSTANCE).is(NULL(), "Database already initialized");
    INSTANCE = this;
    this.config = config;
  }

  @Override
  public void start() throws Exception {
    Check.on(illegalState(), pool).is(NULL());
    HikariConfig hikariConf = new HikariConfig();
    hikariConf.setJdbcUrl(config.getJdbcUrl());
    hikariConf.setUsername(config.user);
    hikariConf.setPassword(config.password);
    hikariConf.setReadOnly(false);
    hikariConf.setAutoCommit(true);
    hikariConf.setMaximumPoolSize(5);
    hikariConf.addDataSourceProperty("cachePrepStmts", "true");
    hikariConf.addDataSourceProperty("prepStmtCacheSize", "250");
    hikariConf.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
    hikariConf.addDataSourceProperty("useServerPrepStmts", "true");
    hikariConf.addDataSourceProperty("useLocalSessionState", "true");
    hikariConf.addDataSourceProperty("rewriteBatchedStatements", "true");
    hikariConf.addDataSourceProperty("cacheResultSetMetadata", "true");
    hikariConf.addDataSourceProperty("cacheServerConfiguration", "true");
    hikariConf.addDataSourceProperty("elideSetAutoCommits", "true");
    hikariConf.addDataSourceProperty("maintainTimeStats", "false");
    HikariDataSource pool = null;
    for (int i = 0; i < 5; ++i) {
      try {
        LOG.info("Attempt {} of 5 to initialize connection pool ...", (i + 1));
        Thread.sleep(i == 0 ? 1500 : 3000);
        pool = new HikariDataSource(hikariConf);
        break;
      } catch (Exception e) {
        LOG.info("Attempt failed: {}", e.getMessage());
      }
    }
    if (pool == null) {
      LOG.error("Failed to initialize connection pool");
      throw new Exception("Failed to initialize connection pool");
    }
    this.pool = pool;
    hackNewLabelTemplate(pool);
    LOG.info("Database ready");
  }

  @Override
  public void stop() throws Exception {
    if (INSTANCE != null) {
      try {
        if (pool != null) {
          LOG.info("Closing database");
          pool.close();
        }
      } finally {
        INSTANCE = null;
      }
    }
  }


  // This is bad stuff. It inserts the (at the time of writing) new label template
  // that allows for some free text along the bottom of the label. This is because
  // we have no CREATE (i.e. INSERT) functionality for label templates yet.
  private static void hackNewLabelTemplate(HikariDataSource pool) throws SQLException {
    LOG.warn("Applying hack to check for, and if necessary insert stdplus label template");
    try (Connection con = pool.getConnection()) {
      // 3 is the ordinal number of LayoutTemplate.STD_PLUS enum constant. Do check this!
      // (see model package)
      String sql = "SELECT COUNT(*) FROM bcd_layout WHERE template = 3";
      SQLQuery query = SQL.create(sql).prepareQuery(con);
      int count = query.getInt();
      if (count > 0) {
        return;
      }
      LOG.warn("Inserting STD_PLUS label template");
      String jsonConfig = """
            {
              "paperSize" : "A4",
              "printAreaTop" : 10,
              "printAreaLeft" : 18,
              "rowCount" : 10,
              "columnCount" : 4,
              "columnWise" : true,
              "rowHeight" : 25,
              "labelHeight" : 23,
              "columnWidth" : 48,
              "labelWidth" : 46,
              "imgSize" : 12,
              "textWidth" : 30,
              "glueAll" : false,
              "gluePrefix" : true,
              "numberEmphasis" : 50,
              "freeText" : "(c) Ayco Holleman 2024",
              "freeTextMarginTop" : 0,
              "freeTextFontSize" : 4
            }
            """;
      Layout layout = new Layout();
      layout.setLayoutName("Entomologie (Voorbeeld Ayco)");
      layout.setTemplate(LayoutTemplate.STD_PLUS);
      layout.setInputType(LayoutInputType.NUMBER_SERIES);
      layout.setLabelUsage(LabelUsage.ENTOMOLOGY);
      layout.setDateModified(LocalDateTime.now());
      layout.setJsonConfig(jsonConfig);
      SQL.prepareInsert()
            .of(Layout.class)
            .into("bcd_layout")
            .excluding("layoutId", "importedFrom")
            .prepare(con)
            .bind(layout)
            .execute();
    }
  }


}
