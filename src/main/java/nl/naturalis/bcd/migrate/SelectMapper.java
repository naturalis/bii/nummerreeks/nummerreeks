package nl.naturalis.bcd.migrate;

import org.klojang.template.NameMapper;

class SelectMapper implements NameMapper {

  public SelectMapper() {}

  @Override
  public String map(String name) {
    return Character.toLowerCase(name.charAt(0)) + name.substring(1);
  }
}
