package nl.naturalis.bcd.migrate;

import java.sql.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.dropwizard.cli.ConfiguredCommand;
import io.dropwizard.setup.Bootstrap;
import net.sourceforge.argparse4j.inf.Namespace;
import nl.naturalis.bcd.BcdConfig;
import nl.naturalis.bcd.BcdServer;
import nl.naturalis.bcd.managed.Database;

/**
 * Main class of this package. Registered with {@link BcdServer}.
 *
 * @author Ayco Holleman
 */
public class MigrateDbCommand extends ConfiguredCommand<BcdConfig> {

  private static final Logger LOG = LoggerFactory.getLogger(MigrateDbCommand.class);

  public MigrateDbCommand() {
    super("migrate-db", "Migrates the legacy database");
  }

  @Override
  protected void run(Bootstrap<BcdConfig> bootstrap, Namespace namespace, BcdConfig cfg)
      throws Exception {
    LOG.info("Database migratie gestart");
    try {
      LOG.info("Verbinding met V1 database maken ...");
      Connection srcCon = Database.connect("legacy database", cfg.legacy);
      LOG.info("Verbonden");
      LOG.info("Verbinding met V2 database maken ...");
      Connection trgCon = Database.connect("target database", cfg.database);
      LOG.info("Verbonden");
      DepartmentMigrator departmentMigrator = new DepartmentMigrator(srcCon, trgCon);
      departmentMigrator.run();
      CmsMigrator cmsMigrator = new CmsMigrator(srcCon, trgCon);
      cmsMigrator.run();
      RequesterMigrator requesterMigrator = new RequesterMigrator(srcCon, trgCon);
      requesterMigrator.run();
      InstituteMigrator instituteMigrator = new InstituteMigrator(srcCon, trgCon);
      instituteMigrator.run();
      CollectionMigrator collectionMigrator = new CollectionMigrator(srcCon, trgCon);
      collectionMigrator.run();
      SeriesMigrator seriesMigrator =
          new SeriesMigrator(
              srcCon,
              trgCon,
              cmsMigrator,
              departmentMigrator,
              requesterMigrator,
              collectionMigrator);
      seriesMigrator.run();
      LOG.info("Klaar!");
    } catch (AbortException e) {
      LOG.error(e.getMessage());
    } catch (Exception e) {
      LOG.error("FATAL ERROR", e);
    }
  }
}
