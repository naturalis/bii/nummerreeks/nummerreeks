package nl.naturalis.bcd.migrate;

import nl.naturalis.bcd.migrate.model.NbcIndiener;
import nl.naturalis.bcd.model.Requester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class RequesterMigrator extends AbstractMigrator<NbcIndiener, Requester> {

  private static final Logger LOG = LoggerFactory.getLogger(RequesterMigrator.class);

  private Map<String, Integer> nameToId = new HashMap<>();
  private Map<String, Integer> codeToId = new HashMap<>();

  public RequesterMigrator(Connection srcCon, Connection trgCon) {
    super(srcCon, trgCon, NbcIndiener.class, Requester.class);
  }

  @Override
  String sourceTable() {
    return "nbc_indiener";
  }

  @Override
  boolean beforeInsert(NbcIndiener src, Requester trg, Connection srcCon, Connection trgCon)
      throws MigrationException {
    Integer id = nameToId.get(src.getIndienerNaam());
    if (id == null) {
      return true;
    }
    LOG.warn(
        "Dubbele aanvrager: \"{}\". Zal de eerste behouden en nummerreeksen van de tweede aanvrager aan de eerste koppelen",
        src.getIndienerNaam());
    codeToId.put(src.getIndienercode(), id);
    return false;
  }

  @Override
  void afterInsert(NbcIndiener src, Requester trg, int id) throws MigrationException {
    nameToId.put(src.getIndienerNaam(), id);
    codeToId.put(src.getIndienercode(), id);
  }

  Optional<Integer> getId(String code) {
    return Optional.ofNullable(codeToId.get(code));
  }
}
