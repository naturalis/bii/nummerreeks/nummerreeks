package nl.naturalis.bcd.migrate;

import nl.naturalis.bcd.dao.CollectionDao;
import nl.naturalis.bcd.dao.SeriesDao;
import nl.naturalis.bcd.migrate.model.NbcNummerRegistratie;
import nl.naturalis.bcd.migrate.model.NbcPrefixToCollection;
import nl.naturalis.bcd.model.BcdCollection;
import nl.naturalis.bcd.model.NumberSeries;
import nl.naturalis.common.util.MutableInt;
import org.klojang.db.Row;
import org.klojang.db.SQL;
import org.klojang.db.SQLInsert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static nl.naturalis.bcd.model.Institute.CORRUPT_CODE;
import static nl.naturalis.bcd.model.Institute.OVERLAP_CODE;
import static nl.naturalis.bcd.migrate.ETLUtil.*;

class SeriesMigrator extends AbstractMigrator<NbcNummerRegistratie, NumberSeries> {

  private static final Logger LOG = LoggerFactory.getLogger(SeriesMigrator.class);

  // Keeps track of TOPDesk IDs passing by to detect duplicates.
  private final Set<String> topdeskIds = new HashSet<>(256);

  private final SeriesDao seriesDao = new SeriesDao();
  private final CollectionDao collDao = new CollectionDao();

  private final CmsMigrator cmsMigrator;
  private final DepartmentMigrator departmentMigrator;
  private final RequesterMigrator requesterMigrator;
  private final CollectionMigrator collectionMigrator;

  private final MutableInt dummyNumber = new MutableInt();

  // The ID of the collection that duplicate number series get reassigned to
  private final int collIdDuplicate;
  // The ID of the collection that corrupt number series get reassigned to
  private final int collIdCorrupt;

  private int good;
  private int duplicates;
  private int corrupt;

  SeriesMigrator(Connection srcCon, Connection trgCon, CmsMigrator cmsMigrator,
      DepartmentMigrator departmentMigrator, RequesterMigrator requesterMigrator,
      CollectionMigrator collectionMigrator) {
    super(srcCon, trgCon, NbcNummerRegistratie.class, NumberSeries.class);
    this.cmsMigrator = cmsMigrator;
    this.departmentMigrator = departmentMigrator;
    this.requesterMigrator = requesterMigrator;
    this.collectionMigrator = collectionMigrator;
    this.collIdDuplicate = getCollectionIdForDuplicates(trgCon);
    this.collIdCorrupt = getCollectionIdForCorruptRecords(trgCon);
  }

  @Override
  String sourceTable() {
    return "nbc_nummerregistratie";
  }

  @Override
  String targetTable() {
    return "bcd_request";
  }

  @Override
  String getExtactSQL() {
    return "SELECT a.* FROM nbc_nummerregistratie a "
        + "LEFT JOIN nbc_prefix b ON a.PrefixID = b.PrefixID "
        + "ORDER BY b.InstitutionCode, b.CollectionCode, a.DatumIndiening DESC, a.Hoeveelheid";
  }

  @Override
  SQLInsert getInsertStatement(Connection trgCon) {
    return SQL.prepareInsert()
        .of(NumberSeries.class)
        .into(targetTable())
        .excluding("requestId", "collectionCode", "instituteId", "instituteCode", "lastUsedLayout")
        .prepare(trgCon);
  }

  @Override
  boolean beforeInsert(NbcNummerRegistratie src, NumberSeries trg, Connection srcCon,
      Connection trgCon) throws MigrationException {
    ensureUniqueTopDeskId(src, trg);
    setForeignKeys(src, trg);
    NbcPrefixToCollection origCollection = collectionMigrator.getOriginalCollection(src);
    if (origCollection != null) {
      // Then this number series had the wrong prefix. Update the comments field
      // of the number series to warn the user that the prefix has changed in the
      // new version of the Nummerreeks application.
      addCollectionChangedWarningToComments(src, trg, origCollection);
    }
    if (checkNumbers(trgCon, trg)) {
      if (checkOverlap(trgCon, trg)) {
        ++good;
      } else {
        ++duplicates;
      }
    } else {
      ++corrupt;
    }
    return true;
  }

  @Override
  void aferMainStatistics(Connection srcCon, Connection trgCon) {
    LOG.info("Aantal corrupte nummerreeksen ............: {}", corrupt);
    LOG.info("Aantal overlappende nummerreeksen ........: {}", duplicates);
    LOG.info("Aantal niet-overlappende nummerreeksen ...: {}", good);
  }

  private void ensureUniqueTopDeskId(NbcNummerRegistratie src, NumberSeries series) {
    String topdeskId = series.getTopDeskId();
    if (topdeskId == null) {
      topdeskId = "Onbekend" + src.getRegistratieID();
      series.setTopDeskId(topdeskId);
    } else if (topdeskIds.contains(topdeskId)) {
      for (int i = 2; ; ++i) {
        String newId = topdeskId + " (" + i + ")";
        if (!topdeskIds.contains(newId)) {
          LOG.warn("Dubbel TOPDesk ID: {}. Vervangen door \"{}\"", topdeskId, newId);
          topdeskId = newId;
          series.setTopDeskId(topdeskId);
          topdeskIds.add(topdeskId);
          break;
        }
      }
    }
    topdeskIds.add(topdeskId);
  }

  private void setForeignKeys(NbcNummerRegistratie src, NumberSeries series)
      throws MigrationException {
    Integer cmsId = cmsMigrator.getNewId(src.getOmgevingID())
        .orElseThrow(orphanException(src.getOmgevingID(), cmsMigrator, src));
    Integer deptId = departmentMigrator.getId(src.getAfdelingcode())
        .orElseThrow(orphanException(src.getAfdelingcode(), departmentMigrator, src));
    Integer reqId = requesterMigrator.getId(src.getIndienercode())
        .orElseThrow(orphanException(src.getIndienercode(), requesterMigrator, src));
    Integer collId = collectionMigrator.getNewId(src.getPrefixID())
        .orElseThrow(orphanException(src.getPrefixID(), collectionMigrator, src));
    series.setCmsId(cmsId);
    series.setDepartmentId(deptId);
    series.setRequesterId(reqId);
    series.setCollectionId(collId);
  }

  private boolean checkNumbers(Connection trgCon, NumberSeries series) {
    if (series.getAmount() == 0) {
      LOG.warn("Nummerreeks {}: ongeldige hoeveelheid (0). Autocorrectie toegepast",
          series.getTopDeskId());
      series.setAmount(series.getLastNumber() - series.getFirstNumber() + 1);
    }
    boolean fnOK = series.getFirstNumber() > 0;
    boolean amOK = series.getAmount() > 0;
    boolean sumOK = series.getFirstNumber() + series.getAmount() == series.getLastNumber() + 1;
    if (fnOK && amOK && sumOK) {
      return true;
    }
    dummyNumber.ipp();
    BcdCollection coll = getCollection(trgCon, series.getCollectionId());
    logCorruption(series, coll, dummyNumber);
    addCorruptionWarningToComments(series, coll);
    series.setFirstNumber(dummyNumber.get());
    series.setLastNumber(series.getFirstNumber());
    series.setAmount(1);
    series.setCollectionId(collIdCorrupt);
    return false;
  }

  /*
   * Find duplicates of the current (about-to-be-inserted) number series amongst number series that
   * have already been inserted. If found, a warning is added to the comments field of the current
   * number specifying the TOPDesk IDs of these already migrated number series. At the same time,
   * the comments field of the already migrated number series is updated post facto with the TOPDesk
   * ID of the current number series. Thus you can always find the number series that overlap for
   * any given number series, no matter the order in which they were inserted.
   */
  private boolean checkOverlap(Connection trgCon, NumberSeries series) {
    List<Row> overlap = seriesDao.findOverlap(trgCon, series);
    BcdCollection coll = getCollection(trgCon, series.getCollectionId());
    String firstLabel = coll.prefix() + "." + series.getFirstNumber();
    String lastLabel = coll.prefix() + "." + series.getLastNumber();
    if (overlap.isEmpty()) {
      // The number series does not overlap with any of the already migrated
      // number series. Go ahead and insert it.
      return true;
    }
    markPostFactoDuplicates(trgCon, series.getTopDeskId(), overlap);
    dummyNumber.ipp();
    logOverlap(series, lastLabel, firstLabel, overlap, dummyNumber);
    addOverlapWarningToComments(series, coll, overlap);
    series.setFirstNumber(dummyNumber.get());
    series.setLastNumber(series.getFirstNumber());
    series.setAmount(1);
    series.setCollectionId(collIdDuplicate);
    return false;
  }

  private HashMap<Integer, BcdCollection> collLookup = new HashMap<>(64);

  private BcdCollection getCollection(Connection con, Integer id) {
    return collLookup.computeIfAbsent(id, k -> collDao.find(con, id));
  }

  /*
   * Returns the ID of the OVERLAP collection, which will become the stand-in collection of
   * duplicate number series
   */
  private int getCollectionIdForDuplicates(Connection trgCon) {
    String sql = "SELECT a.collectionId FROM bcd_collection a "
        + "JOIN bcd_institute b ON a.instituteId = b.instituteId "
        + "WHERE b.instituteCode = :code ";
    return SQL.create(sql).prepareQuery(trgCon).bind("code", OVERLAP_CODE).getInt();
  }

  /*
   * Returns the ID of the CORRUPT collection, which will become the stand-in collection of number
   * series with invalid first number and/or last number and/or amount.
   */
  private Integer getCollectionIdForCorruptRecords(Connection trgCon) {
    String sql = "SELECT a.collectionId FROM bcd_collection a "
        + "JOIN bcd_institute b ON a.instituteId = b.instituteId "
        + "WHERE b.instituteCode = :code ";
    return SQL.create(sql).prepareQuery(trgCon).bind("code", CORRUPT_CODE).getInt();
  }
}
