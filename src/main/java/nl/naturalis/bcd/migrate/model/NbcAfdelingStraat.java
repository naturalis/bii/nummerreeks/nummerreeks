package nl.naturalis.bcd.migrate.model;

import nl.naturalis.bcd.migrate.SelfTransformer;
import nl.naturalis.bcd.model.Department;

public class NbcAfdelingStraat implements SelfTransformer<Department> {

  private String Afdelingcode;
  private String AfdelingNaamstraat;
  private String IndActief;

  @Override
  public Department morph() {
    Department dept = new Department();
    dept.setActive("J".equals(IndActief));
    dept.setDepartmentName(AfdelingNaamstraat);
    return dept;
  }

  public String getAfdelingcode() {
    return Afdelingcode;
  }

  public void setAfdelingcode(String afdelingcode) {
    Afdelingcode = afdelingcode;
  }

  public String getAfdelingNaamstraat() {
    return AfdelingNaamstraat;
  }

  public void setAfdelingNaamstraat(String afdelingNaamstraat) {
    AfdelingNaamstraat = afdelingNaamstraat;
  }

  public String getIndActief() {
    return IndActief;
  }

  public void setIndActief(String indActief) {
    IndActief = indActief;
  }
}
