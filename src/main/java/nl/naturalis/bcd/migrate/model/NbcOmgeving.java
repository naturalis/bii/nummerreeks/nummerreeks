package nl.naturalis.bcd.migrate.model;

import nl.naturalis.bcd.migrate.SelfTransformer;
import nl.naturalis.bcd.model.Cms;

public class NbcOmgeving implements SelfTransformer<Cms> {
  private int OmgevingID;
  private String Omgevingnaam;
  private String IndActief;

  @Override
  public Cms morph() {
    Cms cms = new Cms();
    cms.setActive("J".equals(IndActief));
    cms.setCmsName(Omgevingnaam);
    return cms;
  }

  public int getOmgevingID() {
    return OmgevingID;
  }

  public void setOmgevingID(int omgevingID) {
    OmgevingID = omgevingID;
  }

  public String getOmgevingnaam() {
    return Omgevingnaam;
  }

  public void setOmgevingnaam(String omgevingnaam) {
    Omgevingnaam = omgevingnaam;
  }

  public String getIndActief() {
    return IndActief;
  }

  public void setIndActief(String indActief) {
    IndActief = indActief;
  }
}
