package nl.naturalis.bcd.migrate.model;

import java.time.LocalDateTime;
import nl.naturalis.bcd.migrate.SelfTransformer;
import nl.naturalis.bcd.model.Institute;

public class NbcPrefixToInstitute implements SelfTransformer<Institute> {

  private int PrefixID;
  private String InstitutionCode;
  private String CollectionCode;
  private String Omschrijving;
  private String IndActief;
  private LocalDateTime Datummutatie;
  private String Datuminvoer;
  private String Naamgebruiker;

  @Override
  public Institute morph() {
    Institute inst = new Institute();
    inst.setActive(true);
    inst.setInstituteCode(InstitutionCode);
    inst.setInstituteName(InstitutionCode);
    return inst;
  }

  public int getPrefixID() {
    return PrefixID;
  }

  public void setPrefixID(int prefixID) {
    PrefixID = prefixID;
  }

  public String getInstitutionCode() {
    return InstitutionCode;
  }

  public void setInstitutionCode(String institutionCode) {
    InstitutionCode = institutionCode;
  }

  public String getCollectionCode() {
    return CollectionCode;
  }

  public void setCollectionCode(String collectionCode) {
    CollectionCode = collectionCode;
  }

  public String getOmschrijving() {
    return Omschrijving;
  }

  public void setOmschrijving(String omschrijving) {
    Omschrijving = omschrijving;
  }

  public String getIndActief() {
    return IndActief;
  }

  public void setIndActief(String indActief) {
    IndActief = indActief;
  }

  public LocalDateTime getDatummutatie() {
    return Datummutatie;
  }

  public void setDatummutatie(LocalDateTime datummutatie) {
    Datummutatie = datummutatie;
  }

  public String getDatuminvoer() {
    return Datuminvoer;
  }

  public void setDatuminvoer(String datuminvoer) {
    Datuminvoer = datuminvoer;
  }

  public String getNaamgebruiker() {
    return Naamgebruiker;
  }

  public void setNaamgebruiker(String naamgebruiker) {
    Naamgebruiker = naamgebruiker;
  }
}
