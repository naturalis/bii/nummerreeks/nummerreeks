package nl.naturalis.bcd.migrate.model;

import java.time.LocalDateTime;
import nl.naturalis.bcd.migrate.SelfTransformer;
import nl.naturalis.bcd.model.BcdCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NbcPrefixToCollection implements SelfTransformer<BcdCollection> {

  private static final Logger LOG = LoggerFactory.getLogger(NbcPrefixToCollection.class);

  private int PrefixID;
  private String InstitutionCode;
  private String CollectionCode;
  private String Omschrijving;
  private String IndActief;
  private LocalDateTime Datummutatie;
  private String Datuminvoer;
  private String Naamgebruiker;

  @Override
  public BcdCollection morph() {
    BcdCollection coll = new BcdCollection();
    coll.setActive("J".equals(IndActief));
    if ("BE".equals(InstitutionCode) && "NUM".equals(CollectionCode)) {
      LOG.warn("Bezig met dummy collectie \"NUM\" van dummy instituut \"BE\"");
      LOG.warn("De collectie-code wordt op null gezet en de naam op \"BE\"");
      coll.setCollectionName("BE");
      coll.setCollectionCode(null);
    } else if ("N/A".equals(CollectionCode)) {
      LOG.warn("Bezig met dummy collectie \"N/A\" van instituut \"{}\"", InstitutionCode);
      LOG.warn("De collectie-code wordt op null gezet en de naam op \"{}\"", InstitutionCode);
      coll.setCollectionName(InstitutionCode);
      coll.setCollectionCode(null);
    } else if (InstitutionCode.equals(CollectionCode)) {
      LOG.warn("Bezig met collectie \"{}\" van instituut \"{}\"", CollectionCode, InstitutionCode);
      LOG.warn("De collectie-code wordt op null gezet en de naam op \"{}\"", InstitutionCode);
      coll.setCollectionName(InstitutionCode);
      coll.setCollectionCode(null);
    } else {
      coll.setCollectionName(CollectionCode);
      coll.setCollectionCode(CollectionCode);
    }
    coll.setDescription(Omschrijving);
    return coll;
  }

  public int getPrefixID() {
    return PrefixID;
  }

  public void setPrefixID(int prefixID) {
    PrefixID = prefixID;
  }

  public String getInstitutionCode() {
    return InstitutionCode;
  }

  public void setInstitutionCode(String institutionCode) {
    InstitutionCode = institutionCode;
  }

  public String getCollectionCode() {
    return CollectionCode;
  }

  public void setCollectionCode(String collectionCode) {
    CollectionCode = collectionCode;
  }

  public String getOmschrijving() {
    return Omschrijving;
  }

  public void setOmschrijving(String omschrijving) {
    Omschrijving = omschrijving;
  }

  public String getIndActief() {
    return IndActief;
  }

  public void setIndActief(String indActief) {
    IndActief = indActief;
  }

  public LocalDateTime getDatummutatie() {
    return Datummutatie;
  }

  public void setDatummutatie(LocalDateTime datummutatie) {
    Datummutatie = datummutatie;
  }

  public String getDatuminvoer() {
    return Datuminvoer;
  }

  public void setDatuminvoer(String datuminvoer) {
    Datuminvoer = datuminvoer;
  }

  public String getNaamgebruiker() {
    return Naamgebruiker;
  }

  public void setNaamgebruiker(String naamgebruiker) {
    Naamgebruiker = naamgebruiker;
  }
}
