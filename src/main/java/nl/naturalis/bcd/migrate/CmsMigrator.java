package nl.naturalis.bcd.migrate;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import nl.naturalis.bcd.migrate.model.NbcOmgeving;
import nl.naturalis.bcd.model.Cms;

class CmsMigrator extends AbstractMigrator<NbcOmgeving, Cms> {

  private Map<Integer, Integer> idLookup = new HashMap<>();

  CmsMigrator(Connection srcCon, Connection trgCon) {
    super(srcCon, trgCon, NbcOmgeving.class, Cms.class);
  }

  @Override
  String sourceTable() {
    return "nbc_omgeving";
  }

  @Override
  void afterInsert(NbcOmgeving src, Cms trg, int id) throws MigrationException {
    idLookup.put(src.getOmgevingID(), id);
  }

  Optional<Integer> getNewId(Integer oldId) {
    return Optional.ofNullable(idLookup.get(oldId));
  }
}
