package nl.naturalis.bcd.migrate;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import nl.naturalis.bcd.migrate.model.NbcPrefixToInstitute;
import org.klojang.db.SQLInsert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.bcd.model.Institute;

class InstituteMigrator extends AbstractMigrator<NbcPrefixToInstitute, Institute> {

  private static final Logger LOG = LoggerFactory.getLogger(InstituteMigrator.class);

  private final Map<String, Integer> codeToId = new HashMap<>();

  InstituteMigrator(Connection srcCon, Connection trgCon) {
    super(srcCon, trgCon, NbcPrefixToInstitute.class, Institute.class);
  }

  @Override
  String sourceTable() {
    return "nbc_prefix";
  }

  @Override
  void afterTruncate(Connection srcCon, Connection trgCon) {
    try (SQLInsert insert = getInsertStatement(trgCon)) {
      LOG.info("Maak dummy instituut \"{}\" aan voor overlappende nummerreeksen",
          Institute.OVERLAP_CODE);
      Institute inst = new Institute();
      inst.setActive(true);
      inst.setInstituteCode(Institute.OVERLAP_CODE);
      inst.setInstituteName("Dummy instituut voor dubbele nummerreeksen");
      insert.bind(inst).execute();
      LOG.info("Maak dummy instituut \"{}\" aan voor corrupte nummerreeksen",
          Institute.CORRUPT_CODE);
      inst = new Institute();
      inst.setActive(true);
      inst.setInstituteCode(Institute.CORRUPT_CODE);
      inst.setInstituteName("Dummy instituut voor corrupte nummerreeksen");
      insert.bind(inst).execute();
      LOG.info("Maak nieuw instituut \"RGMS\" (vervangt RGM.THD)");
      inst = new Institute();
      inst.setActive(true);
      inst.setInstituteCode("RGMS");
      inst.setInstituteName("RGMS");
      insert.bind(inst).execute();
    }
  }

  boolean beforeTransform(NbcPrefixToInstitute src) throws MigrationException {
    return !codeToId.containsKey(src.getInstitutionCode());
  }

  @Override
  void afterInsert(NbcPrefixToInstitute src, Institute trg, int id) throws MigrationException {
    codeToId.put(src.getInstitutionCode(), id);
  }

  Integer getId(String code) {
    return codeToId.get(code);
  }
}
