package nl.naturalis.bcd.migrate;

import static nl.naturalis.bcd.model.Institute.CORRUPT_CODE;
import static nl.naturalis.bcd.model.Institute.OVERLAP_CODE;
import static nl.naturalis.common.ObjectMethods.n2e;
import static nl.naturalis.common.StringMethods.append;
import static nl.naturalis.common.StringMethods.implode;

import java.sql.Connection;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import nl.naturalis.bcd.migrate.model.NbcNummerRegistratie;
import nl.naturalis.bcd.migrate.model.NbcPrefixToCollection;
import nl.naturalis.bcd.model.BcdCollection;
import nl.naturalis.bcd.model.NumberSeries;
import nl.naturalis.common.Pair;
import nl.naturalis.common.util.MutableInt;
import org.klojang.db.Row;
import org.klojang.db.SQL;
import org.klojang.db.SQLUpdate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class ETLUtil {

  private static final Logger LOG = LoggerFactory.getLogger(ETLUtil.class);

  static final Function<Row, String> OVERLAP_STRINGIGIFIER =
      row -> row.get("topDeskId") + " (" + row.get("firstNumber") + " t/m " + row.get("lastNumber")
          + ")";

  private static final String NEWLINE = System.lineSeparator();

  private static final StringBuilder SB_MAIN = new StringBuilder(1024);

  /*
   * Marks already-migrated number series as a duplicate of an about-to-be-migrated
   * number series by updating their comments column
   */
  static void markPostFactoDuplicates(Connection trgCon, String topDeskId, List<Row> overlap) {
    SQL sql = SQL.create("UPDATE bcd_request SET comments = "
        + "CONCAT('>>> DEZE NUMMERREEKS OVERLAPT MET ', :topDeskId, ' <<< \n', comments) "
        + "WHERE topDeskId = :updatedTopDeskId");
    for (Row row : overlap) {
      String updatedTopDeskId = row.getString("topDeskId");
      try (SQLUpdate update = sql.prepareUpdate(trgCon)) {
        update.bind("topDeskId", topDeskId).bind("updatedTopDeskId", updatedTopDeskId).execute();
      }
    }
  }

  static void addOverlapWarningToComments(NumberSeries series, BcdCollection collection,
      List<Row> overlap) {
    SB_MAIN.setLength(0);
    SB_MAIN.append(">>> DEZE NUMMERREEKS OVERLAPT MET ANDERE NUMMERREEKSEN!")
        .append(NEWLINE)
        .append("Oorspronkelijke instituut code: ")
        .append(NEWLINE)
        .append(collection.getInstituteCode())
        .append(NEWLINE)
        .append("Oorspronkelijke collectie code: ")
        .append(n2e(collection.getCollectionCode()))
        .append(NEWLINE)
        .append("Oorspronkelijke eerste nummer: ")
        .append(series.getFirstNumber())
        .append(NEWLINE)
        .append("Oorspronkelijke laatste nummer: ")
        .append(series.getLastNumber())
        .append(NEWLINE)
        .append("Oorspronkelijke aantal nummers: ")
        .append(series.getAmount())
        .append(NEWLINE)
        .append("Overlappende nummerreeks(en): ")
        .append(NEWLINE)
        .append(implode(overlap, OVERLAP_STRINGIGIFIER, NEWLINE));
    if (series.getComments() != null) {
      SB_MAIN.append(NEWLINE).append(">>>").append(NEWLINE).append(series.getComments());
    }
    series.setComments(SB_MAIN.toString());
  }

  static void addCollectionChangedWarningToComments(NbcNummerRegistratie oldSeries,
      NumberSeries newSeries, NbcPrefixToCollection origCollection) {
    logPrefixChanged(newSeries, origCollection);
    SB_MAIN.setLength(0);
    SB_MAIN.append("+++ DEZE NUMMERREEKS IS DOOR EEN MIGRATIEFOUT EEN TIJD ")
        .append("FOUTIEF GEREGISTREERD GEWEEST ALS ")
        .append(origCollection.getInstitutionCode())
        .append('.')
        .append(origCollection.getCollectionCode())
        .append('.')
        .append(oldSeries.getEerstenummerreeks())
        .append(" t/m ")
        .append(origCollection.getInstitutionCode())
        .append('.')
        .append(origCollection.getCollectionCode())
        .append('.')
        .append(oldSeries.getLaatstenummerreeks())
        .append(". DE PREFIX IS IN 2022 WEER HERSTELD EN AANGEPAST NAAR ")
        .append(toPrefix(CollectionMigrator.getPrefixChange(origCollection)));
    if (newSeries.getComments() != null) {
      SB_MAIN.append(NEWLINE).append("+++").append(NEWLINE).append(newSeries.getComments());
    }
    newSeries.setComments(SB_MAIN.toString());
  }

  static String toPrefix(Pair<String> p) {
    return p.getFirst() + ((p.getSecond() == null) ? "" : "." + p.getSecond());
  }

  static void addCorruptionWarningToComments(NumberSeries series, BcdCollection coll) {
    SB_MAIN.setLength(0);
    SB_MAIN.append(">>> DIT IS EEN ONGELDIGE NUMMERREEKS!")
        .append(NEWLINE)
        .append("Oorspronkelijke instituut code: ")
        .append(coll.getInstituteCode())
        .append(NEWLINE)
        .append("Oorspronkelijke collectie code: ")
        .append(n2e(coll.getCollectionCode()))
        .append(NEWLINE)
        .append("Oorspronkelijke eerste nummer: ")
        .append(series.getFirstNumber())
        .append(NEWLINE)
        .append("Oorspronkelijke laatste nummer: ")
        .append(series.getLastNumber())
        .append(NEWLINE)
        .append("Oorspronkelijke aantal nummers: ")
        .append(series.getAmount().toString());
    boolean fnOK = series.getFirstNumber() > 0;
    boolean amOK = series.getAmount() > 0;
    boolean sumOK = series.getFirstNumber() + series.getAmount() == series.getLastNumber() + 1;
    if (!fnOK) {
      SB_MAIN.append(NEWLINE).append("Reden: eerste nummer is kleiner dan 1");
    }
    if (!amOK) {
      SB_MAIN.append(NEWLINE).append("Reden: hoeveelheid is kleiner dan 1");
    }
    if (!sumOK) {
      SB_MAIN.append(NEWLINE).append("Reden: optelsom klopt niet");
    }
    if (series.getComments() != null) {
      SB_MAIN.append(NEWLINE).append(">>>").append(NEWLINE).append(series.getComments());
    }
    series.setComments(SB_MAIN.toString());
  }

  @SuppressWarnings("rawtypes")
  static <T> Supplier<MigrationException> orphanException(T key, AbstractMigrator etl,
      NbcNummerRegistratie src) {
    String fmt = "Orphan key %s to %s in record with ID %s";
    int id = src.getRegistratieID();
    String msg = String.format(fmt, key, etl.sourceTable(), id);
    return () -> new MigrationException(msg);
  }

  static void logPrefixChanged(NumberSeries series, NbcPrefixToCollection origCollection) {
    Pair<String> oldPrefix =
        Pair.of(origCollection.getInstitutionCode(), origCollection.getCollectionCode());
    Pair<String> newPrefix = CollectionMigrator.getPrefixChange(origCollection);
    LOG.warn("Nummerreeks {}: prefix gewijzigd. Oorspronkelijke prefix: {}; nieuwe prefix: {}",
        series.getTopDeskId(),
        toPrefix(oldPrefix),
        toPrefix(newPrefix));
  }

  static void logOverlap(NumberSeries series, String lastLabel, String firstLabel,
      List<Row> overlap, MutableInt dummyNumber) {
    LOG.warn(
        "Nummerreeks {} t/m {} ({}) overlapt met een of meer andere reeksen. Gemigreerd als {}.{}",
        firstLabel,
        lastLabel,
        series.getTopDeskId(),
        OVERLAP_CODE,
        dummyNumber);
    for (Row row : overlap) {
      LOG.warn(">>>> " + OVERLAP_STRINGIGIFIER.apply(row));
    }
  }

  static void logCorruption(NumberSeries series, BcdCollection coll, MutableInt dummyNumber) {
    int fn = series.getFirstNumber();
    int ln = series.getLastNumber();
    int am = series.getAmount();
    String firstLabel = coll.prefix() + "." + fn;
    String lastLabel = coll.prefix() + "." + ln;
    boolean fnOK = fn > 0;
    boolean amOK = am > 0;
    boolean sumOK = fn + am == ln + 1;
    LOG.warn("Corrupte nummerreeks {} t/m {} ({}). Gemigreerd als {}.{}",
        firstLabel,
        lastLabel,
        series.getTopDeskId(),
        CORRUPT_CODE,
        dummyNumber);
    LOG.warn(">>>> Eerste nummer ...: " + (fnOK ? "goed" : "corrupt (" + fn + ")"));
    LOG.warn(">>>> Aantal ..........: " + (amOK ? "goed" : "corrupt (" + am + ")"));
    LOG.warn(">>>> Optelsom ........: " + (sumOK ? "goed" : "corrupt"));
  }
}
