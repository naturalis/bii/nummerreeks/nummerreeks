package nl.naturalis.bcd.migrate;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toSet;
import static nl.naturalis.bcd.model.Institute.CORRUPT_CODE;
import static nl.naturalis.bcd.model.Institute.OVERLAP_CODE;
import static nl.naturalis.bcd.migrate.ETLUtil.toPrefix;

import java.sql.Connection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import nl.naturalis.bcd.migrate.model.NbcNummerRegistratie;
import nl.naturalis.bcd.migrate.model.NbcPrefixToCollection;
import nl.naturalis.bcd.model.BcdCollection;
import nl.naturalis.common.Pair;
import org.klojang.db.SQL;
import org.klojang.db.SQLInsert;
import org.klojang.db.SQLQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class CollectionMigrator extends AbstractMigrator<NbcPrefixToCollection, BcdCollection> {

  private static final Logger LOG = LoggerFactory.getLogger(CollectionMigrator.class);

  // Maps institute codes to institute IDs (in the target database)
  private final Map<String, Integer> instituteIdLookup = new HashMap<>();

  // Mps unique key of InstitutionCode and CollectionCode in old
  // table (nbc_prefix) to the id in new table (bcd_collection)
  private final Map<Pair<String>, Integer> uniqueKeyToNewId = new HashMap<>();

  // Maps old primary key (PrefixId) to new primary key (collectionId)
  private final Map<Integer, Integer> oldIdToNewId = new HashMap<>();

  // Maps "bad" collections to the proper ones.
  private static final Map<Pair<String>, Pair<String>> BAD_COLLECTIONS =
      Map.of(Pair.of("RGM", "N/A"),
          Pair.of("RGM", null),
          Pair.of("RMNH", "VER"),
          Pair.of("RGM", null),
          Pair.of("RGM", "UVA.L"),
          Pair.of("RMNH", "ACA.UT"),
          Pair.of("RGM", "THD"),
          Pair.of("RGMS", null));

  // If the specified collection needs to be mapped to another collection,
  // returns that collection; otherwise null. So null means: the collection
  // is (basically) valid and needs to be migrated.
  static Pair<String> getPrefixChange(NbcPrefixToCollection badCollection) {
    return BAD_COLLECTIONS.get(Pair.of(badCollection.getInstitutionCode(),
        badCollection.getCollectionCode()));
  }

  // Maps the id of a "bad" collection to that collection, Will be populated
  // as we encounter them.
  private final Map<Integer, NbcPrefixToCollection> badCollections = new HashMap<>(8);

  // Iterates of the "bad" collections and ensures they will be processed
  // after all the other collections have been processed. Thus the collections
  // that they need to be mapped to will already be present in the target
  // database.
  private static String getOrderBy() {
    Iterator<Pair<String>> iter = BAD_COLLECTIONS.keySet().iterator();
    String fmt = "(InstitutionCode='%s' AND CollectionCode='%s')";
    Pair<String> prefix = iter.next();
    StringBuilder orderBy = new StringBuilder(150);
    orderBy.append(" ORDER BY IF(");
    orderBy.append(String.format(fmt, prefix.getFirst(), prefix.getSecond()));
    while (iter.hasNext()) {
      prefix = iter.next();
      orderBy.append(" OR ").append(String.format(fmt, prefix.getFirst(), prefix.getSecond()));
    }
    orderBy.append(", 1, 0)"); // end of IF expression
    orderBy.append(", InstitutionCode, CollectionCode");
    return orderBy.toString();
  }

  CollectionMigrator(Connection srcCon, Connection trgCon) {
    super(srcCon, trgCon, NbcPrefixToCollection.class, BcdCollection.class);
  }

  @Override
  String getExtactSQL() {
    return super.getExtactSQL() + getOrderBy();
  }

  @Override
  String sourceTable() {
    return "nbc_prefix";
  }

  @Override
  String targetTable() {
    return "bcd_collection";
  }

  @Override
  SQLInsert getInsertStatement(Connection trgCon) {
    return SQL.prepareInsert()
        .of(BcdCollection.class)
        .into(targetTable())
        .excluding("collectionId", "instituteCode")
        .prepare(trgCon);
  }

  @Override
  void afterTruncate(Connection srcCon, Connection trgCon) {
    try (SQLInsert insert = getInsertStatement(trgCon)) {
      LOG.info("Maak dummy collectie aan voor dummy instituut \"{}\"", OVERLAP_CODE);
      BcdCollection coll = new BcdCollection();
      coll.setActive(true);
      coll.setCollectionName("Dummy collectie voor overlappende nummerreeksen");
      coll.setInstituteId(getInstituteId(trgCon, OVERLAP_CODE));
      insert.bind(coll).execute();
      LOG.info("Maak dummy collectie aan voor dummy instituut \"{}\"", CORRUPT_CODE);
      coll = new BcdCollection();
      coll.setActive(true);
      coll.setCollectionName("Dummy collectie voor corrupte nummerreeksen");
      coll.setInstituteId(getInstituteId(trgCon, CORRUPT_CODE));
      insert.bind(coll).execute();
      LOG.info("Maak dummy collectie aan voor instituut \"RGMS\" (vervangt RGM.THD)");
      coll = new BcdCollection();
      coll.setActive(true);
      coll.setCollectionName("RGMS");
      coll.setInstituteId(getInstituteId(trgCon, "RGMS"));
      int id = (int) insert.bind(coll).executeAndGetId();
      uniqueKeyToNewId.put(Pair.of("RGMS", null), id);
    }
  }

  @Override
  boolean beforeInsert(NbcPrefixToCollection src, BcdCollection trg, Connection srcCon,
      Connection trgCon) throws MigrationException {

    /*
     * Is this a collection that needs to be mapped to another collection? If so, lookup the id of
     * the other collection (which ought to have already been inserted) and map this collection's
     * old id to the other collection's new ID, and we're done.
     */
    Pair<String> changed = getPrefixChange(src);
    if (changed != null) {
      String oldPrefix = toPrefix(Pair.of(src.getInstitutionCode(), src.getCollectionCode()));
      String newPrefix = toPrefix(changed);
      LOG.warn("Sla collectie met prefix {} over. De nieuwe prefix wordt {}", oldPrefix, newPrefix);
      int oldId = src.getPrefixID();
      Integer newId = uniqueKeyToNewId.get(changed);
      if (newId == null) { // Should never happen, but let's check anyhow
        String fmt =
            "Prefix %s moet gewijzigd worden in %s, maar er is geen collectie met deze prefix";
        String msg = String.format(fmt, oldPrefix, newPrefix);
        throw new AbortException(msg);
      }
      oldIdToNewId.put(oldId, newId);
      badCollections.put(oldId, src);
      return false;
    }

    // Watch out: this is how it MUST be! Don't use trg for both! See afterInsert()
    Pair<String> uniqueKey = Pair.of(src.getInstitutionCode(), trg.getCollectionCode());

    /*
     * Did we already migrate this collection? If a collection has code "N/A" or its collection code
     * is the same as the institute code, it is in fact a dummy collection for labels that only have
     * an institute code. Thus, a collection with code "N/A" and a collection with a code equal to
     * the institute code are in fact duplicates of each other and we only insert one (after
     * nullifying the collection code).
     */
    Integer newId = uniqueKeyToNewId.get(uniqueKey);
    if (newId == null) { // No, we did not
      Integer instituteId = getInstituteId(trgCon, src.getInstitutionCode());
      trg.setInstituteId(instituteId);
      return true;
    }
    /*
     * We did. Map this collection's id in the source database to the id of the already-migrated
     * collection in the target database and skip this collection.
     */
    oldIdToNewId.put(src.getPrefixID(), newId);
    logCollectionSkipped(src);
    return false;
  }

  @Override
  void afterInsert(NbcPrefixToCollection src, BcdCollection trg, int id) throws MigrationException {
    /*
     * Take institute from source record and collection from target record!!! BcdCollection _does_
     * have an institute code, but it is a denormalization for the web app, It is NOT populated
     * during the migration. On the other hand, we cannot take the collection code from nbc_prefix
     * because it may have been nullified in BcdCollection.
     */
    Pair<String> uniqueKey = Pair.of(src.getInstitutionCode(), trg.getCollectionCode());
    uniqueKeyToNewId.put(uniqueKey, id);
    oldIdToNewId.put(src.getPrefixID(), id);
  }

  @Override
  void aferMainStatistics(Connection srcCon, Connection trgCon) {
    if (badCollections.size() != BAD_COLLECTIONS.size()) {
      Set<Pair<String>> required = new HashSet<>(BAD_COLLECTIONS.values());
      Set<Pair<String>> processed = badCollections.values()
          .stream()
          .map(c -> Pair.of(c.getInstitutionCode(), c.getCollectionCode()))
          .collect(toSet());
      required.removeAll(processed);
      StringBuilder sb = new StringBuilder(150);
      sb.append("Fout tijdens het migreren van de collecties. ")
          .append("De volgende prefixes moesten worden aangepast: ")
          .append(badCollectionToString())
          .append(". Maar de volgende prefix(en) zijn niet aanwezig in de brontabel, ")
          .append("of zijn om een of andere reden niet verwerkt: ")
          .append(required.stream().map(ETLUtil::toPrefix).collect(joining(",")));
      throw new AbortException(sb.toString());
    }
  }

  private String badCollectionToString() {
    StringBuilder sb = new StringBuilder(80);
    boolean first = true;
    for (Map.Entry<Pair<String>, Pair<String>> e : BAD_COLLECTIONS.entrySet()) {
      if (!first) {
        sb.append("; ");
      }
      sb.append(toPrefix(e.getKey())).append(" => ").append(toPrefix(e.getValue()));
      first = false;
    }
    return sb.toString();
  }

  Optional<Integer> getNewId(Integer prefixId) {
    return Optional.ofNullable(oldIdToNewId.get(prefixId));
  }

  /*
   * Returns the original collection of the specified number series _if_ that collection was a "bad"
   * collection that needs to be mapped to another collection, else null. So null means: the
   * collection has not changed and we don't need to add a warning to that effect in the comments
   * field of the record.
   */
  NbcPrefixToCollection getOriginalCollection(NbcNummerRegistratie oldNumberSeries) {
    return badCollections.get(oldNumberSeries.getPrefixID());
  }

  private static final SQL SQL_GET_INSTITUTE_ID =
      SQL.create("SELECT instituteId FROM bcd_institute WHERE instituteCode = :code");

  private Integer getInstituteId(Connection trgCon, String code) {
    Integer id = instituteIdLookup.get(code);
    if (id == null) {
      try (SQLQuery query = SQL_GET_INSTITUTE_ID.prepareQuery(trgCon)) {
        instituteIdLookup.put(code, id = query.bind("code", code).getInt());
      }
    }
    return id;
  }

  private static void logCollectionSkipped(NbcPrefixToCollection src) {
    LOG.warn(
        "Sla collectie {} van instituut {} over. Nadat de collectie-code op null was gezet bleek "
            + "ze een duplicaat van een reeds gemigreerde collectie te zijn. Nummerreeksen in "
            + "deze collectie zullen worden over gezet naar de reeds gemigreerde collectie",
        src.getCollectionCode(),
        src.getInstitutionCode());
  }
}
