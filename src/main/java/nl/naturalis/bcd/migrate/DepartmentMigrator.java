package nl.naturalis.bcd.migrate;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import nl.naturalis.bcd.migrate.model.NbcAfdelingStraat;
import nl.naturalis.bcd.model.Department;

class DepartmentMigrator extends AbstractMigrator<NbcAfdelingStraat, Department> {

  private final Map<String, Integer> idLookup = new HashMap<>();

  DepartmentMigrator(Connection srcCon, Connection trgCon) {
    super(srcCon, trgCon, NbcAfdelingStraat.class, Department.class);
  }

  @Override
  String sourceTable() {
    return "nbc_afdelingstraat";
  }

  @Override
  void afterInsert(NbcAfdelingStraat src, Department trg, int id) throws MigrationException {
    idLookup.put(src.getAfdelingcode(), id);
  }

  Optional<Integer> getId(String code) {
    return Optional.ofNullable(idLookup.get(code));
  }
}
