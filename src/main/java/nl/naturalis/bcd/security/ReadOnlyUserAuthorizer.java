package nl.naturalis.bcd.security;

import org.pac4j.core.authorization.authorizer.Authorizer;
import org.pac4j.core.context.WebContext;
import org.pac4j.core.profile.UserProfile;
import org.pac4j.oidc.profile.azuread.AzureAdProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ReadOnlyUserAuthorizer implements Authorizer<UserProfile> {

    private static final Logger logger = LoggerFactory.getLogger(ReadOnlyUserAuthorizer.class);

    @Override
    public boolean isAuthorized(WebContext webContext, List<UserProfile> list) {
        for (UserProfile profile : list) {
            if (profile instanceof AzureAdProfile && profile.getRoles().contains("VIEWER")) {
                logger.debug("User has role \"VIEWER\" and is allowed read-only access");
                return true;
            }
        }
        return false;
    }
}
