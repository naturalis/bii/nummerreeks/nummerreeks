package nl.naturalis.bcd.security;

import nl.naturalis.bcd.BcdUtil;
import org.pac4j.core.context.WebContext;
import org.pac4j.core.exception.http.RedirectionAction;
import org.pac4j.core.exception.http.RedirectionActionHelper;
import org.pac4j.core.logout.LogoutActionBuilder;
import org.pac4j.core.profile.UserProfile;
import org.pac4j.core.util.CommonHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class AzureAdLogoutActionBuilder implements LogoutActionBuilder {

    private static final Logger logger = LoggerFactory.getLogger(AzureAdLogoutActionBuilder.class);

    @Override
    public Optional<RedirectionAction> getLogoutAction(final WebContext context,
                                                       final UserProfile currentProfile,
                                                       final String targetUrl) {

        String server_url = System.getenv("SERVER_URL");
        if (server_url == null || server_url.isEmpty() || server_url.isBlank()) {
            server_url = "http://localhost:8080/";
        }
        String logoutUrl = BcdUtil.addPathToUrl(server_url, "logoff");
        final String azureAdLogoutUrl = "https://login.microsoftonline.com/common/oauth2/v2.0/logout";
        final String redirect_param = "post_logout_redirect_uri";
        final String redirectUrl = CommonHelper.addParameter(azureAdLogoutUrl, redirect_param, logoutUrl);
        logger.debug("redirectUrl: {}", redirectUrl);
        return Optional.of(RedirectionActionHelper.buildRedirectUrlAction(context, redirectUrl));
    }

}
