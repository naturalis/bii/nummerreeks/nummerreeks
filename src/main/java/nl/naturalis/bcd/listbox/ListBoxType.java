package nl.naturalis.bcd.listbox;

/**
 * All entities for which we have drop-down lists somewhere in the application.
 *
 * @author Ayco Holleman
 */
public enum ListBoxType {
  CMS,
  COLLECTION,
  DEPARTMENT,
  INSTITUTE,
  REQUESTER,
  LAYOUT;
}
