package nl.naturalis.bcd.listbox;

import java.util.List;
import org.klojang.db.Row;
import org.klojang.helpers.ListBoxOptions;
import nl.naturalis.bcd.dao.ListBoxDao;
import nl.naturalis.bcd.model.LabelUsage;

public class ListBoxFactory {

  private static final String DEFAULT_INIT_OPTION = "-- Selecteer a.u.b. --";

  public static ListBoxOptions yikesOption(String entity) {
    String text = String.format("-- Geen actieve %s gevonden --", entity);
    return new ListBoxOptions().withInitOption(text);
  }

  /* -------------------------------------------------- */
  /* If curId equals minus one it means the column      */
  /* corresponding to the listbox doesn't have a value  */
  /* yet. Most likely because we are creating a new     */
  /* record in stead of updating an existing one.       */
  /* -------------------------------------------------- */

  public static ListBoxOptions getCmsOptions(int curId) {
    List<Row> data = new ListBoxDao().cmsOptions(curId);
    if (data.isEmpty()) {
      return yikesOption("systemen");
    }
    ListBoxOptions options = new ListBoxOptions().withOptionData(data);
    return data.size() == 1 ? options.selectOption(0)
        : curId == -1 ? options.withInitOption(DEFAULT_INIT_OPTION) : options.selectValue(curId);
  }

  public static ListBoxOptions getCmsOptions() {
    return getCmsOptions(-1);
  }

  public static ListBoxOptions getRequesterOptions(int curId) {
    List<Row> data = new ListBoxDao().requesterOptions(curId);
    if (data.isEmpty()) {
      return yikesOption("aanvragers");
    }
    ListBoxOptions options = new ListBoxOptions().withOptionData(data);
    return data.size() == 1 ? options.selectOption(0)
        : curId == -1 ? options.withInitOption(DEFAULT_INIT_OPTION) : options.selectValue(curId);
  }

  public static ListBoxOptions getRequesterOptions() {
    return getRequesterOptions(-1);
  }

  /**
   * If curId is provided and not equal to -1, it means we *must* include the corresponding
   * department in the drop-down list even if it has meanwhile been de-activated. This is because we
   * are editing a number series whose current value for departmendId has that particular value and
   * we can't just force the user to select another department.
   */
  public static ListBoxOptions getDepartmentOptions(int curId) {
    List<Row> data = new ListBoxDao().departmentOptions(curId);
    if (data.isEmpty()) {
      return yikesOption("afdelingen");
    }
    ListBoxOptions options = new ListBoxOptions().withOptionData(data);
    return data.size() == 1 ? options.selectOption(0)
        : curId == -1 ? options.withInitOption(DEFAULT_INIT_OPTION) : options.selectValue(curId);
  }

  public static ListBoxOptions getDepartmentOptions() {
    return getDepartmentOptions(-1);
  }


  public static ListBoxOptions getInstituteOptions() {
    return getInstituteOptions(-1, true);
  }

  public static ListBoxOptions getInstituteOptions(boolean excludeDummies) {
    return getInstituteOptions(-1, excludeDummies);
  }

  public static ListBoxOptions getInstituteOptions(int curId) {
    return getInstituteOptions(curId, true);
  }

  public static ListBoxOptions getInstituteOptions(int curId, boolean excludeDummies) {
    List<Row> data = new ListBoxDao().instituteOptions(curId, excludeDummies);
    if (data.isEmpty()) {
      return yikesOption("instituten");
    }
    ListBoxOptions options = new ListBoxOptions().withOptionData(data);
    return data.size() == 1 ? options.selectOption(0)
        : curId == -1 ? options.withInitOption(DEFAULT_INIT_OPTION) : options.selectValue(curId);
  }

  public static ListBoxOptions getCollectionOptions(int curInstituteId, int curId) {
    List<Row> data = new ListBoxDao().collectionOptions(curInstituteId, curId);
    if (data.isEmpty()) {
      return yikesOption("collecties");
    }
    ListBoxOptions options = new ListBoxOptions().withOptionData(data);
    if (curInstituteId == -1) {
      // The user has selected the init option of the institutes list box
      return options.withInitOption(DEFAULT_INIT_OPTION);
    }
    return data.size() == 1 ? options.selectOption(0)
        : curId == -1 ? options.withInitOption(DEFAULT_INIT_OPTION) : options.selectValue(curId);
  }

  public static ListBoxOptions getCollectionOptions() {
    // We'll have to wait until the user selects an institute
    return new ListBoxOptions().withInitOption(DEFAULT_INIT_OPTION);
  }

  public static ListBoxOptions getLayoutOptions(String inputType) {
    List<Row> data = new ListBoxDao().layoutOptions(inputType);
    if (data.isEmpty()) {
      return yikesOption("formaten");
    }
    LabelUsage[] usages = LabelUsage.class.getEnumConstants();
    data.forEach(row -> row.setColumn("optgroup", usages[row.getInt("optgroup")]));
    ListBoxOptions options = new ListBoxOptions().withOptionData(data);
    return options.withInitOption(0, DEFAULT_INIT_OPTION);
  }
}
