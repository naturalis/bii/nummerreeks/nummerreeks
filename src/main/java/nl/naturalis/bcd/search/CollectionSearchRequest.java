package nl.naturalis.bcd.search;

public class CollectionSearchRequest extends SearchRequest {

  private Integer instituteId;
  private CollectionSortColumn sortColumn;

  public CollectionSortColumn getSortColumn() {
    return sortColumn;
  }

  public void setSortColumn(CollectionSortColumn sortColumn) {
    this.sortColumn = sortColumn;
  }

  public Integer getInstituteId() {
    return instituteId;
  }

  public void setInstituteId(Integer instituteId) {
    this.instituteId = instituteId;
  }
}
