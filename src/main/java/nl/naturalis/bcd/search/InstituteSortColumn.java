package nl.naturalis.bcd.search;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import nl.naturalis.common.util.EnumParser;

public enum InstituteSortColumn {
  INSTITUTE_NAME("a.instituteName"),
  INSTITUTE_CODE("a.instituteCode"),
  REQUEST_COUNT("requestCount"),
  MAX_NUMBER("maxNumber"),
  NUMBER_COUNT("numberCount"),
  ACTIVE("a.active");

  private static final EnumParser<InstituteSortColumn> ep =
      new EnumParser<>(InstituteSortColumn.class);

  @JsonCreator
  public static InstituteSortColumn parse(Object value) {
    return ep.parse(value);
  }

  private final String label;

  InstituteSortColumn(String label) {
    this.label = label;
  }

  @JsonValue
  public String toString() {
    return name();
  }

  public String getLabel() {
    return label;
  }
}
