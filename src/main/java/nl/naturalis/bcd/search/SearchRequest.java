package nl.naturalis.bcd.search;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import nl.naturalis.common.NumberMethods;
import nl.naturalis.common.invoke.AnyBeanReader;
import static nl.naturalis.common.ObjectMethods.ifNotNull;
import static nl.naturalis.common.ObjectMethods.ifNull;
import static nl.naturalis.common.ObjectMethods.n2e;
import static nl.naturalis.common.ObjectMethods.nullIf;
import static nl.naturalis.common.StringMethods.ifBlank;
import static nl.naturalis.common.check.CommonChecks.equalTo;

public abstract class SearchRequest {

  public static final int DEFAULT_PAGE_SIZE = 10;

  private static final AnyBeanReader abr = new AnyBeanReader(6);

  private Integer pageNo;
  private Integer pageSize;
  private String search;
  private SortOrder sortOrder;
  private Entity entity;
  private Integer entityId;
  private String entityNameOrCode;
  private Integer rowCount;

  public SearchRequest() {}

  public SearchRequest(SearchRequest other) {
    this.pageNo = other.pageNo;
    this.pageSize = other.pageSize;
    this.search = other.search;
    this.sortOrder = other.sortOrder;
    this.entity = other.entity;
    this.entityId = other.entityId;
    this.entityNameOrCode = other.entityNameOrCode;
    this.rowCount = other.rowCount;
  }

  @JsonIgnore
  public int getOffset() {
    return n2e(pageNo) * getLimit();
  }

  @JsonIgnore
  public int getLimit() {
    return ifNull(pageSize, DEFAULT_PAGE_SIZE);
  }

  @JsonIgnore
  public String getNormalizedSearch() {
    return ifNotNull(getSearch(), String::toUpperCase);
  }

  @JsonIgnore
  public String getSortColumnLabel() {
    return abr.read(getSortColumn(), "label");
  }

  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public int getLastPage() {
    if (getRowCount() == -1) {
      return -1;
    }
    return NumberMethods.getLastPage(getRowCount(), ifNull(pageSize, DEFAULT_PAGE_SIZE));
  }

  public abstract Enum<?> getSortColumn();

  public Integer getPageNo() {
    return pageNo;
  }

  public void setPageNo(Integer pageNo) {
    if (n2e(pageNo) > 0) {
      this.pageNo = pageNo;
    }
  }

  public Integer getPageSize() {
    return pageSize;
  }

  public void setPageSize(Integer pageSize) {
    if (n2e(pageSize) != DEFAULT_PAGE_SIZE) {
      this.pageSize = pageSize;
    }
  }

  public String getSearch() {
    return search;
  }

  public void setSearch(String search) {
    this.search = ifBlank(search, null);
  }

  public SortOrder getSortOrder() {
    return sortOrder;
  }

  public void setSortOrder(SortOrder sortOrder) {
    this.sortOrder = sortOrder;
  }

  public Entity getEntity() {
    return entity;
  }

  public void setEntity(Entity entity) {
    this.entity = entity;
  }

  public Integer getEntityId() {
    return entityId;
  }

  public void setEntityId(Integer entityId) {
    if (n2e(entityId) != 0) {
      this.entityId = entityId;
    }
  }

  public String getEntityNameOrCode() {
    return entityNameOrCode;
  }

  public void setEntityNameOrCode(String entityNameOrCode) {
    this.entityNameOrCode = ifBlank(entityNameOrCode, null);
  }

  // Not (meant to be) set by Jackson
  public void injectRowCount(int rowCount) {
    this.rowCount = rowCount;
    /*
     * The reason for the following code is that a user has navigated to, say, page 10, and then
     * enters a search criterion that reduces the number of returned rows to less than 100. Then
     * that page suddenly finds itself in deep space. This code prevents that.
     */
    pageNo = Math.min(n2e(pageNo), getLastPage());
    pageNo = nullIf(pageNo, equalTo(), 0);
  }

  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public int getRowCount() {
    // Make clear row count has not been injected yet
    return ifNull(rowCount, -1);
  }
}
