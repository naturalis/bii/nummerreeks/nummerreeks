package nl.naturalis.bcd.search;

public class StorageLocationSearchRequest extends SearchRequest {

  private StorageLocationSortColumn sortColumn;

  public StorageLocationSortColumn getSortColumn() {
    return sortColumn;
  }

  public void setSortColumn(StorageLocationSortColumn sortColumn) {
    this.sortColumn = sortColumn;
  }
}
