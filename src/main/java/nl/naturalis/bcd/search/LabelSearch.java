package nl.naturalis.bcd.search;

import static nl.naturalis.common.ArrayMethods.implode;
import static nl.naturalis.common.NumberMethods.isPlainInt;
import static nl.naturalis.common.StringMethods.trim;

// See src/main/resources/html/Series.listUsingLabel.sql
public class LabelSearch {

  /*
   * The number segment of the search phrase, which MUST be the last segment in order to be picked
   * up as such.
   */
  private final int number;

  /*
   * text is everything up to the number if the last segment can be parsed into an integer, or the
   * entite search phrase if the last segment in not an integer. This allows us to catch collection
   * codes like ACA.A, which unfortunately also contain a dot.
   */
  private final String text;

  /*
   * If the search phrase has just 1 non-integer segment, we compare both the instituteCode column
   * AND the collectionCode column with this value, so the name "institute" is somewhat misleading.
   */
  private final String institute;

  /*
   * If the search contains two or more non-integer segements, the first segment is interpreted as
   * the instituteCode while the re-join of the other segments is taken to be the collectionCode
   * (e.g. "ACA.P").
   */
  private final String collection;

  public LabelSearch(int number) {
    this.number = number;
    text = null;
    institute = null;
    collection = null;
  }

  public LabelSearch(String searchPhrase) {
    searchPhrase = trim(searchPhrase, ".");
    String[] parts = searchPhrase.split("\\.");
    int x = parts.length;
    if (isPlainInt(parts[x - 1])) {
      number = Integer.parseInt(parts[x - 1]);
      text = x == 1 ? null : implode(parts, ".", 0, x - 1);
      if (x == 1) {
        institute = null;
        collection = null;
      } else if (x == 2) {
        institute = parts[0];
        collection = null;
      } else if (x == 3) {
        institute = parts[0];
        collection = parts[1];
      } else {
        institute = parts[0];
        collection = implode(parts, ".", 1, x - 1);
      }
    } else {
      number = -1;
      text = searchPhrase;
      if (x == 1) {
        institute = parts[0];
        collection = null;
      } else if (x == 2) {
        institute = parts[0];
        collection = parts[1];
      } else {
        institute = parts[0];
        collection = implode(parts, ".", 1, x);
      }
    }
  }

  public int getNumber() {
    return number;
  }

  public String getText() {
    return text;
  }

  public String getInstitute() {
    return institute;
  }

  public String getCollection() {
    return collection;
  }
}
