package nl.naturalis.bcd.search;

public class CmsSearchRequest extends SearchRequest {

  private CmsSortColumn sortColumn;

  public CmsSortColumn getSortColumn() {
    return sortColumn;
  }

  public void setSortColumn(CmsSortColumn sortColumn) {
    this.sortColumn = sortColumn;
  }
}
