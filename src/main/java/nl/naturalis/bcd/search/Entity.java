package nl.naturalis.bcd.search;

import java.util.function.UnaryOperator;
import com.fasterxml.jackson.annotation.JsonCreator;
import nl.naturalis.common.util.EnumParser;

public enum Entity {
  CMS("Collectie Management Systeem", "Collectie Management Systemen"),
  COLLECTION("Collectie", "Collecties"),
  DEPARTMENT("Afdeling", "Afdelingen"),
  INSTITUTE("Instituut", "Instituten"),
  NUMBER_SERIES("Nummerreeks", "Nummerreeksen", "requestId"),
  REQUESTER("Aanvrager", "Aanvragers"),
  STORAGE_LOCATION("Storage Location", "Storage Locations", "storageLocationId"),
  LAYOUT("Formaat", "Formaten"),
  USER("Gebruiker", "Gebruikers");

  private static final EnumParser<Entity> ep =
      new EnumParser<>(Entity.class, UnaryOperator.identity());

  @JsonCreator
  public static Entity parse(String value) {
    return ep.parse(value);
  }

  private final String description;
  private final String plural;
  private final String primaryKey;

  private Entity(String description, String plural, String primaryKey) {
    this.description = description;
    this.plural = plural;
    this.primaryKey = primaryKey;
  }

  private Entity(String description, String plural) {
    this.description = description;
    this.plural = plural;
    this.primaryKey = name().toLowerCase() + "Id";
  }

  public String toString() {
    return name();
  }

  public String getDescription() {
    return description;
  }

  public String getPlural() {
    return plural;
  }

  public String getPrimaryKey() {
    return primaryKey;
  }
}
