package nl.naturalis.bcd.search;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import nl.naturalis.common.util.EnumParser;

public enum StorageLocationSortColumn {
  TOP_DESK_ID("a.topDeskId"),
  FILE_NAME("a.fileName"),
  PREFIXES("a.prefixes"),
  DATE_MODIFIED("a.dateModified"),
  REQUESTER_NAME("b.requesterName"),
  DISPENSE_COUNT("a.dispenseCount");

  private static final EnumParser<StorageLocationSortColumn> ep =
      new EnumParser<>(StorageLocationSortColumn.class);

  @JsonCreator
  public static StorageLocationSortColumn parse(Object value) {
    return ep.parse(value);
  }

  private final String label;

  StorageLocationSortColumn(String label) {
    this.label = label;
  }

  @JsonValue
  public String toString() {
    return name();
  }

  public String getLabel() {
    return label;
  }
}
