package nl.naturalis.bcd.search;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import nl.naturalis.common.util.EnumParser;

public enum UserSortColumn {
  USER_NAME("userName"),
  ROLE("role"),
  REQUEST_COUNT("requestCount"),
  NUMBER_COUNT("numberCount"),
  ACTIVE("active");

  private static final EnumParser<UserSortColumn> ep = new EnumParser<>(UserSortColumn.class);

  @JsonCreator
  public static UserSortColumn parse(Object value) {
    return ep.parse(value);
  }

  private final String label;

  UserSortColumn(String label) {
    this.label = label;
  }

  @JsonValue
  public String toString() {
    return name();
  }

  public String getLabel() {
    return label;
  }
}
