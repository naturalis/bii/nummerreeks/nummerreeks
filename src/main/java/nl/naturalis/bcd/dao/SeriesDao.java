package nl.naturalis.bcd.dao;

import static java.util.stream.Collectors.toList;
import static nl.naturalis.bcd.dao.DaoUtil.logSQL;
import static nl.naturalis.bcd.dao.SeriesDaoHelper.countRowsLabelSearch;
import static nl.naturalis.bcd.dao.SeriesDaoHelper.countRowsNormalSearch;
import static nl.naturalis.bcd.dao.SeriesDaoHelper.labelSearch;
import static nl.naturalis.bcd.dao.SeriesDaoHelper.normalSearch;
import static nl.naturalis.bcd.model.RequestStatus.CLOSED;
import static nl.naturalis.common.ObjectMethods.e2n;
import static nl.naturalis.common.ObjectMethods.ifNotNull;
import static nl.naturalis.common.StringMethods.trim;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import nl.naturalis.bcd.managed.Database;
import nl.naturalis.bcd.model.NumberSeries;
import nl.naturalis.bcd.model.RequestStatus;
import nl.naturalis.bcd.search.LabelSearch;
import nl.naturalis.bcd.search.SearchRequest;
import nl.naturalis.bcd.search.SeriesSearchRequest;
import nl.naturalis.common.ExceptionMethods;
import org.klojang.db.Row;
import org.klojang.db.SQLQuery;
import org.klojang.db.SQLUpdate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SeriesDao extends CRUDDao<NumberSeries> {

  private static final Logger LOG = LoggerFactory.getLogger(SeriesDao.class);

  @SuppressWarnings("unused")
  private static final Pattern LABEL_PATTERN =
      Pattern.compile("([A-Z]{1,6})(\\.([A-Z.]{1,6}))?(\\.([0-9]{1,10}))");


  public int countRows(SearchRequest searchRequest, int sessionUserId) {
    SeriesSearchRequest ssr = (SeriesSearchRequest) searchRequest;
    String search = e2n(ifNotNull(ssr.getNormalizedSearch(), String::strip));
    if (search == null) {
      return countRowsNormalSearch(ssr, sessionUserId);
    } else if (search.startsWith("\"") && search.endsWith("\"")) {
      SeriesSearchRequest copy = new SeriesSearchRequest(ssr);
      copy.setSearch(trim(copy.getSearch(), "\""));
      return countRowsNormalSearch(copy, sessionUserId);
    } else if (search.startsWith("'") && search.endsWith("'")) {
      SeriesSearchRequest copy = new SeriesSearchRequest(ssr);
      copy.setSearch(trim(copy.getSearch(), "'"));
      return countRowsNormalSearch(copy, sessionUserId);
    } else if (search.contains(".")) {
      return countRowsLabelSearch(ssr, new LabelSearch(search), sessionUserId);
    }
    return countRowsNormalSearch(ssr, sessionUserId);
  }

  public List<Row> list(SearchRequest searchRequest, int sessionUserId) {
    SeriesSearchRequest ssr = (SeriesSearchRequest) searchRequest;
    String search = e2n(ifNotNull(ssr.getNormalizedSearch(), String::strip));
    if (search == null) {
      return normalSearch(ssr, sessionUserId);
    } else if (search.startsWith("\"") && search.endsWith("\"")) {
      SeriesSearchRequest copy = new SeriesSearchRequest(ssr);
      copy.setSearch(trim(copy.getSearch(), "\""));
      return normalSearch(copy, sessionUserId);
    } else if (search.startsWith("'") && search.endsWith("'")) {
      SeriesSearchRequest copy = new SeriesSearchRequest(ssr);
      copy.setSearch(trim(copy.getSearch(), "'"));
      return normalSearch(copy, sessionUserId);
    } else if (search.contains(".")) {
      LabelSearch ls = new LabelSearch(search);
      return labelSearch(ssr, ls, sessionUserId);
    }
    return normalSearch(ssr, sessionUserId);
  }

  public Optional<Row> view(int id) {
    try (Connection con = Database.pool().getConnection()) {
      try (SQLQuery query = getSQL("view", getQueries()).prepareQuery(con)) {
        logSQL(LOG, query.getSQL());
        return query.bind("id", id).getMappifier().mappify();
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  public RequestStatus getStatus(int id) {
    try (Connection con = Database.pool().getConnection()) {
      try (SQLQuery query = getSQL("getStatus", getQueries()).prepareQuery(con)) {
        logSQL(LOG, query.getSQL());
        return query.bind("id", id).lookup(RequestStatus.class);
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  public void updateStatus(NumberSeries series, int sessionUserId) {
    LocalDateTime now = LocalDateTime.now();
    series.setDateModified(now);
    series.setModifiedBy(sessionUserId);
    if (series.getStatus() == CLOSED) {
      if (series.getDateFirstClosed() == null) {
        series.setDateFirstClosed(now);
      }
      series.setDispenseCount(series.getDispenseCount() + 1);
    }
    try (Connection con = Database.pool().getConnection()) {
      try (SQLUpdate query = getSQL("updateStatus", getQueries()).prepareUpdate(con)) {
        logSQL(LOG, query.getSQL());
        query.bind(series).execute();
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  public List<String> findOverlap(NumberSeries series) {
    try (Connection con = Database.pool().getConnection()) {
      List<Row> rows = findOverlap(con, series);
      return rows.stream().map(r -> r.getString("topDeskId")).collect(toList());
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  // Used by data migration code
  public List<Row> findOverlap(Connection con, NumberSeries series) {
    try (SQLQuery query = getSQL("findOverlap", getQueries()).prepareQuery(con)) {
      logSQL(LOG, query.getSQL());
      return query.bind(series).getMappifier().mappifyAll();
    }
  }

  public int getMinFirstNumber(int collectionId, Integer curFirstNumber) {
    try (Connection con = Database.pool().getConnection()) {
      try (SQLQuery query = getSQL("minFirstNumber", getQueries()).prepareQuery(con)) {
        logSQL(LOG, query.getSQL());
        return query.bind("collectionId", collectionId)
            .bind("firstNumber", curFirstNumber)
            .getInt();
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  @Override
  Queries getQueries() {
    return Queries.series();
  }
}
