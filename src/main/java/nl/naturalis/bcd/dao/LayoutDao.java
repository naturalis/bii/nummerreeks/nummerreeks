package nl.naturalis.bcd.dao;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.klojang.db.Row;
import org.klojang.db.SQL;
import org.klojang.db.SQLInsert;
import org.klojang.db.SQLQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.core.type.TypeReference;
import nl.naturalis.bcd.exception.BcdUserInputException;
import nl.naturalis.bcd.json.BcdModule;
import nl.naturalis.bcd.managed.Database;
import nl.naturalis.bcd.model.Layout;
import nl.naturalis.common.ExceptionMethods;
import static java.util.stream.Collectors.toMap;
import static nl.naturalis.bcd.dao.DaoUtil.logSQL;

public class LayoutDao extends CRUDDao<Layout> {

  private static final Logger LOG = LoggerFactory.getLogger(CRUDDao.class);

  public LayoutDao() {}

  public String getConfig(String layoutName) {
    try (Connection con = Database.pool().getConnection()) {
      SQL sql = getSQL("getConfig", getQueries());
      try (SQLQuery query = sql.prepareQuery(con)) {
        logSQL(LOG, query.getSQL());
        return query.bind("layoutName", layoutName).getString();
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  @Override
  Queries getQueries() {
    return Queries.layout();
  }

  // Returns the id of an arbitrary layout
  public int findAny() {
    try (Connection con = Database.pool().getConnection()) {
      SQL sql = getSQL("findAny", getQueries());
      try (SQLQuery query = sql.prepareQuery(con)) {
        logSQL(LOG, query.getSQL());
        return query.getInt();
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  public int countUsages(Layout layout) {
    try (Connection con = Database.pool().getConnection()) {
      SQL sql = getSQL("countUsages", getQueries());
      try (SQLQuery query = sql.prepareQuery(con)) {
        logSQL(LOG, query.getSQL());
        return query.bind(layout).getInt();
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  public void exportLayouts(OutputStream out) {
    try (Connection con = Database.pool().getConnection()) {
      SQL sql = SQL.create("SELECT * FROM bcd_layout");
      try (SQLQuery query = sql.prepareQuery(con)) {
        logSQL(LOG, query.getSQL());
        List<Row> rows = query.getMappifier().mappifyAll();
        for (Row row : rows) {
          // Store a pretty version of the json config along with the raw
          // version, in case users want to peek into the file before
          // re-importing it.
          String jsonConfig = row.getString("jsonConfig");
          Map<String, Object> deserialized = BcdModule.readJson(jsonConfig);
          row.addColumn("configuration", deserialized);
        }
        String str = BcdModule.writePrettyJson(rows);
        out.write(str.getBytes(StandardCharsets.UTF_8));
      }
    } catch (Exception e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  public void importFormats(byte[] data, String importType, String fileName) {
    List<Layout> layouts;
    try {
      layouts = BcdModule.OBJECT_MAPPER.readValue(data, new TypeReference<List<Layout>>() {});
      layouts.forEach(l -> l.setImportedFrom(fileName));
    } catch (IOException e) {
      throw new BcdUserInputException(
          "File kon niet verwerkt worden. Foutmelding: " + e.getMessage());
    }
    if (importType.equals("upsert")) {
      upsertLayouts(layouts);
    } else {
      replaceLayouts(layouts);
    }
  }

  private static void upsertLayouts(List<Layout> layouts) {
    LOG.debug("Adding/updating {} layouts", layouts.size());
    Collection<Layout> newLayouts;
    try (Connection con = Database.pool().getConnection()) {
      SQL sql = SQL.create("SELECT * FROM bcd_layout");
      try (SQLQuery query = sql.prepareQuery(con)) {
        logSQL(LOG, query.getSQL());
        List<Layout> all = query.getBeanifier(Layout.class, Layout::new).beanifyAll();
        Map<String, Layout> m = all.stream().collect(toMap(Layout::getLayoutName, x -> x));
        layouts.forEach(l -> m.put(l.getLayoutName(), l));
        newLayouts = m.values();
      }
    } catch (Exception e) {
      throw ExceptionMethods.uncheck(e);
    }
    replaceLayouts(newLayouts);
  }

  private static void replaceLayouts(Collection<Layout> layouts) {
    LOG.debug("Inserting {} layouts", layouts.size());
    boolean autocommit;
    try (Connection con = Database.pool().getConnection()) {
      autocommit = con.getAutoCommit();
      con.setAutoCommit(false);
      LOG.debug("Deleting current layouts");
      con.createStatement().executeUpdate("DELETE FROM bcd_layout");
      LOG.debug("Inserting new/updated layouts");
      try (SQLInsert insert =
          SQL.prepareInsert()
              .of(Layout.class)
              .into("bcd_layout")
              .excluding("layoutId")
              .prepare(con)) {
        logSQL(LOG, insert.getSQL());
        insert.insertAll(layouts);
        con.commit();
      } catch (Exception e) {
        con.rollback();
      } finally {
        con.setAutoCommit(autocommit);
      }
    } catch (Exception e) {
      throw ExceptionMethods.uncheck(e);
    }
  }
}
