package nl.naturalis.bcd.dao;

import static nl.naturalis.bcd.dao.DaoUtil.logSQL;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import nl.naturalis.bcd.managed.Database;
import nl.naturalis.bcd.search.SearchRequest;
import nl.naturalis.common.ExceptionMethods;
import nl.naturalis.common.Tuple;
import org.klojang.db.BindInfo;
import org.klojang.db.Row;
import org.klojang.db.SQL;
import org.klojang.db.SQLInsert;
import org.klojang.db.SQLQuery;
import org.klojang.db.SQLUpdate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Base class for all entity DAOs (all except ListBoxDao).
 *
 * @param <MODEL> The model object (a.k.a. entity)
 */
public abstract class CRUDDao<MODEL> {

  private static final Logger LOG = LoggerFactory.getLogger(CRUDDao.class);

  private static final Map<Tuple<Class<?>, String>, SQL> sqlCache = new HashMap<>();

  static final BindInfo BIND_INFO = new BindInfo() {

    @Override
    public boolean bindEnumUsingToString(String enumProperty) {
      return enumProperty.equals("entity");
    }
  };

  public int countRows(SearchRequest searchRequest) {
    try (Connection con = Database.pool().getConnection()) {
      SQL sql = getSQL("count", getQueries(), BIND_INFO);
      try (SQLQuery query = sql.prepareQuery(con)) {
        logSQL(LOG, query.getSQL());
        return query.bind(searchRequest).getInt();
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  public List<Row> list(SearchRequest searchRequest) {
    try (Connection con = Database.pool().getConnection()) {
      SQL sql = getSQL("list", getQueries(), BIND_INFO);
      sql.setOrderBy(searchRequest.getSortColumnLabel(), searchRequest.getSortOrder());
      try (SQLQuery query = sql.prepareQuery(con)) {
        logSQL(LOG, query.getSQL());
        return query.bind(searchRequest).getMappifier().mappifyAll(searchRequest.getLimit());
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  public Optional<MODEL> find(Class<MODEL> modelClass, Supplier<MODEL> instanceSupplier, int id) {
    try (Connection con = Database.pool().getConnection()) {
      return find(con, modelClass, instanceSupplier, id);
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  // Used by data migration code
  public Optional<MODEL> find(Connection con, Class<MODEL> modelClass,
      Supplier<MODEL> instanceSupplier, int id) {
    SQL sql = getSQL("find", getQueries());
    try (SQLQuery query = sql.prepareQuery(con)) {
      logSQL(LOG, query.getSQL());
      return query.bind("id", id).getBeanifier(modelClass, instanceSupplier).beanify();
    }
  }

  public boolean isUnique(MODEL bean) {
    SQL sql = getSQL("unique", getQueries());
    try (Connection con = Database.pool().getConnection()) {
      try (SQLQuery query = sql.prepareQuery(con)) {
        logSQL(LOG, query.getSQL());
        return query.bind(bean).getInt() == 0;
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  public int insert(MODEL model) {
    SQL sql = getSQL("insert", getQueries());
    try (Connection con = Database.pool().getConnection()) {
      try (SQLInsert query = sql.prepareInsert(con)) {
        logSQL(LOG, query.getSQL());
        return (int) query.bind(model).executeAndGetId();
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  public void update(MODEL model) {
    SQL sql = getSQL("update", getQueries());
    try (Connection con = Database.pool().getConnection()) {
      try (SQLUpdate query = sql.prepareUpdate(con)) {
        logSQL(LOG, query.getSQL());
        query.bind(model).execute();
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  public int delete(int id) {
    SQL sql = getSQL("delete", getQueries());
    try (Connection con = Database.pool().getConnection()) {
      try (SQLUpdate query = sql.prepareUpdate(con)) {
        logSQL(LOG, query.getSQL());
        return query.bind("id", id).execute();
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  public int deactivate(int id) {
    SQL sql = getSQL("deactivate", getQueries());
    try (Connection con = Database.pool().getConnection()) {
      try (SQLUpdate query = sql.prepareUpdate(con)) {
        logSQL(LOG, query.getSQL());
        return query.bind("id", id).execute();
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  boolean hasRequests(String foreignKeyColumn, int value) {
    SQL sql = getGenericSQL("hasRequests");
    sql.set("foreignKeyColumn", foreignKeyColumn);
    try (Connection con = Database.pool().getConnection()) {
      try (SQLQuery query = sql.prepareQuery(con)) {
        logSQL(LOG, query.getSQL());
        return query.bind("value", value).getInt() != 0;
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  abstract Queries getQueries();

  SQL getGenericSQL(String name) {
    return getGenericSQL(name, new BindInfo() {});
  }

  SQL getGenericSQL(String name, BindInfo bindInfo) {
    Tuple<Class<?>, String> t = Tuple.of(Queries.generic().getClass(), name);
    SQL sql = sqlCache.get(t);
    if (sql == null) {
      String s = Queries.generic().loadQuery(name);
      sql = SQL.create(s, bindInfo);
      sqlCache.put(t, sql);
    }
    return sql;
  }

  /**
   * Constructs a path to the file name containing the query, loads the file contents and parses it
   * into an SQL instance, and caches the SQL instance using the requesting dao class and the
   * basename of the file as key.
   */
  static SQL getSQL(Class<? extends CRUDDao> daoClass, String name, Queries queries) {
    return getSQL(daoClass, name, queries, new BindInfo() {});
  }

  /**
   * Constructs a path to the file name containing the query, loads the file contents and parses it
   * into an SQL instance, and caches the SQL instance using the requesting dao class and the
   * basename of the file as key.
   */
  static SQL getSQL(Class<? extends CRUDDao> daoClass, String name, Queries queries,
      BindInfo bindInfo) {
    return sqlCache.computeIfAbsent(Tuple.of(daoClass, name),
        k -> SQL.create(queries.loadQuery(name), bindInfo));
  }


  /**
   * Really the same as getSQL, except that a different factory method is used to create the SQL
   * instance: SQL.dynamic(...) instead of SQL.create(...) The difference is that the latter goes on
   * to cache even more stuff, which can't be done in the case of dynamic SQL.
   */
  static SQL getDynamicSQL(Class<? extends CRUDDao> daoClass, String name, Queries queries) {
    return getDynamicSQL(daoClass, name, queries, new BindInfo() {});
  }

  static SQL getDynamicSQL(Class<? extends CRUDDao> daoClass, String name, Queries queries,
      BindInfo bindInfo) {
    return sqlCache.computeIfAbsent(Tuple.of(daoClass, name),
        k -> SQL.dynamic(queries.loadQuery(name), bindInfo));
  }

  SQL getSQL(String name, Queries queries) {
    return getSQL(getClass(), name, queries);
  }

  SQL getSQL(String name, Queries queries, BindInfo bindInfo) {
    return getDynamicSQL(getClass(), name, queries, bindInfo);
  }

  SQL getDynamicSQL(String name, Queries queries) {
    return getDynamicSQL(getClass(), name, queries);
  }

  SQL getDynamicSQL(String name, Queries queries, BindInfo bindInfo) {
    return getDynamicSQL(getClass(), name, queries, bindInfo);
  }

}
