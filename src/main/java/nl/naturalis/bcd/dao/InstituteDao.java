package nl.naturalis.bcd.dao;

import java.sql.Connection;
import java.sql.SQLException;
import org.klojang.db.SQL;
import org.klojang.db.SQLQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.bcd.managed.Database;
import nl.naturalis.bcd.model.Institute;
import nl.naturalis.common.ExceptionMethods;

public class InstituteDao extends CRUDDao<Institute> {

  private static final Logger LOG = LoggerFactory.getLogger(InstituteDao.class);

  /**
   * Returns whether or not there are any number series associated with the specified institute.
   *
   * @param instituteId
   * @return
   */
  public boolean hasRequests(int instituteId) {
    SQL sql = getSQL("hasRequests", getQueries());
    try (Connection con = Database.pool().getConnection()) {
      try (SQLQuery query = sql.prepareQuery(con)) {
        DaoUtil.logSQL(LOG, query.getSQL());
        return query.bind("id", instituteId).getInt() != 0;
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  @Override
  Queries getQueries() {
    return Queries.institute();
  }
}
