package nl.naturalis.bcd.dao;

import nl.naturalis.bcd.managed.Database;
import nl.naturalis.common.ExceptionMethods;
import org.klojang.db.SQL;
import org.klojang.db.SQLQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.bcd.model.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;

import static nl.naturalis.bcd.dao.DaoUtil.logSQL;

public class UserDao extends CRUDDao<User> {

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(UserDao.class);

  public boolean hasRequests(int value) {
    return super.hasRequests("modifiedBy", value);
  }

  public Optional<User> findByUpn(String upn) {
    SQL sql = getSQL("findByUpn", getQueries());
    try (Connection con = Database.pool().getConnection()) {
      try (SQLQuery query = sql.prepareQuery(con)) {
        logSQL(LOG, query.getSQL());
        return query.bind("upn", upn).getBeanifier(User.class).beanify();
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  @Override
  Queries getQueries() {
    return Queries.user();
  }
}
