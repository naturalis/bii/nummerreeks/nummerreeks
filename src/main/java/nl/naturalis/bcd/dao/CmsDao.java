package nl.naturalis.bcd.dao;

import java.sql.Connection;
import java.sql.SQLException;
import org.klojang.db.SQL;
import org.klojang.db.SQLQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.bcd.managed.Database;
import nl.naturalis.bcd.model.Cms;
import nl.naturalis.common.ExceptionMethods;
import static nl.naturalis.bcd.dao.DaoUtil.logSQL;

public class CmsDao extends CRUDDao<Cms> {

  private static final Logger LOG = LoggerFactory.getLogger(CmsDao.class);

  public boolean hasRequests(int value) {
    return super.hasRequests("cmsId", value);
  }

  public int countRequests(int cmsId) {
    SQL sql = getSQL("countRequests", getQueries());
    try (Connection con = Database.pool().getConnection()) {
      try (SQLQuery query = sql.prepareQuery(con)) {
        logSQL(LOG, query.getSQL());
        return query.bind("id", cmsId).getInt();
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  @Override
  Queries getQueries() {
    return Queries.cms();
  }
}
