package nl.naturalis.bcd.dao;

import java.sql.Connection;
import java.sql.SQLException;
import org.klojang.db.SQL;
import org.klojang.db.SQLQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.bcd.managed.Database;
import nl.naturalis.bcd.model.Department;
import nl.naturalis.common.ExceptionMethods;

public class DepartmentDao extends CRUDDao<Department> {

  private static final Logger LOG = LoggerFactory.getLogger(DepartmentDao.class);

  public boolean hasRequests(int value) {
    return super.hasRequests("departmentId", value);
  }

  public int countRequests(int departmentId) {
    SQL sql = getSQL("countRequests", getQueries());
    try (Connection con = Database.pool().getConnection()) {
      try (SQLQuery query = sql.prepareQuery(con)) {
        DaoUtil.logSQL(LOG, query.getSQL());
        return query.bind("id", departmentId).getInt();
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  @Override
  Queries getQueries() {
    return Queries.department();
  }
}
