package nl.naturalis.bcd.model;

import java.time.LocalDateTime;
import java.util.List;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import com.fasterxml.jackson.annotation.JsonIgnore;
import static nl.naturalis.common.ObjectMethods.e2n;

public class StorageLocation {

  @FormDataParam("file")
  private byte[] upload;

  @FormDataParam("file")
  private FormDataContentDisposition fileDetail;

  private String fileName;

  @FormDataParam("storageLocationId")
  private Integer storageLocationId;

  @FormDataParam("topDeskId")
  private String topDeskId;

  @FormDataParam("requesterId")
  private Integer requesterId;

  @FormDataParam("prefixes")
  private String prefixes;

  @FormDataParam("prefixSegmentCount")
  private Byte prefixSegmentCount;

  @FormDataParam("comments")
  private String comments;

  @FormDataParam("status") // TODO: do we really get this from the frontend?
  private RequestStatus status;

  private int dispenseCount;
  private LocalDateTime dateCreated;
  private LocalDateTime dateModified;
  private LocalDateTime dateFirstClosed;
  private Integer modifiedBy;
  private String lastUsedLayout;

  // Denormalizations:
  private String requesterName;
  private List<StorageLabel> labels;

  @JsonIgnore
  public byte[] getUpload() {
    return upload;
  }

  public void setUpload(byte[] upload) {
    this.upload = upload;
  }

  @JsonIgnore
  public FormDataContentDisposition getFileDetail() {
    return fileDetail;
  }

  public void setFileDetail(FormDataContentDisposition fileDetail) {
    this.fileDetail = fileDetail;
  }

  public String getFileName() {
    if (fileName == null && getFileDetail() != null) {
      fileName = getFileDetail().getFileName();
    }
    return e2n(fileName);
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public Integer getStorageLocationId() {
    return storageLocationId;
  }

  public void setStorageLocationId(Integer storageLocationId) {
    this.storageLocationId = storageLocationId;
  }

  public String getTopDeskId() {
    return topDeskId;
  }

  public void setTopDeskId(String topDeskId) {
    this.topDeskId = topDeskId;
  }

  public Integer getRequesterId() {
    return requesterId;
  }

  public void setRequesterId(Integer requesterId) {
    this.requesterId = requesterId;
  }

  @JsonIgnore
  public String getPrefixes() {
    return prefixes;
  }

  public void setPrefixes(String prefixes) {
    this.prefixes = prefixes;
  }

  public Byte getPrefixSegmentCount() {
    return prefixSegmentCount;
  }

  public void setPrefixSegmentCount(Byte prefixSegmentCount) {
    this.prefixSegmentCount = prefixSegmentCount;
  }

  @JsonIgnore
  public RequestStatus getStatus() {
    return status;
  }

  public void setStatus(RequestStatus status) {
    this.status = status;
  }

  public int getDispenseCount() {
    return dispenseCount;
  }

  public void setDispenseCount(int dispenseCount) {
    this.dispenseCount = dispenseCount;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  @JsonIgnore
  public LocalDateTime getDateCreated() {
    return dateCreated;
  }

  public void setDateCreated(LocalDateTime dateCreated) {
    this.dateCreated = dateCreated;
  }

  @JsonIgnore
  public LocalDateTime getDateModified() {
    return dateModified;
  }

  public void setDateModified(LocalDateTime dateModified) {
    this.dateModified = dateModified;
  }

  public LocalDateTime getDateFirstClosed() {
    return dateFirstClosed;
  }

  public void setDateFirstClosed(LocalDateTime dateFirstClosed) {
    this.dateFirstClosed = dateFirstClosed;
  }

  @JsonIgnore
  public Integer getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(Integer modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public String getLastUsedLayout() {
    return lastUsedLayout;
  }

  public void setLastUsedLayout(String lastUsedLayout) {
    this.lastUsedLayout = lastUsedLayout;
  }

  public String getRequesterName() {
    return requesterName;
  }

  public void setRequesterName(String requesterName) {
    this.requesterName = requesterName;
  }

  @JsonIgnore
  public List<StorageLabel> getLabels() {
    return labels;
  }

  public void setLabels(List<StorageLabel> labels) {
    this.labels = labels;
  }

  public String toString() {
    return labels.get(0) + "_" + labels.get(labels.size() - 1);
  }
}
