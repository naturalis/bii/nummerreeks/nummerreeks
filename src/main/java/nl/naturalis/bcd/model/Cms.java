package nl.naturalis.bcd.model;

import static nl.naturalis.common.ObjectMethods.e2n;
import static nl.naturalis.common.ObjectMethods.ifNotNull;

public class Cms {

  private Integer cmsId;
  private String cmsName;
  private boolean active = true;

  public Cms() {}

  public Integer getCmsId() {
    return cmsId;
  }

  public void setCmsId(Integer cmsId) {
    this.cmsId = cmsId;
  }

  public String getCmsName() {
    return e2n(ifNotNull(cmsName, String::strip));
  }

  public void setCmsName(String cmsName) {
    this.cmsName = cmsName;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }
}
