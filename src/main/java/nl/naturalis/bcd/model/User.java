package nl.naturalis.bcd.model;

import static nl.naturalis.common.ObjectMethods.e2n;
import static nl.naturalis.common.ObjectMethods.ifNotNull;

@SuppressWarnings("unused")
public class User {

  private Integer userId;

  private String upn;

  private String firstName;
  private String lastName;
  @Deprecated
  private String userName;
  @Deprecated
  private String password;
  private UserRole role;
  private boolean active = true;

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public String getUpn() {
    return upn;
  }

  public void setUpn(String upn) {
    this.upn = upn;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  @Deprecated
  public String getUserName() {
    return e2n(ifNotNull(userName, String::strip));
  }

  @Deprecated
  public void setUserName(String userName) {
    this.userName = userName;
  }

  @Deprecated
  public String getPassword() {
    return password;
  }

  @Deprecated
  public void setPassword(String password) {
    this.password = password;
  }

  public UserRole getRole() {
    return role;
  }

  public void setRole(UserRole role) {
    this.role = role;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }
  @Override
  public String toString() {
    return "userId: " + userId + ", upn: " + upn + ", firstName: " + firstName + ", lastName: " + lastName + ", role: " + role + ", active: " + active;
  }
}
