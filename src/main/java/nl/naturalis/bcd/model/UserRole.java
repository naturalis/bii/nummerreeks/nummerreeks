package nl.naturalis.bcd.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import nl.naturalis.common.util.EnumParser;

public enum UserRole {
  VIEWER("Viewer"),
  USER("User"),
  ADMIN("Admin");

  private static final EnumParser<UserRole> ep = new EnumParser<>(UserRole.class);

  @JsonCreator
  public static UserRole parse(Object value) {
    return ep.parse(value);
  }

  private final String name;

  UserRole(String name) {
    this.name = name;
  }

  @JsonValue
  public int intValue() {
    return ordinal();
  }

  public String getName() {
    return name;
  }

  @Override
  public String toString() {
    return name;
  }
}
