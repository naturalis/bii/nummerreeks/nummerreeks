package nl.naturalis.bcd.model;

import static nl.naturalis.common.ObjectMethods.e2n;
import static nl.naturalis.common.ObjectMethods.ifNotNull;

public class Institute {

  // Institute code for dummy institute for overlapping / duplicate number series
  public static final String OVERLAP_CODE = "OVERLAP";
  // Institute code for dummy institute for corrupt number series
  public static final String CORRUPT_CODE = "CORRUPT";

  private Integer instituteId;
  private String instituteName;
  private String instituteCode;
  private boolean active = true;

  public Institute() {
  }

  public Integer getInstituteId() {
    return instituteId;
  }

  public void setInstituteId(Integer instituteId) {
    this.instituteId = instituteId;
  }

  public String getInstituteName() {
    return e2n(ifNotNull(instituteName, String::strip));
  }

  public void setInstituteName(String instituteName) {
    this.instituteName = instituteName;
  }

  public String getInstituteCode() {
    return e2n(ifNotNull(instituteCode, String::strip));
  }

  public void setInstituteCode(String instituteCode) {
    this.instituteCode = instituteCode;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }
}
