package nl.naturalis.bcd.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Corresponds to the bcd_layout table.
 *
 * @author Ayco Holleman
 */
public class Layout {

  private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d MMM uuuu HH:mm");

  private Integer layoutId;
  private String layoutName;
  private LayoutTemplate template;
  private LayoutInputType inputType;
  private LabelUsage labelUsage;
  private LocalDateTime dateModified;
  private String importedFrom;
  private String jsonConfig;

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder(50);
    sb.append(layoutName).append(" (");
    if (importedFrom != null) {
      sb.append("in ").append(importedFrom).append(", ");
    }
    sb.append("versie ").append(dtf.format(dateModified)).append(")");
    return sb.toString();
  }

  public Integer getLayoutId() {
    return layoutId;
  }

  public void setLayoutId(Integer layoutId) {
    this.layoutId = layoutId;
  }

  public String getLayoutName() {
    return layoutName;
  }

  public void setLayoutName(String layoutName) {
    this.layoutName = layoutName;
  }

  public LayoutTemplate getTemplate() {
    return template;
  }

  public void setTemplate(LayoutTemplate template) {
    this.template = template;
  }

  public LayoutInputType getInputType() {
    return inputType;
  }

  public void setInputType(LayoutInputType inputType) {
    this.inputType = inputType;
  }

  public LabelUsage getLabelUsage() {
    return labelUsage;
  }

  public void setLabelUsage(LabelUsage usage) {
    this.labelUsage = usage;
  }

  public LocalDateTime getDateModified() {
    return dateModified;
  }

  public void setDateModified(LocalDateTime dateModified) {
    this.dateModified = dateModified;
  }

  public String getImportedFrom() {
    return importedFrom;
  }

  public void setImportedFrom(String importedFrom) {
    this.importedFrom = importedFrom;
  }

  public String getJsonConfig() {
    return jsonConfig;
  }

  public void setJsonConfig(String jsonConfig) {
    this.jsonConfig = jsonConfig;
  }
}
