package nl.naturalis.bcd.model;

import static nl.naturalis.common.ObjectMethods.e2n;
import static nl.naturalis.common.ObjectMethods.ifNotNull;

public class Department {

  private Integer departmentId;
  private String departmentName;
  private boolean active = true;

  public Department() {}

  public Integer getDepartmentId() {
    return departmentId;
  }

  public void setDepartmentId(Integer departmentId) {
    this.departmentId = departmentId;
  }

  public String getDepartmentName() {
    return e2n(ifNotNull(departmentName, String::strip));
  }

  public void setDepartmentName(String departmentName) {
    this.departmentName = departmentName;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }
}
