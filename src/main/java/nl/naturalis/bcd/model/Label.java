package nl.naturalis.bcd.model;

public class Label {

  private float top;
  private float left;
  private String text;
  private float fontSize;
  private String dataMatrix;

  public float getTop() {
    return top;
  }

  public float getLeft() {
    return left;
  }

  public String getText() {
    return text;
  }

  public float getFontSize() {
    return fontSize;
  }

  public String getQrCode() {
    return dataMatrix;
  }

  public void setTopMM(float top) {
    this.top = top;
  }

  public void setLeftMM(float left) {
    this.left = left;
  }

  public void setText(String text) {
    this.text = text;
  }

  public void setFontSize(float fontSize) {
    this.fontSize = fontSize;
  }

  public void setQrCode(String dataMatrix) {
    this.dataMatrix = dataMatrix;
  }
}
