package nl.naturalis.bcd.model;

public enum LabelUsage {
  SPECIMEN("Specimens"),
  STORAGE_UNIT("Storage Unit"),
  STORAGE_LOCATION("Storage Location"),
  LABORATORY("Laboratorium"),
  ENTOMOLOGY("Entomologie");

  private final String description;

  LabelUsage(String descr) {
    this.description = descr;
  }

  public String getDescription() {
    return description;
  }

  public String toString() {
    return description;
  }
}
