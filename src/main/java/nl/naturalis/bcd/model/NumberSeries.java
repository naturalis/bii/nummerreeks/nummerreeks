package nl.naturalis.bcd.model;

import java.time.LocalDateTime;
import nl.naturalis.common.StringMethods;
import static nl.naturalis.common.ObjectMethods.e2n;
import static nl.naturalis.common.ObjectMethods.ifNotNull;

/**
 * The central model class in this application. Unfortunately also the only one whose name does not
 * map straight to the table it is bbased on. It maps to the bcd_request table.
 *
 * @author Ayco Holleman
 */
public class NumberSeries {

  public static String getLabel(String instituteCode, String collectionCode, Integer number) {
    if (number == null) {
      return StringMethods.EMPTY;
    }
    return BcdCollection.getPrefix(instituteCode, collectionCode) + "." + number;
  }

  private Integer requestId;
  private String topDeskId;
  private Integer requesterId;
  private Integer departmentId;
  private Integer cmsId;
  private Integer collectionId;
  private Integer firstNumber;
  private Integer lastNumber;
  private Integer amount;
  private String comments;
  private RequestStatus status;
  private Integer dispenseCount;
  private LocalDateTime dateCreated;
  private LocalDateTime dateModified;
  private Integer modifiedBy;
  private LocalDateTime dateFirstClosed;
  private String lastUsedLayout;

  // Denormalizations:
  private Integer instituteId;
  private String collectionCode;
  private String instituteCode;

  public NumberSeries() {}

  public NumberSeries(NumberSeries other) {
    requestId = other.requestId;
    topDeskId = other.topDeskId;
    requesterId = other.requesterId;
    departmentId = other.departmentId;
    cmsId = other.cmsId;
    collectionId = other.collectionId;
    firstNumber = other.firstNumber;
    lastNumber = other.lastNumber;
    amount = other.amount;
    comments = other.comments;
    status = other.status;
    dispenseCount = other.dispenseCount;
    dateCreated = other.dateCreated;
    dateModified = other.dateModified;
    modifiedBy = other.modifiedBy;
    dateFirstClosed = other.dateFirstClosed;
    instituteId = other.instituteId;
    collectionCode = other.collectionCode;
    instituteCode = other.instituteCode;
  }

  public String description() {
    return prefix() + "[" + firstNumber + " - " + lastNumber + "]";
  }

  @Override
  public String toString() {
    return description();
  }

  public String prefix() {
    if (requestId == null) {
      // This is a fresh instance, not populated yet with database or form data
      return null;
    }
    return BcdCollection.getPrefix(instituteCode, collectionCode);
  }

  public String firstLabel() {
    if (requestId == null) {
      return null;
    }
    return getLabel(instituteCode, collectionCode, firstNumber);
  }

  public String lastLabel() {
    if (requestId == null) {
      return null;
    }
    return getLabel(instituteCode, collectionCode, lastNumber);
  }

  public Integer getRequestId() {
    return requestId;
  }

  public void setRequestId(Integer requestId) {
    this.requestId = requestId;
  }

  public String getTopDeskId() {
    return e2n(ifNotNull(topDeskId, String::strip));
  }

  public void setTopDeskId(String topDeskId) {
    this.topDeskId = topDeskId;
  }

  public Integer getRequesterId() {
    return requesterId;
  }

  public void setRequesterId(Integer requesterId) {
    this.requesterId = requesterId;
  }

  public Integer getDepartmentId() {
    return departmentId;
  }

  public void setDepartmentId(Integer departmentId) {
    this.departmentId = departmentId;
  }

  public Integer getCmsId() {
    return cmsId;
  }

  public void setCmsId(Integer cmsId) {
    this.cmsId = cmsId;
  }

  public Integer getCollectionId() {
    return collectionId;
  }

  public void setCollectionId(Integer collectionId) {
    this.collectionId = collectionId;
  }

  public Integer getFirstNumber() {
    return firstNumber;
  }

  public void setFirstNumber(Integer firstNumber) {
    this.firstNumber = firstNumber;
  }

  public Integer getLastNumber() {
    return lastNumber;
  }

  public void setLastNumber(Integer lastNumber) {
    this.lastNumber = lastNumber;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

  public String getComments() {
    return e2n(ifNotNull(comments, String::strip));
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public RequestStatus getStatus() {
    return status;
  }

  public void setStatus(RequestStatus status) {
    this.status = status;
  }

  public Integer getDispenseCount() {
    return dispenseCount;
  }

  public void setDispenseCount(Integer dispenseCount) {
    this.dispenseCount = dispenseCount;
  }

  public LocalDateTime getDateCreated() {
    return dateCreated;
  }

  public void setDateCreated(LocalDateTime dateCreated) {
    this.dateCreated = dateCreated;
  }

  public LocalDateTime getDateModified() {
    return dateModified;
  }

  public void setDateModified(LocalDateTime dateModified) {
    this.dateModified = dateModified;
  }

  public Integer getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(Integer modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public LocalDateTime getDateFirstClosed() {
    return dateFirstClosed;
  }

  public void setDateFirstClosed(LocalDateTime dateFirstClosed) {
    this.dateFirstClosed = dateFirstClosed;
  }

  public String getLastUsedLayout() {
    return lastUsedLayout;
  }

  public void setLastUsedLayout(String lastUsedLayoutId) {
    this.lastUsedLayout = lastUsedLayoutId;
  }

  public Integer getInstituteId() {
    return instituteId;
  }

  public void setInstituteId(Integer instituteId) {
    this.instituteId = instituteId;
  }

  public String getCollectionCode() {
    if (requestId == null) {
      return null;
    }
    return BcdCollection.getNormalizedCollectionCode(instituteCode, collectionCode);
  }

  public void setCollectionCode(String collectionCode) {
    this.collectionCode = collectionCode;
  }

  public String getInstituteCode() {
    return instituteCode;
  }

  public void setInstituteCode(String instituteCode) {
    this.instituteCode = instituteCode;
  }
}
