package nl.naturalis.bcd.exception;

/**
 * Base class for runtime exceptions within the BCD code base.
 *
 * @author Ayco Holleman
 */
public class BcdRuntimeException extends RuntimeException {

  public BcdRuntimeException() {}

  public BcdRuntimeException(String message) {
    super(message);
  }

  public BcdRuntimeException(Throwable cause) {
    super(cause);
  }

  public BcdRuntimeException(String message, Throwable cause) {
    super(message, cause);
  }
}
