package nl.naturalis.bcd.exception;

/**
 * Exception thrown specifically in response to invalid input from the user.
 *
 * @author Ayco Holleman
 */
public class BcdUserInputException extends BcdUserException {

  public BcdUserInputException(String message) {
    super(message);
  }
}
