package nl.naturalis.bcd.exception;

public class NotAuthorizedException extends BcdRuntimeException {

  public static NotAuthorizedException notAuthorized(String message) {
    return new NotAuthorizedException(message);
  }

  public NotAuthorizedException() {
    super();
  }

  public NotAuthorizedException(String message) {
    super(message);
  }
}
