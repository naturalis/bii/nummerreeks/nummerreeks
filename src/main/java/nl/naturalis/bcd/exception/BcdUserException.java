package nl.naturalis.bcd.exception;

/**
 * Base class for runtime exceptions that represent well-defined, anticipated error conditions that
 * were nevertheless not turned into checked exceptions. Exceptions of this kind should not result
 * in error pages with "scary" stack traces. Only the error messages should be displayed.
 *
 * @author Ayco Holleman
 */
public class BcdUserException extends BcdRuntimeException {

  public BcdUserException(String message) {
    super(message);
  }
}
