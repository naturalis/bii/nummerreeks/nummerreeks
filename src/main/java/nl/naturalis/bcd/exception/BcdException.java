package nl.naturalis.bcd.exception;

/**
 * Base class for checked exceptions within the BCD code base.
 *
 * @author Ayco Holleman
 */
public class BcdException extends Exception {

  public BcdException(String message) {
    super(message);
  }

  public BcdException(Throwable cause) {
    super(cause);
  }

  public BcdException(String message, Throwable cause) {
    super(message, cause);
  }
}
