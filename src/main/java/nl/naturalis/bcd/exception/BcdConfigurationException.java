package nl.naturalis.bcd.exception;

public class BcdConfigurationException extends BcdException {

  public BcdConfigurationException(String message) {
    super(message);
  }

  public BcdConfigurationException(Throwable cause) {
    super(cause);
  }

  public BcdConfigurationException(String message, Throwable cause) {
    super(message, cause);
  }
}
