package nl.naturalis.bcd.exception;

import nl.naturalis.bcd.search.Entity;
import static nl.naturalis.common.StringMethods.concat;

public class DuplicateKeyException extends BcdUserInputException {

  public DuplicateKeyException(Entity entity, String uniqueKeyName, Object uniqueKeyValue) {
    super(createMessage(entity, uniqueKeyName, uniqueKeyValue));
  }

  private static String createMessage(Entity entity, String uniqueKeyName, Object uniqueKeyValue) {
    String quote = uniqueKeyValue.getClass() == String.class ? "\"" : "";
    return concat(
        "Er bestaat al een ",
        entity.getDescription().toLowerCase(),
        " met ",
        uniqueKeyName,
        " ",
        quote,
        uniqueKeyValue,
        quote);
  }
}
