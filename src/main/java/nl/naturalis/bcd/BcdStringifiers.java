package nl.naturalis.bcd;

import static nl.naturalis.common.ObjectMethods.ifNotEmpty;
import static nl.naturalis.common.StringMethods.EMPTY;
import static nl.naturalis.common.StringMethods.ellipsis;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import nl.naturalis.bcd.model.RequestStatus;
import nl.naturalis.bcd.model.UserRole;
import org.klojang.template.StringifierRegistry;

/**
 * Factory class for a klojang StringifierFactory instance.
 */
public class BcdStringifiers {

  private static final DateTimeFormatter DATE_FORMAT0 =
      DateTimeFormatter.ofPattern("uuuu/MM/dd'&nbsp;'HH:mm");

  public static StringifierRegistry BCD_STRINGIFIERS = StringifierRegistry.configure()
      .registerByType(str::dateFormat0, LocalDateTime.class)
      .registerByName(str::role, "role")
      .registerByGroup(str::getStatusName, "statusName")
      .registerByGroup(str::getStatusDescription, "statusDescription")
      .registerByGroup(str::notApplicable, "nvt")
      .registerByGroup(obj -> ellipsis(obj, 25), "abbrev25")
      .registerByGroup(str::extractTopDeskId, "topdesk")
      .freeze();

  private static class str {

    /**
     * Stringifies all template variables of type LocalDateTime.
     */
    static String dateFormat0(Object obj) {
      return obj == null ? EMPTY : DATE_FORMAT0.format((LocalDateTime) obj);
    }

    /**
     * Stringifies template variables with the role prefix.
     */
    static String role(Object obj) {
      return obj == null ? EMPTY : UserRole.parse(obj).name();
    }

    /**
     * Stringifies template variables with the statusName prefix.
     */
    static String getStatusName(Object obj) {
      return obj == null ? EMPTY : RequestStatus.parse(obj).name();
    }

    /**
     * Stringifies template variables with the statusDescription prefix.
     */
    static String getStatusDescription(Object obj) {
      return obj == null ? EMPTY : RequestStatus.parse(obj).toString();
    }

    /**
     * Stringifies template variables with the nvt prefix.
     */
    static String notApplicable(Object obj) {
      return ifNotEmpty(obj, Object::toString, "n.v.t.");
    }

    /**
     * Stringifies template variables with the topdesk prefix.
     */
    static String extractTopDeskId(Object obj) {
      return obj == null ? EMPTY : BcdUtil.extractTopDeskId(obj.toString());
    }
  }

  private BcdStringifiers() {
  }
}
