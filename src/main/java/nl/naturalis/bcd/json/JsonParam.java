package nl.naturalis.bcd.json;

public final class JsonParam<T> {

  private final String json;

  public JsonParam() {
    this.json = "{}";
  }

  public JsonParam(String json) {
    this.json = json;
  }

  public T deserialize(Class<T> clazz) {
    return BcdModule.readJson(json, clazz);
  }

  public String getJson() {
    return json;
  }
}
