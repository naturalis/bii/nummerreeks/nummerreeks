package nl.naturalis.bcd.json;

import java.util.HashMap;
import java.util.Map;
import org.klojang.db.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import nl.naturalis.common.ExceptionMethods;

public class BcdModule extends SimpleModule {

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(BcdModule.class);

  public static final ObjectMapper OBJECT_MAPPER = configure(new ObjectMapper());

  private static final Map<Class<?>, ObjectWriter> writers = new HashMap<>();

  private static final Map<Class<?>, ObjectReader> readers = new HashMap<>();

  public static String writeJson(Object obj) {
    try {
      return writers.computeIfAbsent(obj.getClass(), k -> writer(k, false)).writeValueAsString(obj);
    } catch (JsonProcessingException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  public static String writePrettyJson(Object obj) {
    try {
      return writer(obj.getClass(), true).writeValueAsString(obj);
    } catch (JsonProcessingException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  public static <T> T readJson(String json, Class<T> clazz) {
    try {
      return readers.computeIfAbsent(clazz, k -> OBJECT_MAPPER.readerFor(k)).readValue(json);
    } catch (JsonProcessingException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  private static final TypeReference<Map<String, Object>> MAP_TYPE_REF = new TypeReference<>() {};

  public static Map<String, Object> readJson(String json) throws JsonProcessingException {
    return OBJECT_MAPPER.readValue(json, MAP_TYPE_REF);
  }

  public BcdModule() {
    super("BCD");
    addSerializer(Row.class, new RowSerializer());
  }

  private static ObjectMapper configure(ObjectMapper mapper) {
    mapper.enable(MapperFeature.USE_STD_BEAN_NAMING);
    mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

    mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);

    mapper.registerModule(new JavaTimeModule());
    mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

    mapper.registerModule(new BcdModule());

    return mapper;
  }

  private static ObjectWriter writer(Class<?> c, boolean pretty) {
    if (pretty) {
      return OBJECT_MAPPER.writerFor(c).withDefaultPrettyPrinter();
    }
    return OBJECT_MAPPER.writerFor(c);
  }
}
