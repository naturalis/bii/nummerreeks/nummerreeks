package nl.naturalis.bcd.jaxrs;

class ThrowableInfo extends AbstractExceptionInfo<Throwable> {

  @Override
  public boolean isUserError() {
    return false;
  }

  @Override
  public Class<Throwable> getExceptionClass() {
    return Throwable.class;
  }
}
