package nl.naturalis.bcd.jaxrs;

import java.util.Optional;
import org.klojang.db.KJSQLException;

public class KJSQLExceptionInfo extends AbstractExceptionInfo<KJSQLException> {

  @Override
  public boolean isUserError() {
    return false;
  }

  @Override
  public Class<KJSQLException> getExceptionClass() {
    return KJSQLException.class;
  }

  @Override
  public Optional<String> getHeader() {
    return Optional.of("Database Fout");
  }
}
