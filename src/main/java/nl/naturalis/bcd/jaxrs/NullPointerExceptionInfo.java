package nl.naturalis.bcd.jaxrs;

import java.util.Optional;

class NullPointerExceptionInfo extends AbstractExceptionInfo<NullPointerException> {

  public NullPointerExceptionInfo() {
  }

  @Override
  public boolean isUserError() {
    return false;
  }

  @Override
  public Class<NullPointerException> getExceptionClass() {
    return NullPointerException.class;
  }

  @Override
  public Optional<String> getHeader() {
    return Optional.of("Aiiii, je hebt een bug gevonden! (NullPointerException)");
  }
}
