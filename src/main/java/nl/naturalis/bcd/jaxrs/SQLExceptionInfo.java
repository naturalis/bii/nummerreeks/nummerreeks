package nl.naturalis.bcd.jaxrs;

import java.sql.SQLException;
import java.util.Optional;

public class SQLExceptionInfo extends AbstractExceptionInfo<SQLException> {

  @Override
  public boolean isUserError() {
    return false;
  }

  @Override
  public Class<SQLException> getExceptionClass() {
    return SQLException.class;
  }

  @Override
  public Optional<String> getHeader() {
    return Optional.of("Database Fout");
  }
}
