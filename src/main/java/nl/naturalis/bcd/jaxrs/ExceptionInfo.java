package nl.naturalis.bcd.jaxrs;

import java.util.Optional;
import javax.ws.rs.core.Response.Status;

/**
 * Provides info to controller classes about what to display in the error page, and how to display
 * it.
 */
public interface ExceptionInfo<E extends Throwable> {

  boolean isUserError();

  Status getStatus();

  Optional<String> getHeader();

  boolean showStackTrace();

  Class<E> getExceptionClass();
}
