package nl.naturalis.bcd.jaxrs;

import java.util.Optional;
import javax.ws.rs.core.Response.Status;

abstract class AbstractExceptionInfo<E extends Throwable> implements ExceptionInfo<E> {

  @Override
  public Optional<String> getHeader() {
    if (isUserError()) {
      return Optional.empty();
    }
    return Optional.of("Er is een onverwachte fout opgetreden");
  }

  @Override
  public Status getStatus() {
    if (isUserError()) {
      return Status.BAD_REQUEST;
    }
    return Status.INTERNAL_SERVER_ERROR;
  }

  @Override
  public boolean showStackTrace() {
    if (isUserError()) {
      return false;
    }
    return true;
  }
}
