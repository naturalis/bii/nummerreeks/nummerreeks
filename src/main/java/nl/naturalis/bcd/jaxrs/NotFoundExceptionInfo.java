package nl.naturalis.bcd.jaxrs;

import javax.ws.rs.core.Response.Status;
import nl.naturalis.bcd.exception.NotFoundException;

class NotFoundExceptionInfo extends AbstractExceptionInfo<NotFoundException> {

  @Override
  public boolean isUserError() {
    return false;
  }

  @Override
  public Status getStatus() {
    return Status.NOT_FOUND;
  }

  @Override
  public Class<NotFoundException> getExceptionClass() {
    return NotFoundException.class;
  }
}
