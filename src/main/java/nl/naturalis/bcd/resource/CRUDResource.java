package nl.naturalis.bcd.resource;

import nl.naturalis.bcd.dao.CRUDDao;
import nl.naturalis.bcd.json.BcdModule;
import nl.naturalis.bcd.managed.Database;
import nl.naturalis.bcd.model.NumberSeries;
import nl.naturalis.bcd.model.Permission;
import nl.naturalis.bcd.model.StorageLocation;
import nl.naturalis.bcd.search.Entity;
import nl.naturalis.bcd.search.SearchRequest;
import nl.naturalis.common.ExceptionMethods;
import org.klojang.db.KJSQLException;
import org.klojang.db.Row;
import org.klojang.db.SQL;
import org.klojang.db.SQLQuery;
import org.klojang.helpers.PagingDataHelper;
import org.klojang.template.RenderException;
import org.klojang.template.RenderSession;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.jax.rs.annotations.Pac4JProfileManager;
import org.pac4j.oidc.profile.azuread.AzureAdProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;

import static nl.naturalis.bcd.json.BcdModule.writeJson;
import static nl.naturalis.bcd.search.SearchRequest.DEFAULT_PAGE_SIZE;
import static nl.naturalis.common.ObjectMethods.ifEmpty;
import static nl.naturalis.common.ObjectMethods.ifNull;
import static nl.naturalis.common.StringMethods.rchop;

abstract class CRUDResource<MODEL, DAO extends CRUDDao<MODEL>> {

  private static final Logger LOG = LoggerFactory.getLogger(CRUDResource.class);

  final DAO dao;

  private final Class<MODEL> modelClass;

  ProfileManager<AzureAdProfile> pm;
  AzureAdProfile profile;

  CRUDResource(DAO dao, Class<MODEL> modelClass) {
    this.dao = dao;
    this.modelClass = modelClass;
  }

  StreamingOutput list(
          SearchRequest searchRequest,
          HttpServletRequest httpRequest,
          @Pac4JProfileManager ProfileManager<AzureAdProfile> pm)
          throws RenderException {
    this.pm = pm;
    this.profile = getProfile(pm);
    LOG.debug("list (type={};searchRequest={})", typeName(), writeJson(searchRequest));
    ToIntFunction<SearchRequest> rowCountProvider = getRowCountProvider();
    if (rowCountProvider == null) {
      rowCountProvider = dao::countRows;
    }
    int rowCount = rowCountProvider.applyAsInt(searchRequest);
    // We ping-pong the search request between client and server, and the Javascript
    // on the client side needs the row count for its paging magic:
    searchRequest.injectRowCount(rowCount);
    Function<SearchRequest, List<Row>> rowsProvider = getRowsProvider();
    if (rowsProvider == null) {
      rowsProvider = dao::list;
    }
    List<Row> rows = rowsProvider.apply(searchRequest);
    listDataReady(rows, rowCount);
    RenderSession session = newListPageRenderSession();

    Permission permission = hasPermission();
    session.populate("mainTable", rows)
            .set("basePath", getBasePath(httpRequest))
            .set("searchRequest", writeJson(searchRequest))
            .set("permission", permission.name())
            .show(navbar(), "logo", "bsDialog", "sortAscIcon", "sortDescIcon");
    for (RenderSession child : session.getChildSessions("mainTable")) {
      child.show();
    }
    if (rowCount > searchRequest.getLimit()) {
      int pageNo = ifNull(searchRequest.getPageNo(), 0);
      int pageSize = ifNull(searchRequest.getPageSize(), DEFAULT_PAGE_SIZE);
      PagingDataHelper pager = new PagingDataHelper().setRowCount(rowCount)
              .setRowsPerPage(pageSize)
              .setSelectedPage(pageNo);
      session.in("PageButtons").populateWithTuple("page", pager.data()).show();
    }
    beforeRenderListPage(session, searchRequest, rows, rowCount);
    return session::render;
  }

  ToIntFunction<SearchRequest> getRowCountProvider() {
    // Override if necessary
    return null;
  }

  Function<SearchRequest, List<Row>> getRowsProvider() {
    // Override if necessary
    return null;
  }

  Response showRequests(SearchRequest searchRequest,
                        Entity entity,
                        int entityId,
                        String entityNameOrCode,
                        @Pac4JProfileManager ProfileManager<AzureAdProfile> pm) {
    this.pm = pm;
    this.profile = getProfile(pm);
    searchRequest.setEntity(entity);
    searchRequest.setEntityId(entityId);
    searchRequest.setEntityNameOrCode(entityNameOrCode);
    String val = URLEncoder.encode(writeJson(searchRequest), StandardCharsets.UTF_8);
    return Response.seeOther(URI.create("/series/?searchRequest=" + val)).build();
  }

  Response create(
          Supplier<MODEL> model,
          @Context HttpServletRequest httpRequest,
          @Pac4JProfileManager ProfileManager<AzureAdProfile> pm)
          throws RenderException {
    this.pm = pm;
    this.profile = getProfile(pm);
    LOG.debug("create (type={})", typeName());
    // We ping-pong the search request from the list page, so that when the user closes
    // the form, he/she returns to the same page number within the list page. Note,
    // however, that we have no need to deserialize the search request here.
    String searchRequest = ifEmpty(httpRequest.getParameter("searchRequest"), "{}");
    RenderSession session = newEditPageRenderSession();
    session
        .set("modelBean", writeJson(model.get()))
        .set("basePath", getBasePath(httpRequest))
        .set("searchRequest", searchRequest);
    // Adding/editing NumberSeries or StorageLocation requires bsDialog as template, NOT EditButtons
    if (model.get() instanceof StorageLocation || model.get() instanceof NumberSeries) {
      session.showRecursive(navbar(), "bsDialog");
    } else {
      session.showRecursive(navbar(), "bsDialog", "EditButtons");
    }
    StreamingOutput so = session::render;
    beforeRenderCreatePage(session);
    return Response.ok(so).build();
  }

  Response saveOrUpdate(HttpServletRequest httpRequest, Function<MODEL, Integer> idSupplier, ProfileManager<AzureAdProfile> pm) {
    this.pm = pm;
    this.profile = getProfile(pm);
    String entity = httpRequest.getParameter("entity");
    if (LOG.isDebugEnabled()) {
      LOG.debug("saveOrUpdate (type={};entity={})", typeName(), entity);
    }
    MODEL model = BcdModule.readJson(entity, modelClass);
    Integer id = idSupplier.apply(model);
    if (id == null) {
      beforeSave(model);
      id = dao.insert(model);
    } else {
      beforeUpdate(model);
      dao.update(model);
    }
    return forwardTo("edit", id, httpRequest);
  }

  Response delete(int id) {
    LOG.debug("delete (type={};id={})", typeName(), id);
    beforeDelete(id);
    if (dao.delete(id) == 0) {
      LOG.warn("Nothing deleted");
    }
    return Response.noContent().build();
  }

  Response deactivate(int id) {
    LOG.debug("deactivate (type={};id={})", typeName(), id);
    dao.deactivate(id);
    return Response.noContent().build();
  }

  RenderSession newListPageRenderSession() {
    return newRenderSession("list");
  }

  RenderSession newEditPageRenderSession() {
    return newRenderSession("edit");
  }

  RenderSession newRenderSession(String basename) {
    basename = rchop(getClass().getSimpleName(), "Resource") + "." + basename;
    return ResourceUtil.newRenderSession(basename);
  }

  @SuppressWarnings("unused")
  void listDataReady(List<Row> rows, int rowCount) {
    // Override if necessary
  }

  @SuppressWarnings("unused")
  void beforeRenderListPage(RenderSession session, SearchRequest searchRequest, List<Row> rows,
                            int rowCount) throws RenderException {
    // Override if necessary
  }

  @SuppressWarnings("unused")
  void beforeRenderCreatePage(RenderSession session) throws RenderException {
    // Override if necessary
  }

  @SuppressWarnings({"unused", "RedundantThrows"})
  void beforeRenderEditPage(RenderSession session, MODEL model) throws RenderException {
    // Override if necessary
  }

  @SuppressWarnings("unused")
  void beforeSave(MODEL model) {
    // Override if necessary
  }

  @SuppressWarnings("unused")
  void beforeUpdate(MODEL model) {
    // Override if necessary
  }

  @SuppressWarnings("unused")
  void beforeDelete(int id) {
    // Override if necessary
  }

  Response forwardTo(int id, HttpServletRequest request) {
    return ResourceUtil.forwardTo(getClass(), id, request);
  }

  @SuppressWarnings("SameParameterValue")
  Response forwardTo(String action, int id, HttpServletRequest request) {
    return ResourceUtil.forwardTo(getClass(), action, id, request);
  }

  String getBasePath(HttpServletRequest request) {
    return ResourceUtil.getBasePath(getClass(), request);
  }

  String typeName() {
    return modelClass.getSimpleName();
  }

  /**
   * Returns the upn (User Principle Name) of the user doing a request.
   *
   * @return upn
   */
  int getSessionUserId() {
    Optional<AzureAdProfile> profile = pm.get(true);
    if (profile.isPresent()) {
      AzureAdProfile azureAdProfile = profile.get();
      String upn = azureAdProfile.getUsername();
      LOG.debug("upn: {}", upn);
      return getUserId(upn);
    }
    final String msg = "Unknown profile";
    throw new WebApplicationException(msg, Response.Status.INTERNAL_SERVER_ERROR);
  }

  /**
   * Returns the used id of the current user
   *
   * @param upn : the User Principal Name
   * @return user id
   */
  int getUserId(String upn) {
    try (Connection con = Database.connect()) {
      SQL sql = SQL.create("SELECT userId FROM bcd_user WHERE upn = :upn");
      try (SQLQuery query = sql.prepareQuery(con)) {
        int userId = query.bind("upn", upn).getInt();
        LOG.info("userId: {}", userId);
        return userId;
      } catch (KJSQLException e) {
        throw new WebApplicationException("Unknown user", Response.Status.INTERNAL_SERVER_ERROR);
      }
    } catch(SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  /*
  * Returns the Permission (VIEW or EDIT) granted to a specific profile.
  * NOTE: The profile can only be an AzureAd Profile.
  */
  Permission hasPermission() {
      Set<String> permissions = this.profile.getPermissions();
      if (permissions.contains(Permission.EDIT.name())) {
        return Permission.EDIT;
      } else if (permissions.contains(Permission.VIEW.name())) {
        return Permission.VIEW;
      } else {
        return null;
      }
  }

  String navbar() {
    if (profile.getRoles().contains("ADMIN")) {
       return "adminNavbar";
    }
    return "navbar";
  }

  AzureAdProfile getProfile(ProfileManager<AzureAdProfile> pm) {
    List<AzureAdProfile> profiles = pm.getAll(true);
    if (profiles.size() != 1) {
      // Note: should not be possible!
      LOG.warn("Found more than one profile for this user: {}. Forced logout.", pm.getAll(true).get(0).getUpn());
      pm.remove(true);
      pm.logout();
      throw new WebApplicationException("Invalid user profile", Response.Status.UNAUTHORIZED);
    }
    return profiles.get(0);
  }

}