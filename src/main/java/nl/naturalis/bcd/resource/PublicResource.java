package nl.naturalis.bcd.resource;

import org.klojang.template.RenderException;
import org.klojang.template.RenderSession;
import org.pac4j.jax.rs.annotations.Pac4JSecurity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.StreamingOutput;

import static nl.naturalis.bcd.resource.ResourceUtil.newRenderSession;
@Path("/public")
public class PublicResource {

    private final static Logger logger = LoggerFactory.getLogger(PublicResource.class);
    @GET
    @Pac4JSecurity(clients = "AzureAdClient", authorizers = "mustBeAdmin")
    @Produces(MediaType.TEXT_HTML)
    public StreamingOutput printPublic() throws RenderException {
        logger.info("Public page");
        RenderSession session = newRenderSession("Public").show("logo");
        return session::render;
    }

}


