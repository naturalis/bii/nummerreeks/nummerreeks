package nl.naturalis.bcd.resource;

import com.fasterxml.jackson.core.JsonProcessingException;
import nl.naturalis.bcd.dao.LayoutDao;
import nl.naturalis.bcd.exception.NotFoundException;
import nl.naturalis.bcd.exception.*;
import nl.naturalis.bcd.json.BcdModule;
import nl.naturalis.bcd.listbox.ListBoxFactory;
import nl.naturalis.bcd.model.Layout;
import nl.naturalis.bcd.model.Permission;
import nl.naturalis.bcd.print.LayoutDataFactory;
import nl.naturalis.common.ExceptionMethods;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.klojang.template.RenderException;
import org.klojang.template.RenderSession;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.jax.rs.annotations.Pac4JProfileManager;
import org.pac4j.jax.rs.annotations.Pac4JSecurity;
import org.pac4j.oidc.profile.azuread.AzureAdProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.net.URI;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalInt;

import static nl.naturalis.bcd.listbox.ListBoxFactory.getLayoutOptions;
import static nl.naturalis.bcd.resource.ResourceUtil.stream;
import static nl.naturalis.bcd.search.Entity.LAYOUT;

@Path("/layout")
@Pac4JSecurity(clients = "AzureAdClient", authorizers = "mustBeEditor")
@Produces(MediaType.TEXT_HTML)
public class LayoutResource extends CRUDResource<Layout, LayoutDao> {

  private static final Logger LOG = LoggerFactory.getLogger(LayoutResource.class);

  public LayoutResource() {
    super(new LayoutDao(), Layout.class);
  }

  @GET
  @Path("options")
  @Produces(MediaType.TEXT_HTML)
  public String options(@QueryParam("inputType") String inputType) {
    return ListBoxFactory.getLayoutOptions(inputType).getHTML();
  }

  @GET
  public Response home(@Context HttpServletRequest httpRequest) {
    return Response.seeOther(URI.create(getBasePath(httpRequest) + "0")).build();
  }

  @GET
  @Path("{layoutId}")
  @Produces(MediaType.TEXT_HTML)
  public Response editConfig(
        @PathParam("layoutId") int layoutId,
        @Context HttpServletRequest httpRequest,
        @Pac4JProfileManager ProfileManager<AzureAdProfile> pm)
        throws RenderException {
    this.pm = pm;
    this.profile = getProfile(pm);
    LOG.debug("editConfig (type={})", typeName());
    boolean fresh = false;
    if (layoutId == 0) {
      // Will be true if we arrive here from the menu bar (see AdminNavbar.html)
      // or if the user has just deleted a layout. We just load some arbitrary
      // Layout instance to kind of get the right number of rows and columns
      // for the <textarea>.
      layoutId = dao.findAny();
      fresh = true;
    }
    Optional<Layout> opt = dao.find(Layout.class, Layout::new, layoutId);
    if (opt.isEmpty()) {
      throw new NotFoundException(LAYOUT, "layoutId", layoutId);
    }
    Layout layout = opt.get();
    String json = layout.getJsonConfig();
    String[] lines = json.split("\\n");
    int lineCount = lines.length;
    OptionalInt optInt = Arrays.stream(lines).mapToInt(String::length).max();
    if (optInt.isEmpty()) {
      throw new BcdRuntimeException("Page has no line endings.");
    }
    int lineWidth = optInt.getAsInt();

    // This parameter will be present after a redirect from updateConfig(). It
    // will be inserted into the template so that the javascript code will generate
    // a success alert.
    boolean updated = httpRequest.getParameter("updated") != null;
    RenderSession session = newRenderSession("editor");
    Permission permission = hasPermission();
    if (fresh) {
      layout = new Layout();
      layout.setLayoutId(0);
      session.insert(layout);
    } else {
      session.insert(layout).set("json", json);
    }
    session
          .set("basePath", getBasePath(httpRequest))
          .set("layoutOptions", getLayoutOptions(null)) // show all layouts
          .set("rows", lineCount + 1)
          .set("cols", lineWidth + 6)
          .set("updated", updated)
          .set("permission", permission.name())
          .showRecursive(navbar(), "bsDialog");
    return stream(session::render);
  }

  @GET
  @Path("/{id}/delete")
  public Response delete(@PathParam("id") int id,
        @Context HttpServletRequest httpRequest) {
    Optional<Layout> opt = dao.find(Layout.class, Layout::new, id);
    if (opt.isEmpty()) {
      throw new NotFoundException(LAYOUT, id);
    }
    Layout layout = opt.get();
    int usages = dao.countUsages(layout);
    if (usages == 1) {
      String fmt =
            "Formaat \"%s\" mag niet verwijderd worden want het is het enige "
                  + "formaat voor toepassing \"%s\"";
      String msg = String.format(fmt, layout.getLayoutName(), layout.getLabelUsage());
      throw new BcdUserException(msg);
    }
    super.delete(id);
    return Response.seeOther(URI.create(getBasePath(httpRequest) + "0")).build();
  }

  @GET
  @Path("{id}/legend")
  @Produces(MediaType.TEXT_HTML)
  public Response getLegend(@PathParam("id") int id,
        @Context HttpServletRequest httpRequest)
        throws RenderException {
    LOG.debug("getLegend (type={};id={})", typeName(), id);
    Optional<Layout> opt = dao.find(Layout.class, Layout::new, id);
    if (opt.isEmpty()) {
      throw new NotFoundException(LAYOUT, id);
    }
    Layout layout = opt.get();
    RenderSession session = switch (layout.getTemplate()) {
      case STANDARD -> newRenderSession("legend.specimens");
      case STORAGE_LOCATION -> newRenderSession("legend.storage");
      case LABORATORY -> newRenderSession("legend.lab");
      case STD_PLUS -> newRenderSession("legend.stdplus");
    };
    session.set("basePath", getBasePath(httpRequest)).set("layoutId", id).showRecursive();
    return stream(session::render);
  }

  @GET
  @Path("{id}/updateConfig/")
  public Response updateConfig(
        @PathParam("id") int id, @QueryParam("jsonConfig") String jsonConfig) {
    LOG.debug("updateConfig (type={};jsonConfig={})", typeName(), jsonConfig);
    Map<String, Object> newConfig;
    try {
      newConfig = BcdModule.readJson(jsonConfig);
    } catch (JsonProcessingException e) {
      throw new BcdUserInputException("Syntax fout in JSON configuratie");
    }
    Optional<Layout> opt = dao.find(Layout.class, Layout::new, id);
    if (opt.isEmpty()) {
      throw new NotFoundException(LAYOUT, id);
    }
    Layout layout = opt.get();
    Map<String, Object> oldConfig;
    try {
      oldConfig = BcdModule.readJson(layout.getJsonConfig());
    } catch (JsonProcessingException e) {
      throw ExceptionMethods.uncheck(e);
    }
    if (!newConfig.equals(oldConfig)) {
      layout.setJsonConfig(jsonConfig);
      LayoutDataFactory.validate(layout);
      // As soon as an update takes place we erase the importedFrom property
      // because that file can no longer be used to restore the configuration
      // we just changed.
      layout.setImportedFrom(null);
      layout.setDateModified(LocalDateTime.now());
      dao.update(layout);
    }
    return Response.seeOther(URI.create("/layout/" + id + "/?updated=1")).build();
  }

  @GET
  @Path("{id}/copy/")
  public Response copy(
        @PathParam("id") int id,
        @QueryParam("newName") String newName,
        @Context HttpServletRequest httpRequest) {
    LOG.debug("copy (type={};newName={})", typeName(), newName);
    newName = newName.strip();
    if (newName.isEmpty()) {
      throw new BcdUserInputException("Geen naam ingevuld");
    }
    Layout layout = new Layout();
    layout.setLayoutName(newName);
    if (!dao.isUnique(layout)) {
      throw new DuplicateKeyException(LAYOUT, "naam", newName);
    }
    Optional<Layout> opt = dao.find(Layout.class, Layout::new, id);
    if (opt.isEmpty()) {
      throw new NotFoundException(LAYOUT, "naam", newName);
    }
    layout = opt.get();
    layout.setLayoutId(null);
    layout.setLayoutName(newName);
    int newId = dao.insert(layout);
    return Response.seeOther(URI.create(getBasePath(httpRequest) + newId)).build();
  }

  @GET
  @Path("/export")
  public Response exportFormats() {
    LOG.debug("exportFormats (type={})", typeName());
    String fileName =
          "ConfiguratieLabelFormaten_"
                + DateTimeFormatter.ofPattern("uuuuMMdd_HHmmss")
                .format(LocalDateTime.now())
                + ".json";
    StreamingOutput output = dao::exportLayouts;
    return Response.ok(output)
          .header("Content-Disposition", "attachment; filename=" + fileName)
          .build();
  }

  @GET
  @Path("/import")
  public Response importFormats(@Context HttpServletRequest httpRequest)
        throws RenderException {
    LOG.debug("importFormats (type={})", typeName());
    RenderSession session = newRenderSession("import");
    session.set("basePath", getBasePath(httpRequest)).showRecursive();
    return stream(session::render);
  }

  @POST
  @Path("/upload")
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  public Response upload(
        @FormDataParam("file") byte[] data,
        @FormDataParam("importType") String importType,
        @FormDataParam("file") FormDataContentDisposition fileDetail,
        @Context HttpServletRequest httpRequest) {
    LOG.debug("upload (type={};importType={})", typeName(), importType);
    dao.importFormats(data, importType, fileDetail.getFileName());
    return Response.seeOther(URI.create(getBasePath(httpRequest) + 0)).build();
  }
}
