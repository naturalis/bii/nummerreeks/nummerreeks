package nl.naturalis.bcd.resource;

import nl.naturalis.bcd.BcdUtil;
import nl.naturalis.bcd.dao.LayoutDao;
import nl.naturalis.bcd.dao.SeriesDao;
import nl.naturalis.bcd.dao.StorageLocationDao;
import nl.naturalis.bcd.exception.NotAuthorizedException;
import nl.naturalis.bcd.exception.NotFoundException;
import nl.naturalis.bcd.json.BcdModule;
import nl.naturalis.bcd.json.JsonParam;
import nl.naturalis.bcd.model.NumberSeries;
import nl.naturalis.bcd.model.Permission;
import nl.naturalis.bcd.model.RequestStatus;
import nl.naturalis.bcd.model.Layout;
import nl.naturalis.bcd.model.StorageLocation;
import nl.naturalis.bcd.print.NumberSeriesPrintService;
import nl.naturalis.bcd.search.SearchRequest;
import nl.naturalis.bcd.search.SeriesSearchRequest;
import nl.naturalis.bcd.search.SeriesSortColumn;
import org.klojang.db.Row;
import org.klojang.helpers.ListBoxOptions;
import org.klojang.template.RenderException;
import org.klojang.template.RenderSession;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.jax.rs.annotations.Pac4JProfileManager;
import org.pac4j.jax.rs.annotations.Pac4JSecurity;
import org.pac4j.oidc.profile.azuread.AzureAdProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.ToIntFunction;

import static nl.naturalis.bcd.BcdUtil.ERR_INVALID_TOPDESK_ID;
import static nl.naturalis.bcd.BcdUtil.checkInput;
import static nl.naturalis.bcd.exception.NotAuthorizedException.notAuthorized;
import static nl.naturalis.bcd.json.BcdModule.writeJson;
import static nl.naturalis.bcd.listbox.ListBoxFactory.*;
import static nl.naturalis.bcd.model.NumberSeries.getLabel;
import static nl.naturalis.bcd.model.RequestStatus.*;
import static nl.naturalis.bcd.resource.ResourceUtil.getEditHeader;
import static nl.naturalis.bcd.resource.ResourceUtil.includeComments;
import static nl.naturalis.bcd.resource.ResourceUtil.stream;
import static nl.naturalis.bcd.search.Entity.NUMBER_SERIES;
import static nl.naturalis.bcd.search.SortOrder.ASC;
import static nl.naturalis.bcd.search.SortOrder.DESC;
import static nl.naturalis.common.CollectionMethods.implode;
import static nl.naturalis.common.ObjectMethods.ifEmpty;
import static nl.naturalis.common.StringMethods.ifBlank;
import static nl.naturalis.common.check.CommonChecks.*;

@Path("/series")
@Pac4JSecurity(clients = "AzureAdClient", authorizers = "mustBeUser", matchers = "excludedUnprotectedPaths")
public class SeriesResource extends CRUDResource<NumberSeries, SeriesDao> {

  private static final Logger LOG = LoggerFactory.getLogger(SeriesResource.class);

  public SeriesResource() {
    super(new SeriesDao(), NumberSeries.class);
  }

  @GET
  @Produces(MediaType.TEXT_HTML)
  public StreamingOutput list(
          @QueryParam("searchRequest") JsonParam<SeriesSearchRequest> json,
          @Context HttpServletRequest httpRequest,
          @Pac4JProfileManager ProfileManager<AzureAdProfile> pm)
          throws RenderException {
    this.pm = pm;
    this.profile = getProfile(pm);
    if (json == null) {
      json = new JsonParam<>();
    }
    SeriesSearchRequest sr = json.deserialize(SeriesSearchRequest.class);
    if (sr.getSortColumn() == null) {
      sr.setSortColumn(SeriesSortColumn.DATE_MODIFIED);
      sr.setSortOrder(DESC);
    } else if (sr.getSortOrder() == null) {
      if (sr.getSortColumn() == SeriesSortColumn.DATE_MODIFIED) {
        sr.setSortOrder(DESC);
      } else {
        sr.setSortOrder(ASC);
      }
    }
    return super.list(sr, httpRequest, pm);
  }

  @Override
  ToIntFunction<SearchRequest> getRowCountProvider() {
    return searchRequest -> dao.countRows(searchRequest, getSessionUserId());
  }

  @Override
  Function<SearchRequest, List<Row>> getRowsProvider() {
    return req -> dao.list(req, getSessionUserId());
  }

  @Override
  void beforeRenderListPage(RenderSession session, SearchRequest searchRequest, List<Row> rows,
      int rowCount) throws RenderException {
    session.set("header", ResourceUtil.getSeriesListHeader(searchRequest));
  }

  @GET
  @Path("/new")
  @Pac4JSecurity(clients = "AzureAdClient", authorizers = "mustBeEditor")
  @Produces(MediaType.TEXT_HTML)
  public Response create(
          @Context HttpServletRequest httpRequest,
          @Pac4JProfileManager ProfileManager<AzureAdProfile> pm)
          throws RenderException {
    return super.create(NumberSeries::new, httpRequest, pm);
  }

  void beforeRenderCreatePage(RenderSession session) throws RenderException {
    session.set("header", "Nieuwe Nummerreeks Invoeren")
        .set("cmsOptions", getCmsOptions())
        .set("departmentOptions", getDepartmentOptions())
        .set("instituteOptions", getInstituteOptions())
        .set("requesterOptions", getRequesterOptions())
        .set("collectionOptions", getCollectionOptions());
  }

  @GET
  @Path("/{id}")
  @Pac4JSecurity(clients = "AzureAdClient", authorizers = "mustBeUser")
  @Produces(MediaType.TEXT_HTML)
  public Response view(
          @PathParam("id") int id,
          @Context HttpServletRequest httpRequest,
          @Pac4JProfileManager ProfileManager<AzureAdProfile> pm)
      throws RenderException {
    this.pm = pm;
    this.profile = getProfile(pm);
    LOG.debug("view (type={};id={})", typeName(), id);
    // We ping-pong the search request from the list page, so that when the user closes
    // the form, he/she returns to the same page number within the list page. Note,
    // however, that we have no need to deserialize the search request here.
    String searchRequest = ifEmpty(httpRequest.getParameter("searchRequest"), "{}");
    Optional<Row> opt = dao.view(id);
    if (opt.isEmpty()) {
      throw new NotFoundException(NUMBER_SERIES, id);
    }
    Row row = opt.get();
    String ic = row.getString("instituteCode");
    String cc = row.getString("collectionCode");
    row.addColumn("firstLabel", getLabel(ic, cc, row.getInt("firstNumber")));
    row.addColumn("lastLabel", getLabel(ic, cc, row.getInt("lastNumber")));
    RenderSession session = newRenderSession("view");
    Permission permission = hasPermission();
    session.insert(row)
        .set("basePath", getBasePath(httpRequest))
        .set("searchRequest", searchRequest)
        .set("permission", permission.name())
        .show(navbar(), "bsDialog");
    RequestStatus status = RequestStatus.parse(row.getInt("status"));
    if (status == OPEN) {
      session.in("statusOpen").set("requestId", id).show();
    } else if (status == LOCKED) {
      session.in("statusLocked").insert(row).show();
    } else /* CLOSED */ {
      session.in("statusClosed").set("requestId", id).show();
      session.populate("layoutDisplay", row);
    }
    String comments = ifBlank((row.getString("comments")), null);
    includeComments(session, comments);
    return stream(session::render);
  }

  @GET
  @Path("/{id}/edit")
  @Pac4JSecurity(clients = "AzureAdClient", authorizers = "mustBeEditor")
  @Produces(MediaType.TEXT_HTML)
  public Response edit(
          @PathParam("id") int id,
          @Context HttpServletRequest httpRequest,
          @Pac4JProfileManager ProfileManager<AzureAdProfile> pm)
      throws RenderException {
    this.pm = pm;
    this.profile = getProfile(pm);
    LOG.debug("Edit (type={};id={})", typeName(), id);
    String searchRequest = ifEmpty(httpRequest.getParameter("searchRequest"), "{}");
    Optional<NumberSeries> opt = dao.find(NumberSeries.class, NumberSeries::new, id);
    if (opt.isEmpty()) {
      throw new NotFoundException(NUMBER_SERIES, id);
    }
    NumberSeries bean = opt.get();
    if (bean.getStatus() != OPEN) { // Malicious
      throw new NotAuthorizedException();
    }
    RenderSession session = newEditPageRenderSession();
    Permission permission = hasPermission();
    ListBoxOptions collections =
        getCollectionOptions(bean.getInstituteId(), bean.getCollectionId());
    session.set("header", getEditHeader(NUMBER_SERIES, bean.firstLabel()))
        .set("modelBean", BcdModule.writeJson(bean))
        .set("basePath", getBasePath(httpRequest))
        .set("searchRequest", searchRequest)
        .set("permission", permission.name())
        .set("cmsOptions", getCmsOptions(bean.getCmsId()))
        .set("departmentOptions", getDepartmentOptions(bean.getDepartmentId()))
        .set("instituteOptions", getInstituteOptions(bean.getInstituteId()))
        .set("requesterOptions", getRequesterOptions(bean.getRequesterId()))
        .set("collectionOptions", collections)
        .showRecursive(navbar(), "bsDialog");
    return stream(session::render);
  }

  @GET
  @Pac4JSecurity(clients = "AzureAdClient", authorizers = "mustBeEditor")
  @Path("/save-or-update")
  public Response saveOrUpdate(@Context HttpServletRequest httpRequest,
                               @Pac4JProfileManager ProfileManager<AzureAdProfile> pm) {
    return super.saveOrUpdate(httpRequest, NumberSeries::getRequestId, pm);
  }

  void beforeSave(NumberSeries series) {
    checkNumbers(series);
    checkTopDeskId(series, true);
    series.setStatus(OPEN);
    series.setDispenseCount(0);
    series.setDateCreated(LocalDateTime.now());
    series.setDateModified(series.getDateCreated());
    series.setModifiedBy(getSessionUserId());
  }

  void beforeUpdate(NumberSeries series) {
    checkNumbers(series);
    Optional<NumberSeries> opt = dao.find(NumberSeries.class, NumberSeries::new, series.getRequestId());
    if (opt.isEmpty()) {
      throw new NotFoundException(NUMBER_SERIES, series.getRequestId());
    }

    // Don't allow new topdesk ID to be less perfect than what we already have
    checkTopDeskId(series, BcdUtil.isValidTopDeskId(opt.get().getTopDeskId()));
    series.setDateModified(LocalDateTime.now());
    series.setModifiedBy(getSessionUserId());
  }

  @DELETE
  @Pac4JSecurity(clients = "AzureAdClient", authorizers = "mustBeEditor")
  @Path("/{id}")
  public Response delete(
          @PathParam("id") int id,
          @Pac4JProfileManager ProfileManager<AzureAdProfile> pm) {
    return super.delete(id);
  }

  @Override
  void beforeDelete(int id) {
    RequestStatus curStatus = dao.getStatus(id);
    if (curStatus != OPEN) { // Malicious
      throw notAuthorized("Illegal attempt to delete number series");
    }
  }

  @GET
  @Pac4JSecurity(clients = "AzureAdClient", authorizers = "mustBeEditor")
  @Path("/{id}/lock")
  public Response lock(
          @PathParam("id") int id,
          @Context HttpServletRequest httpRequest,
          @Pac4JProfileManager ProfileManager<AzureAdProfile> pm) {
    this.pm = pm;
    LOG.debug("lock (type={};id={})", typeName(), id);
    Optional<NumberSeries> opt = dao.find(NumberSeries.class, NumberSeries::new, id);
    if (opt.isEmpty()) {
      throw new NotFoundException(NUMBER_SERIES, id);
    }
    NumberSeries series = opt.get();
    if (series.getStatus() != OPEN) { // Bad stuff
      throw notAuthorized("Illegal attempt to lock number series");
    }
    LocalDateTime now = LocalDateTime.now();
    series.setDateModified(now);
    series.setModifiedBy(getSessionUserId());
    series.setStatus(LOCKED);
    dao.update(series);
    return forwardTo(id, httpRequest);
  }

  @GET
  @Pac4JSecurity(clients = "AzureAdClient", authorizers = "mustBeEditor")
  @Path("/{id}/unlock")
  public Response unlock(
          @PathParam("id") int id,
          @Context HttpServletRequest httpRequest,
          @Pac4JProfileManager ProfileManager<AzureAdProfile> pm) {
    this.pm = pm;
    LOG.debug("unlock (type={};id={})", typeName(), id);
    Optional<NumberSeries> opt = dao.find(NumberSeries.class, NumberSeries::new, id);
    if (opt.isEmpty()) {
      throw new NotFoundException(NUMBER_SERIES, id);
    }
    NumberSeries series = opt.get();
    if (series.getStatus() != LOCKED) { // Bad stuff
      throw notAuthorized("Illegal attempt to unlock number series");
    }
    LocalDateTime now = LocalDateTime.now();
    series.setDateModified(now);
    series.setModifiedBy(getSessionUserId());
    series.setStatus(OPEN);
    dao.update(series);
    return forwardTo("edit", id, httpRequest);
  }

  @GET
  @Pac4JSecurity(clients = "AzureAdClient", authorizers = "mustBeEditor")
  @Path("/{id}/archive")
  public Response archive(
          @PathParam("id") int id,
          @Context HttpServletRequest httpRequest,
          @Pac4JProfileManager ProfileManager<AzureAdProfile> pm) {
    this.pm = pm;
    this.profile = getProfile(pm);
    LOG.debug("archive (type={};id={})", typeName(), id);
    Optional<NumberSeries> opt = dao.find(NumberSeries.class, NumberSeries::new, id);
    if (opt.isEmpty()) {
      throw new NotFoundException(NUMBER_SERIES, id);
    }
    NumberSeries series = opt.get();
    if (series.getStatus() != LOCKED) { // Bad stuff
      throw notAuthorized("Illegal attempt to archive number series");
    }
    LocalDateTime now = LocalDateTime.now();
    series.setDateModified(now);
    series.setModifiedBy(getSessionUserId());
    if (series.getDateFirstClosed() == null) {
      series.setDateFirstClosed(now);
    }
    series.setDispenseCount(series.getDispenseCount() + 1);
    series.setStatus(CLOSED);
    dao.update(series);
    return forwardTo(id, httpRequest);
  }

  @GET
  @Pac4JSecurity(clients = "AzureAdClient", authorizers = "mustBeEditor")
  @Path("/{id}/unarchive")
  public Response unarchive(
          @PathParam("id") int id,
          @Context HttpServletRequest httpRequest,
          @Pac4JProfileManager ProfileManager<AzureAdProfile> pm) {
    this.pm = pm;
    LOG.debug("unarchive (type={};id={})", typeName(), id);
    Optional<NumberSeries> opt = dao.find(NumberSeries.class, NumberSeries::new, id);
    if (opt.isEmpty()) {
      throw new NotFoundException(NUMBER_SERIES, id);
    }
    NumberSeries series = opt.get();
    if (series.getStatus() != CLOSED) { // Bad stuff
      throw notAuthorized("Illegal attempt to unarchive number series");
    }
    LocalDateTime now = LocalDateTime.now();
    series.setDateModified(now);
    series.setModifiedBy(getSessionUserId());
    series.setStatus(LOCKED);
    dao.update(series);
    return forwardTo(id, httpRequest);
  }

  @GET
  @Pac4JSecurity(clients = "AzureAdClient", authorizers = "mustBeEditor")
  @Path("/institute-changed")
  @Produces(MediaType.TEXT_HTML)
  public String instituteChanged(
          @QueryParam("instituteId") int instituteId,
          @QueryParam("collectionId") int collectionId,
          @Pac4JProfileManager ProfileManager<AzureAdProfile> pm) {
    LOG.debug("instituteChanged (type={};instituteId={};collectionId={})",
        typeName(),
        instituteId,
        collectionId);
    return getCollectionOptions(instituteId, collectionId).getHTML();
  }

  @POST
  @Pac4JSecurity(clients = "AzureAdClient", authorizers = "mustBeEditor")
  @Path("/find-overlap")
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  public List<String> findOverlap(NumberSeries series) {
    LOG.debug("findOverlap (type={};series={})", typeName(), writeJson(series));
    return dao.findOverlap(series);
  }

  @GET
  @Pac4JSecurity(clients = "AzureAdClient", authorizers = "mustBeEditor")
  @Path("/min-first-number-for-collection/{collectionId}")
  @Produces(MediaType.APPLICATION_JSON)
  public int getMinFirstNumberForCollection(@PathParam("collectionId") int collectionId) {
    LOG.debug("getMinFirstNumberForCollection (type={};collectionId={})", typeName(), collectionId);
    return dao.getMinFirstNumber(collectionId, null);
  }

  @GET
  @Pac4JSecurity(clients = "AzureAdClient", authorizers = "mustBeEditor")
  @Path("/min-first-number-for-series/{collectionId}/{requestId}")
  @Produces(MediaType.APPLICATION_JSON)
  public int getMinFirstNumberForSeries(@PathParam("collectionId") int collectionId,
      @PathParam("requestId") int curFirstNumber) {
    LOG.debug("getMinFirstNumberForSeries (type={};collectionId={};curFirstNumber={})",
        typeName(),
        collectionId,
        curFirstNumber);
    return dao.getMinFirstNumber(collectionId, curFirstNumber);
  }

  @SuppressWarnings("SpellCheckingInspection")
  @GET
  @Pac4JSecurity(clients = "AzureAdClient", authorizers = "mustBeEditor")
  @Path("/{requestId}/print/{layoutId}")
  @Produces("application/pdf")
  public Response printLabels(
          @PathParam("requestId") int id,
          @PathParam("layoutId") int layoutId) {
    LOG.debug("printLabels (type={};id={};layoutId={})", typeName(), id, layoutId);
    Optional<Layout> optLayout = new LayoutDao().find(Layout.class, Layout::new, layoutId);
    if (optLayout.isEmpty()) {
      throw new WebApplicationException(String.format("Layout met id %s is niet beschikbaar.", layoutId));
    }
    Layout layout = optLayout.get();
    Optional<NumberSeries> numberSeries = dao.find(NumberSeries.class, NumberSeries::new, id);
    if (numberSeries.isEmpty()) {
      throw new WebApplicationException(String.format("De nummerreeks met id %s is niet aanwezig.", id));
    }
    NumberSeries series = numberSeries.get();
    NumberSeriesPrintService svc = new NumberSeriesPrintService(series, layout);
    String fileName = String.format("%s.%s_%s_%s.%s.pdf",
        DateTimeFormatter.ofPattern("uuuuMMddHHmmss").format(LocalDateTime.now()),
        series.prefix(),
        series.getFirstNumber(),
        series.getLastNumber(),
        layout.getLabelUsage().name().toLowerCase());
    StreamingOutput output = svc::printLabels;
    series.setLastUsedLayout(layout.toString());
    dao.update(series);
    return Response.ok(output)
        .header("Content-Disposition", "attachment; filename=" + fileName)
        .build();
  }

  private void checkNumbers(NumberSeries series) {
    Integer first = series.getFirstNumber();
    Integer last = series.getLastNumber();
    Integer amount = series.getAmount();
    checkInput(first).is(notNull(), "Eerste nummer verplicht")
        .is(greaterThan(), 0, "Eerste nummer moet minimaal 1 zijn");
    checkInput(amount).is(notNull(), "Aantal verplicht")
        .is(greaterThan(), 0, "Aantal moet minimaal 1 zijn");
    checkInput(last).is(notNull(), "Laatste nummer verplicht")
        .is(atLeast(), first, "Laatste nummer moet minimaal %s zijn", first);
    checkInput(first + amount).is(eq(), last + 1, "Optelsom klopt niet");
    List<String> overlap = dao.findOverlap(series);
    checkInput(overlap).is(empty(),
        "Nummerreeks overlapt met andere nummerreeksen: %s",
        implode(overlap));
  }

  private void checkTopDeskId(NumberSeries series, boolean strict) {
    String id = checkInput(series.getTopDeskId()).isNot(blank(), "TOPDesk Nummer verplicht").ok(String::strip);
    if (strict) {
      checkInput(id).is(BcdUtil::isValidTopDeskId, ERR_INVALID_TOPDESK_ID, id);
    } else {
      checkInput(id).is(BcdUtil::isValidLegacyTopDeskId, ERR_INVALID_TOPDESK_ID, id);
    }
    series.setTopDeskId(id);
    checkInput(series).is(dao::isUnique, "TOPDesk nummer bestaat al");
    StorageLocation storageLocation = new StorageLocation();
    storageLocation.setTopDeskId(id);
    StorageLocationDao slDao = new StorageLocationDao();
    checkInput(storageLocation).is(slDao::isUnique,
        "Er is al een storage location met dit TOPDesk nummer");
  }
}
