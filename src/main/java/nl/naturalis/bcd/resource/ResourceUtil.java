package nl.naturalis.bcd.resource;

import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;

import org.apache.http.client.utils.URIBuilder;
import org.klojang.template.ParseException;
import org.klojang.template.RenderException;
import org.klojang.template.RenderSession;
import org.klojang.template.Template;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.bcd.search.Entity;
import nl.naturalis.bcd.search.SearchRequest;
import nl.naturalis.common.ExceptionMethods;
import static nl.naturalis.bcd.BcdStringifiers.BCD_STRINGIFIERS;
import static nl.naturalis.bcd.search.Entity.CMS;
import static nl.naturalis.bcd.search.Entity.DEPARTMENT;
import static nl.naturalis.bcd.search.Entity.INSTITUTE;
import static nl.naturalis.bcd.search.Entity.NUMBER_SERIES;
import static nl.naturalis.common.ObjectMethods.ifEmpty;
import static nl.naturalis.common.StringMethods.concat;
import static nl.naturalis.common.StringMethods.count;
import static nl.naturalis.common.StringMethods.ellipsis;
import static nl.naturalis.common.StringMethods.rtrim;
import static nl.naturalis.common.StringMethods.trim;

public class ResourceUtil {

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(ResourceUtil.class);

  private ResourceUtil() {}

  public static RenderSession newRenderSession(String basename) {
    try {
      return getTemplate(basename).newRenderSession(BCD_STRINGIFIERS);
    } catch (ParseException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  static Template getTemplate(String basename) throws ParseException {
    String path = "/html/" + basename + ".html";
    return Template.fromResource(ResourceUtil.class, path);
  }

  static Response jsonResponse(Object entity) {
    return Response.status(Status.OK).type(MediaType.APPLICATION_JSON).entity(entity).build();
  }

  static Response stream(StreamingOutput out) {
    return Response.ok(out).build();
  }

  static Response stream(Status status, StreamingOutput out) {
    return Response.status(status).entity(out).build();
  }

  static Response forwardTo(Class<?> resourceClass, int id, HttpServletRequest httpRequest) {
    String searchRequest = ifEmpty(httpRequest.getParameter("searchRequest"), "{}");
    String path = getBasePath(resourceClass, httpRequest) + id;
    try {
      URIBuilder ub = new URIBuilder(path, StandardCharsets.UTF_8);
      ub.addParameter("searchRequest", searchRequest);
      return Response.seeOther(ub.build()).build();
    } catch (URISyntaxException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  static Response forwardTo(
      Class<?> resourceClass, String action, int id, HttpServletRequest httpRequest) {
    String searchRequest = ifEmpty(httpRequest.getParameter("searchRequest"), "{}");
    String path = getBasePath(resourceClass, httpRequest) + id + "/" + action;
    try {
      URIBuilder ub = new URIBuilder(path, StandardCharsets.UTF_8);
      ub.addParameter("searchRequest", searchRequest);
      return Response.seeOther(ub.build()).build();
    } catch (URISyntaxException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  static String getBasePath(Class<?> resourceClass, HttpServletRequest request) {
    String cp = request.getContextPath();
    Path path = resourceClass.getDeclaredAnnotation(Path.class);
    return rtrim(cp, '/') + '/' + trim(path.value(), '/') + '/';
  }

  private static final String HEADER_ENTITY_SPAN = "&nbsp;<span id=\"header-entity\">";
  private static final String SPAN_CLOSE = "</span>";

  public static String getSeriesListHeader(SearchRequest searchRequest) {
    if (searchRequest.getEntity() == null) {
      return NUMBER_SERIES.getPlural();
    }
    switch (searchRequest.getEntity()) {
      case CMS:
        return concat(
            NUMBER_SERIES.getPlural(),
            " voor ",
            CMS.getDescription(),
            HEADER_ENTITY_SPAN,
            ellipsis(searchRequest.getEntityNameOrCode(), 40),
            SPAN_CLOSE);
      case COLLECTION:
        return concat(
            NUMBER_SERIES.getPlural(),
            " voor Prefix ",
            HEADER_ENTITY_SPAN,
            searchRequest.getEntityNameOrCode(),
            SPAN_CLOSE);
      case DEPARTMENT:
        return concat(
            NUMBER_SERIES.getPlural(),
            " voor ",
            DEPARTMENT.getDescription(),
            HEADER_ENTITY_SPAN,
            ellipsis(searchRequest.getEntityNameOrCode(), 40),
            SPAN_CLOSE);
      case INSTITUTE:
        return concat(
            NUMBER_SERIES.getPlural(),
            " voor ",
            INSTITUTE.getDescription(),
            HEADER_ENTITY_SPAN,
            searchRequest.getEntityNameOrCode(),
            SPAN_CLOSE);
      case REQUESTER:
        return concat(
            NUMBER_SERIES.getPlural(),
            " Aangevraagd door",
            HEADER_ENTITY_SPAN,
            searchRequest.getEntityNameOrCode(),
            SPAN_CLOSE);
      case USER:
        return concat(
            NUMBER_SERIES.getPlural(),
            " Ingevoerd door",
            HEADER_ENTITY_SPAN,
            searchRequest.getEntityNameOrCode(),
            SPAN_CLOSE);
      case NUMBER_SERIES:
      default:
        return NUMBER_SERIES.getPlural();
    }
  }

  static String getEditHeader(Entity entity, String nameOrCode) {
    return concat(entity.getDescription(), ' ', HEADER_ENTITY_SPAN, nameOrCode, SPAN_CLOSE);
  }

  static RenderSession includeComments(RenderSession session, String comments) throws RenderException {
    if (comments != null) {
      int numLines = count(comments, '\n') + 1;
      // NB 70 is the value of the cols attribute of the <textarea> for the comments
      int estimate = comments.length() / 70 + 1;
      int commentLineCount = Math.max(numLines, estimate);
      session.in("commentsRow").set("comments", comments).set("commentLineCount", commentLineCount);
    }
    return session;
  }

}
