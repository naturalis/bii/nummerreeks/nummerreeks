package nl.naturalis.bcd.resource;

import nl.naturalis.bcd.dao.RequesterDao;
import nl.naturalis.bcd.exception.BcdUserException;
import nl.naturalis.bcd.exception.NotFoundException;
import nl.naturalis.bcd.json.BcdModule;
import nl.naturalis.bcd.json.JsonParam;
import nl.naturalis.bcd.model.Requester;
import nl.naturalis.bcd.search.Entity;
import nl.naturalis.bcd.search.RequesterSearchRequest;
import nl.naturalis.bcd.search.RequesterSortColumn;
import nl.naturalis.bcd.search.SeriesSearchRequest;
import org.klojang.template.RenderException;
import org.klojang.template.RenderSession;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.jax.rs.annotations.Pac4JProfileManager;
import org.pac4j.jax.rs.annotations.Pac4JSecurity;
import org.pac4j.oidc.profile.azuread.AzureAdProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.util.Optional;

import static nl.naturalis.bcd.resource.ResourceUtil.getEditHeader;
import static nl.naturalis.bcd.resource.ResourceUtil.stream;
import static nl.naturalis.bcd.search.Entity.REQUESTER;
import static nl.naturalis.bcd.search.SortOrder.ASC;
import static nl.naturalis.common.ObjectMethods.ifEmpty;

@Path("/requester")
@Pac4JSecurity(clients = "AzureAdClient", authorizers = "mustBeAdmin")
@Produces(MediaType.TEXT_HTML)
public class RequesterResource extends CRUDResource<Requester, RequesterDao> {

  private static final Logger LOG = LoggerFactory.getLogger(RequesterResource.class);

  public RequesterResource() {
    super(new RequesterDao(), Requester.class);
  }

  @GET
  @Produces(MediaType.TEXT_HTML)
  public StreamingOutput list(
      @QueryParam("searchRequest") JsonParam<RequesterSearchRequest> jsonParam,
      @Context HttpServletRequest httpRequest,
      @Pac4JProfileManager ProfileManager<AzureAdProfile> pm)
      throws RenderException {
    this.pm = pm;
    this.profile = getProfile(pm);
    if (jsonParam == null) {
      jsonParam = new JsonParam<>();
    }
    RequesterSearchRequest sr = jsonParam.deserialize(RequesterSearchRequest.class);
    if (sr.getSortColumn() == null) {
      sr.setSortColumn(RequesterSortColumn.REQUESTER_NAME);
    }
    if (sr.getSortOrder() == null) {
      sr.setSortOrder(ASC);
    }
    return super.list(sr, httpRequest, pm);
  }

  @GET
  @Path("/show-requests")
  @Produces(MediaType.TEXT_HTML)
  public Response showRequests(
          @QueryParam("id") int entityId,
          @QueryParam("name") String name,
          @Pac4JProfileManager ProfileManager<AzureAdProfile> pm) {
    // Subtype of SearchRequest doesn't matter
    return showRequests(new SeriesSearchRequest(), Entity.REQUESTER, entityId, name, pm);
  }

  @GET
  @Path("/new")
  @Produces(MediaType.TEXT_HTML)
  public Response create(
          @Context HttpServletRequest httpRequest,
          @Pac4JProfileManager ProfileManager<AzureAdProfile> pm)
          throws RenderException {
    return super.create(Requester::new, httpRequest, pm);
  }

  @Override
  void beforeRenderCreatePage(RenderSession session) throws RenderException {
    session.set("header", "Nieuwe Aanvrager Invoeren");
  }

  @GET
  @Path("{id}/edit")
  @Produces(MediaType.TEXT_HTML)
  public Response edit(
          @PathParam("id") int id,
          @Context HttpServletRequest httpRequest,
          @Pac4JProfileManager ProfileManager<AzureAdProfile> pm)
      throws RenderException {
    this.pm = pm;
    this.profile = getProfile(pm);
    LOG.debug("edit (type={};id={})", typeName(), id);
    String searchRequest = ifEmpty(httpRequest.getParameter("searchRequest"), "{}");
    Optional<Requester> opt = dao.find(Requester.class, Requester::new, id);
    if (opt.isEmpty()) {
      throw new NotFoundException(REQUESTER, id);
    }
    Requester bean = opt.get();
    RenderSession session = newEditPageRenderSession();
    session
        .set("header", getEditHeader(REQUESTER, bean.getRequesterName()))
        .set("modelBean", BcdModule.writeJson(bean))
        .set("basePath", getBasePath(httpRequest))
        .set("searchRequest", searchRequest)
        .set("hasRequests", dao.hasRequests(id))
        .showRecursive();
    return stream(session::render);
  }

  @GET
  @Path("/save-or-update")
  public Response saveOrUpdate(@Context HttpServletRequest httpRequest, @Pac4JProfileManager ProfileManager<AzureAdProfile> pm) {
    return super.saveOrUpdate(httpRequest, Requester::getRequesterId, pm);
  }

  @DELETE
  @Path("/{id}")
  public Response delete(@PathParam("id") int id) {
    return super.delete(id);
  }

  void beforeDelete(int id) {
    if (dao.hasRequests(id)) {
      throw new BcdUserException("Er zijn nog nummerreeksen gekoppeld aan deze aanvrager");
    }
  }

  @POST
  @Path("/{id}/deactivate")
  public Response deactivate(@PathParam("id") int id) {
    return super.deactivate(id);
  }

  @POST
  @Path("/unique")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public boolean isUnique(Requester requester) {
    LOG.debug("isUnique (type={};name={})", typeName(), requester.getRequesterName());
    return dao.isUnique(requester);
  }
}
