package nl.naturalis.bcd.resource;

import nl.naturalis.bcd.BcdUtil;
import nl.naturalis.bcd.dao.LayoutDao;
import nl.naturalis.bcd.dao.SeriesDao;
import nl.naturalis.bcd.dao.StorageLocationDao;
import nl.naturalis.bcd.exception.NotFoundException;
import nl.naturalis.bcd.json.BcdModule;
import nl.naturalis.bcd.json.JsonParam;
import nl.naturalis.bcd.model.Layout;
import nl.naturalis.bcd.model.NumberSeries;
import nl.naturalis.bcd.model.Permission;
import nl.naturalis.bcd.model.StorageLocation;
import nl.naturalis.bcd.print.StorageLocationPrintService;
import nl.naturalis.bcd.search.StorageLocationSearchRequest;
import nl.naturalis.bcd.search.StorageLocationSortColumn;
import nl.naturalis.common.ExceptionMethods;
import nl.naturalis.common.invoke.BeanWriter;
import org.klojang.template.RenderException;
import org.klojang.template.RenderSession;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.jax.rs.annotations.Pac4JProfileManager;
import org.pac4j.jax.rs.annotations.Pac4JSecurity;
import org.pac4j.oidc.profile.azuread.AzureAdProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.Optional;

import static nl.naturalis.bcd.BcdUtil.ERR_INVALID_TOPDESK_ID;
import static nl.naturalis.bcd.BcdUtil.checkInput;
import static nl.naturalis.bcd.exception.NotAuthorizedException.notAuthorized;
import static nl.naturalis.bcd.listbox.ListBoxFactory.getRequesterOptions;
import static nl.naturalis.bcd.model.RequestStatus.CLOSED;
import static nl.naturalis.bcd.model.RequestStatus.LOCKED;
import static nl.naturalis.bcd.model.RequestStatus.OPEN;
import static nl.naturalis.bcd.resource.ResourceUtil.getEditHeader;
import static nl.naturalis.bcd.resource.ResourceUtil.includeComments;
import static nl.naturalis.bcd.resource.ResourceUtil.stream;
import static nl.naturalis.bcd.search.Entity.STORAGE_LOCATION;
import static nl.naturalis.bcd.search.SortOrder.ASC;
import static nl.naturalis.bcd.search.SortOrder.DESC;
import static nl.naturalis.common.ObjectMethods.ifEmpty;
import static nl.naturalis.common.StringMethods.ellipsis;
import static nl.naturalis.common.StringMethods.ifBlank;
import static nl.naturalis.common.check.CommonChecks.blank;

@Path("/storage")
@Pac4JSecurity(clients = "AzureAdClient", authorizers = "mustBeUser")
@Produces(MediaType.TEXT_HTML)
public class StorageLocationResource extends CRUDResource<StorageLocation, StorageLocationDao> {

  private static final Logger LOG = LoggerFactory.getLogger(StorageLocationResource.class);

  private static final BeanWriter<StorageLocation> WRITER = new BeanWriter<>(StorageLocation.class);

  public StorageLocationResource() {
    super(new StorageLocationDao(), StorageLocation.class);
  }

  @GET
  @Produces(MediaType.TEXT_HTML)
  public StreamingOutput list(
      @QueryParam("searchRequest") JsonParam<StorageLocationSearchRequest> jsonParam,
      @Context HttpServletRequest httpRequest,
      @Pac4JProfileManager ProfileManager<AzureAdProfile> pm)
      throws RenderException {
    this.pm = pm;
    this.profile = getProfile(pm);
    if (jsonParam == null) {
      jsonParam = new JsonParam<>();
    }
    StorageLocationSearchRequest sr = jsonParam.deserialize(StorageLocationSearchRequest.class);
    if (sr.getSortColumn() == null) {
      sr.setSortColumn(StorageLocationSortColumn.DATE_MODIFIED);
      sr.setSortOrder(DESC);
    } else if (sr.getSortOrder() == null) {
      if (sr.getSortColumn() == StorageLocationSortColumn.DATE_MODIFIED) {
        sr.setSortOrder(DESC);
      } else {
        sr.setSortOrder(ASC);
      }
    }
    return super.list(sr, httpRequest, pm);
  }

  @GET
  @Path("/{id}")
  @Produces(MediaType.TEXT_HTML)
  public Response view(
          @PathParam("id") int id,
          @Context HttpServletRequest httpRequest,
          @Pac4JProfileManager ProfileManager<AzureAdProfile> pm)
      throws RenderException, SQLException {
    this.pm = pm;
    this.profile = getProfile(pm);
    LOG.debug("view (type={};id={})", typeName(), id);
    // We ping-pong the search request from the list page, so that when the user closes
    // the form, he/she returns to the same page number within the list page. Note,
    // however, that we have no need to deserialize the search request here.
    String searchRequest = ifEmpty(httpRequest.getParameter("searchRequest"), "{}");
    Optional<StorageLocation> opt = dao.doFind(id);
    if (opt.isEmpty()) {
      throw new NotFoundException(STORAGE_LOCATION, id);
    }
    StorageLocation bean = opt.get();
    RenderSession session = newRenderSession("view");
    Permission permission = hasPermission();
    String navbar = navbar();
    session
        .insert(bean)
        .set("basePath", getBasePath(httpRequest))
        .set("searchRequest", searchRequest)
        .set("permission", permission.name())
        .set("labelCount", bean.getLabels().size())
        .show(navbar, "bsDialog");
    if (bean.getStatus() == OPEN) {
      session.in("statusOpen").set("storageLocationId", id).show();
    } else if (bean.getStatus() == LOCKED) {
      session.in("statusLocked").set("storageLocationId", id).show();
    } else /* CLOSED */ {
      session.in("statusClosed").set("storageLocationId", id).show();
      session.populate("layoutDisplay", bean);
    }
    String comments = ifBlank(bean.getComments(), null);
    includeComments(session, comments);
    return stream(session::render);
  }

  @GET
  @Path("/new")
  public Response create(
          @Context HttpServletRequest httpRequest,
          @Pac4JProfileManager ProfileManager<AzureAdProfile> pm)
          throws Exception {
    return super.create(StorageLocation::new, httpRequest, pm);
  }

  @Override
  void beforeRenderCreatePage(RenderSession session) throws RenderException {
    session.set("header", "Upload File").set("requesterOptions", getRequesterOptions());
  }

  @GET
  @Path("{id}/edit")
  public Response edit(
          @PathParam("id") int id,
          @Context HttpServletRequest httpRequest,
          @Pac4JProfileManager ProfileManager<AzureAdProfile> pm)
      throws Exception {
    this.pm = pm;
    this.profile = getProfile(pm);
    LOG.debug("edit (type={};id={})", typeName(), id);
    String searchRequest = ifEmpty(httpRequest.getParameter("searchRequest"), "{}");
    Optional<StorageLocation> opt = dao.doFind(id);
    if (opt.isEmpty()) {
      throw new NotFoundException(STORAGE_LOCATION, id);
    }
    StorageLocation bean = opt.get();
    RenderSession session = newEditPageRenderSession();
    Permission permission = hasPermission();
    session
        .set("header", getEditHeader(STORAGE_LOCATION, ellipsis(bean.getPrefixes(), 20)))
        .set("modelBean", BcdModule.writeJson(bean))
        .set("basePath", getBasePath(httpRequest))
        .set("searchRequest", searchRequest)
        .set("permission", permission.name())
        .set("requesterOptions", getRequesterOptions())
        .insert(bean)
        .populate("prefixDisplay", bean)
        .populate("extraActionButtons", bean)
        .populate("labelDisplay", bean)
        .showRecursive(navbar(), "bsDialog");
    return stream(session::render);
  }

  @POST
  @Path("/upload")
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  public Response upload(@BeanParam StorageLocation edited,
                         @Context HttpServletRequest httpRequest,
                         @Pac4JProfileManager ProfileManager<AzureAdProfile> pm)
      throws Throwable {
    this.pm = pm;
    this.profile = getProfile(pm);
    LOG.debug("upload (type={})", typeName());
    if (edited.getStorageLocationId() == null) {
      checkTopDeskId(edited, true);
      edited.setStatus(OPEN);
      edited.setDateCreated(LocalDateTime.now());
      edited.setDateModified(edited.getDateCreated());
      edited.setModifiedBy(getSessionUserId());
      dao.doInsert(edited);
    } else {
      int id = edited.getStorageLocationId();
      Optional<StorageLocation> opt = dao.doFind(id);
      if (opt.isEmpty()) {
        throw new NotFoundException(STORAGE_LOCATION, id);
      }
      StorageLocation original = opt.get();
      checkTopDeskId(edited, BcdUtil.isValidLegacyTopDeskId( original.getTopDeskId() ));
      boolean withNewSegmentCount =
              !Objects.equals(original.getPrefixSegmentCount(), edited.getPrefixSegmentCount());
      WRITER.transfer(edited, original);
      original.setDateModified(LocalDateTime.now());
      original.setModifiedBy(getSessionUserId());
      dao.doUpdate(original, withNewSegmentCount);
    }
    int id = edited.getStorageLocationId();
    return forwardTo("edit", id, httpRequest);
  }

  @DELETE
  @Path("/{id}")
  public Response delete(@PathParam("id") int id) {
    LOG.debug("delete (type={};id={})", typeName(), id);
    try {
      if (dao.doDelete(id) == 0) {
        LOG.warn("Nothing deleted");
      }
    } catch (SQLException e) {
      throw ExceptionMethods.uncheck(e);
    }
    return Response.noContent().build();
  }

  @POST
  @Path("/unique")
  @Pac4JSecurity(clients = "AzureAdClient", authorizers = "mustBeUser")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  /* Check topdesk ID */
  public boolean isUnique(StorageLocation storageLocation, @Pac4JProfileManager ProfileManager<AzureAdProfile> pm) {
    this.pm = pm;
    this.profile = getProfile(pm);
    LOG.debug("isUnique (type={};id={})", typeName(), storageLocation.getStorageLocationId());
    return dao.isUnique(storageLocation);
  }

  @GET
  @Path("/{id}/lock")
  public Response lock(@PathParam("id") int id,
                       @Context HttpServletRequest httpRequest,
                       @Pac4JProfileManager ProfileManager<AzureAdProfile> pm) {
    this.pm = pm;
    this.profile = getProfile(pm);
    LOG.debug("lock (type={};id={})", typeName(), id);
    Optional<StorageLocation> opt = dao.findPlain(id);
    if (opt.isEmpty()) {
      throw new NotFoundException(STORAGE_LOCATION, id);
    }
    StorageLocation storageLocation = opt.get();
    if (storageLocation.getStatus() != OPEN) { // Bad stuff
      throw notAuthorized("Illegal attempt to lock storage location");
    }
    LocalDateTime now = LocalDateTime.now();
    storageLocation.setDateModified(now);
    storageLocation.setModifiedBy(getSessionUserId());
    storageLocation.setStatus(storageLocation.getStatus().next());
    dao.updatePlain(storageLocation);
    return forwardTo(id, httpRequest);
  }

  @GET
  @Path("/{id}/unlock")
  public Response unlock(@PathParam("id") int id,
                         @Context HttpServletRequest httpRequest,
                         @Pac4JProfileManager ProfileManager<AzureAdProfile> pm) {
    this.pm = pm;
    this.profile = getProfile(pm);
    LOG.debug("unlock (type={};id={})", typeName(), id);
    Optional<StorageLocation> opt = dao.findPlain(id);
    if (opt.isEmpty()) {
      throw new NotFoundException(STORAGE_LOCATION, id);
    }
    StorageLocation storageLocation = opt.get();
    if (storageLocation.getStatus() != LOCKED) { // Bad stuff
      throw notAuthorized("Illegal attempt to unlock storage location");
    }
    LocalDateTime now = LocalDateTime.now();
    storageLocation.setDateModified(now);
    storageLocation.setModifiedBy(getSessionUserId());
    storageLocation.setStatus(OPEN);
    dao.updatePlain(storageLocation);
    return forwardTo("edit", id, httpRequest);
  }

  @GET
  @Path("/{id}/archive")
  public Response archive(@PathParam("id") int id,
                          @Context HttpServletRequest httpRequest,
                          @Pac4JProfileManager ProfileManager<AzureAdProfile> pm) {
    this.pm = pm;
    this.profile = getProfile(pm);
    LOG.debug("archive (type={};id={})", typeName(), id);
    Optional<StorageLocation> opt = dao.findPlain(id);
    if (opt.isEmpty()) {
      throw new NotFoundException(STORAGE_LOCATION, id);
    }
    StorageLocation storageLocation = opt.get();
    if (storageLocation.getStatus() != LOCKED) { // Bad stuff
      throw notAuthorized("Illegal attempt to archive storage location");
    }
    LocalDateTime now = LocalDateTime.now();
    storageLocation.setDateModified(now);
    storageLocation.setModifiedBy(getSessionUserId());
    if (storageLocation.getDateFirstClosed() == null) {
      storageLocation.setDateFirstClosed(now);
    }
    storageLocation.setDispenseCount(storageLocation.getDispenseCount() + 1);
    storageLocation.setStatus(CLOSED);
    dao.updatePlain(storageLocation);
    return forwardTo(id, httpRequest);
  }

  @GET
  @Path("/{id}/unarchive")
  public Response unarchive(@PathParam("id") int id,
                            @Context HttpServletRequest httpRequest,
                            @Pac4JProfileManager ProfileManager<AzureAdProfile> pm) {
    this.pm = pm;
    this.profile = getProfile(pm);
    LOG.debug("unarchive (type={};id={})", typeName(), id);
    Optional<StorageLocation> opt = dao.findPlain(id);
    if (opt.isEmpty()) {
      throw new NotFoundException(STORAGE_LOCATION, id);
    }
    StorageLocation storageLocation = opt.get();
    if (storageLocation.getStatus() != CLOSED) { // Bad stuff
      throw notAuthorized("Illegal attempt to unarchive storage location");
    }
    LocalDateTime now = LocalDateTime.now();
    storageLocation.setDateModified(now);
    storageLocation.setModifiedBy(getSessionUserId());
    storageLocation.setStatus(LOCKED);
    dao.updatePlain(storageLocation);
    return forwardTo(id, httpRequest);
  }

  @GET
  @Path("/{requestId}/print/{layoutId}")
  @Produces("application/pdf")
  public Response printLabels(@PathParam("requestId") int id, @PathParam("layoutId") int layoutId)
      throws SQLException {
    LOG.debug("printLabels (type={};id={};layoutId={})", typeName(), id, layoutId);
    Layout layout;
    Optional<Layout> optLayout = new LayoutDao().find(Layout.class, Layout::new, layoutId);
    if (optLayout.isPresent()) {
      layout = optLayout.get();
    } else {
      throw new WebApplicationException("Cannot find layout with id {}", layoutId);
    }
    Optional<StorageLocation> opt = dao.doFind(id);
    if (opt.isEmpty()) {
      throw new NotFoundException(STORAGE_LOCATION, id);
    }
    StorageLocation storageLocation = opt.get();
    StorageLocationPrintService svc = new StorageLocationPrintService(storageLocation, layout);
    String fileName =
        String.format(
            "%s.%s.%s.pdf",
            DateTimeFormatter.ofPattern("uuuuMMddHHmmss").format(LocalDateTime.now()),
            storageLocation,
            layout.getLabelUsage().name().toLowerCase());
    StreamingOutput output = svc::printLabels;
    storageLocation.setLastUsedLayout(layout.toString());
    dao.updatePlain(storageLocation);
    return Response.ok(output)
        .header("Content-Disposition", "attachment; filename=" + fileName)
        .build();
  }

  private void checkTopDeskId(StorageLocation storageLocation, boolean strict) {
    String id =
        checkInput(storageLocation.getTopDeskId())
            .isNot(blank(), "TOPDesk Nummer verplicht")
            .ok(String::strip);
    if (strict) {
      checkInput(id).is(BcdUtil::isValidTopDeskId, ERR_INVALID_TOPDESK_ID, id);
    } else {
      checkInput(id).is(BcdUtil::isValidLegacyTopDeskId, ERR_INVALID_TOPDESK_ID, id);
    }
    storageLocation.setTopDeskId(id);
    checkInput(storageLocation).is(dao::isUnique, "TOPDesk nummer bestaat al");
    NumberSeries series = new NumberSeries();
    series.setTopDeskId(id);
    SeriesDao seriesDao = new SeriesDao();
    checkInput(series).is(seriesDao::isUnique, "Er is al een nummerreeks met dit TOPDesk nummer");
  }
}
