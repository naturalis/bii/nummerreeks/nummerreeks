package nl.naturalis.bcd.resource;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.List;
import java.util.Properties;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import org.klojang.template.RenderException;
import org.klojang.template.RenderSession;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.jax.rs.annotations.Pac4JCallback;
import org.pac4j.jax.rs.annotations.Pac4JLogout;
import org.pac4j.jax.rs.annotations.Pac4JProfileManager;
import org.pac4j.jax.rs.annotations.Pac4JSecurity;
import org.pac4j.oidc.profile.azuread.AzureAdProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static nl.naturalis.bcd.resource.HttpResponses.streamingResponse;
import static nl.naturalis.bcd.resource.ResourceUtil.newRenderSession;

@Path("/")
public class HomeResource {

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(HomeResource.class);

  private AzureAdProfile profile;

  @GET
  public Response home() {
    return Response.seeOther(URI.create("/series")).build();
  }

  /**
   * Intercepts requests for a favicon in case we are running under "/".
   *
   * @return the favicon image
   */
  @GET
  @Path("/favicon.ico")
  @Produces("image/x-icon")
  public Response favicon() {
    InputStream is = getClass().getResourceAsStream("/static/images/favicon.ico");
    return streamingResponse(is, "image/x-icon");
  }

  @GET
  @Path("/help/search")
  @Pac4JSecurity(clients = "AzureAdClient", authorizers = "mustBeAuth")
  @Produces(MediaType.TEXT_HTML)
  public StreamingOutput searchHelp(@Pac4JProfileManager ProfileManager<AzureAdProfile> pm) throws RenderException {
    this.profile = getProfile(pm);
    RenderSession session = newRenderSession("SearchHelp").show(navbar(), "logo");
    return session::render;
  }

  @GET
  @Pac4JSecurity(clients = "AzureAdClient", authorizers = "mustBeAuth")
  @Path("/help/about")
  @Produces(MediaType.TEXT_HTML)
  public StreamingOutput about(@Pac4JProfileManager ProfileManager<AzureAdProfile> pm) throws IOException, RenderException {
    this.profile = getProfile(pm);
    RenderSession session = newRenderSession("About")
            .show(navbar(), "logo");
    try (InputStream in = getClass().getResourceAsStream("/git.properties")) {
      if (in != null) {
        Properties props = new Properties(32);
        props.load(in);
        session.insert(props);
      }
    }
    return session::render;
  }

  @SuppressWarnings("VoidMethodAnnotatedWithGET")
  @GET
  @Path("/callback/AzureAdClient")
  @Pac4JCallback(multiProfile = false, renewSession = true, defaultUrl = "/series", saveInSession = true, defaultClient = "AzureAdClient")
  public void callbackGet() {
    // nothing to do here, Pac4J handles everything
  }

  @POST
  @Path("/callback/AzureAdClient")
  @Pac4JCallback(multiProfile = false, renewSession = true, defaultUrl = "/series", saveInSession = true, defaultClient = "AzureAdClient" )
  public void callbackPost() {
    // nothing to do here, Pac4J handles everything
  }

  @SuppressWarnings("VoidMethodAnnotatedWithGET")
  @GET
  @Path("/logout")
  @Pac4JSecurity(clients = "AzureAdClient", authorizers = "mustBeAuth")
  @Pac4JLogout(destroySession = { true }, centralLogout = { true })
  public void logout() {
    // nothing to do here, Pac4J handles everything
  }

  @GET
  @Path("/logoff")
  @Produces(MediaType.TEXT_HTML)
  public StreamingOutput logOff() throws RenderException {
    RenderSession session = newRenderSession("Bye").show("navbar", "logo");
    return session::render;
  }

  @GET
  @Path(("/mustBeAuth"))
  @Produces(MediaType.TEXT_HTML)
  public StreamingOutput notAuthorized() throws RenderException {
    RenderSession session = newRenderSession("NotAuthorized").show("navbar","logo");
    return session::render;
  }

  @GET
  @Path(("/mustBeAdmin"))
  @Pac4JSecurity(clients = "AzureAdClient", authorizers = "mustBeAuth")
  @Produces(MediaType.TEXT_HTML)
  public StreamingOutput notAdmin(@Pac4JProfileManager ProfileManager<AzureAdProfile> pm) throws RenderException {
    RenderSession session = newRenderSession("NotAuthorized").show("navbar","logo");
    return session::render;
  }

  private AzureAdProfile getProfile(ProfileManager<AzureAdProfile> pm) {
    List<AzureAdProfile> profiles = pm.getAll(true);
    if (profiles.size() != 1) {
      // Note: should not be possible!
      LOG.warn("Found more than one profile for this user: {}. Forced logout.", pm.getAll(true).get(0).getUpn());
      pm.remove(true);
      pm.logout();
      throw new WebApplicationException("Invalid user profile", Response.Status.UNAUTHORIZED);
    }
    return profiles.get(0);
  }

  private String navbar() {
    if (profile.getRoles().contains("ADMIN")) {
      return "adminNavbar";
    }
    return "navbar";
  }

}
