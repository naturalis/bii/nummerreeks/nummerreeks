package nl.naturalis.bcd;

import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.configuration.ConfigurationSourceProvider;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import nl.naturalis.bcd.dao.UserDao;
import nl.naturalis.bcd.health.DbHealthCheck;
import nl.naturalis.bcd.jaxrs.ExceptionMapperBase;
import nl.naturalis.bcd.managed.Database;
import nl.naturalis.bcd.migrate.MigrateDbCommand;
import nl.naturalis.bcd.model.*;
import nl.naturalis.bcd.exception.NotAuthorizedException;
import nl.naturalis.bcd.resource.SeriesResource;
import nl.naturalis.bcd.security.AzureAdLogoutActionBuilder;
import nl.naturalis.bcd.security.IsKnownUserAuthorizer;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.klojang.db.SQL;
import org.klojang.db.SQLInsert;
import org.klojang.db.SQLQuery;
import org.pac4j.core.authorization.authorizer.RequireAnyPermissionAuthorizer;
import org.pac4j.core.authorization.generator.AuthorizationGenerator;
import org.pac4j.core.client.Clients;
import org.pac4j.core.config.Config;
import org.pac4j.core.profile.UserProfile;
import org.pac4j.dropwizard.Pac4jBundle;
import org.pac4j.dropwizard.Pac4jFactory;
import org.pac4j.oidc.client.AzureAdClient;
import org.pac4j.oidc.profile.azuread.AzureAdProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.TimeZone;

import static nl.naturalis.common.ArrayMethods.pack;

/* Start here... */
public class BcdServer extends Application<BcdConfig> {

  private static final Logger LOG = LoggerFactory.getLogger(BcdServer.class);

  public static void main(String[] args) throws Exception {
    new BcdServer().run(args);
  }

  @SuppressWarnings("Convert2Diamond")
  private final Pac4jBundle<BcdConfig> pac4j =
        new Pac4jBundle<BcdConfig>() {
          @Override
          public Pac4jFactory getPac4jFactory(BcdConfig configuration) {
            return configuration.pac4jFactory;
          }
        };

  @Override
  public String getName() {
    return "Barcode Dispenser";
  }

  @Override
  public void initialize(Bootstrap<BcdConfig> bootstrap) {

    // Enable env var substitution in config file
    ConfigurationSourceProvider csp = bootstrap.getConfigurationSourceProvider();
    EnvironmentVariableSubstitutor evs = new EnvironmentVariableSubstitutor(false);
    SubstitutingSourceProvider ssp = new SubstitutingSourceProvider(csp, evs);
    bootstrap.setConfigurationSourceProvider(ssp);
    bootstrap.addBundle(pac4j);

    // Serve static assets from /src/main/resources/static
    bootstrap.addBundle(new AssetsBundle("/static/"));
    bootstrap.addCommand(new MigrateDbCommand());
  }

  @Override
  public void run(BcdConfig cfg, Environment env) throws Exception {

    TimeZone tz = TimeZone.getTimeZone("Europe/Amsterdam");
    TimeZone.setDefault(tz);

    BcdConfig.INSTANCE = cfg;

    env.lifecycle().manage(new Database(cfg.database));

    env.jersey().register(new AbstractBinder() {
      @Override
      protected void configure() {

        // 1. Modify the config:
        Config config = pac4j.getConfig();

        // 2. Add one or more Authorizer(s)
        IsKnownUserAuthorizer<UserProfile> isKnownUserAuthorizer = new IsKnownUserAuthorizer<>();
        // ... and define a redirection URL
        isKnownUserAuthorizer.setRedirectionUrl("/?mustBeAuth");
        config.addAuthorizer("mustBeUser", isKnownUserAuthorizer);

        // 3. define permissions (when needed):
        RequireAnyPermissionAuthorizer<UserProfile> mustBeEditor = new RequireAnyPermissionAuthorizer<>(
              "EDIT");
        // You cannot define a redirection URL
        config.addAuthorizer("mustBeEditor", mustBeEditor);

        // 4. define an AuthorizationGenerator
        AuthorizationGenerator authorizationGenerator = (webContext, userProfile) -> loadProfile(
              userProfile);

        Clients clients = config.getClients();
        clients.setAuthorizationGenerators(authorizationGenerator);
        // so that we can inject the config in resources if needed
        Optional<AzureAdClient> client = clients.findClient(AzureAdClient.class);
        if (client.isPresent()) {
          AzureAdClient azureAdClient = client.get();
          azureAdClient.setLogoutActionBuilder(new AzureAdLogoutActionBuilder());
        }
        // Config is now ready
        bind(pac4j.getConfig()).to(Config.class);
      }
    });

    env.jersey().register(MultiPartFeature.class);
    env.jersey().packages(pack(
          SeriesResource.class.getPackageName(),
          ExceptionMapperBase.class.getPackageName()));
    env.healthChecks().register("Database Health", new DbHealthCheck());
    LOG.info("{} setup complete", getName());
  }

  private Optional<UserProfile> loadProfile(UserProfile userProfile) {
    if (userProfile instanceof AzureAdProfile profile) {
      String upn = profile.getUpn();
      LOG.debug("upn: {}", upn);

      UserDao userDao = new UserDao();
      Optional<User> user = userDao.findByUpn(upn);
      if (user.isPresent() && user.get().isActive()) {
        UserRole role = user.get().getRole();
        userProfile.addRole(role.name());
        if (role.equals(UserRole.ADMIN) || role.equals(UserRole.USER)) {
          userProfile.addPermission(Permission.EDIT.name());
        } else if (role.equals(UserRole.VIEWER)) {
          userProfile.addPermission(Permission.VIEW.name());
        }
        LOG.debug("authorization:: user with upn: {} has role: {} ({}) and permission: {}",
              user.get().getUpn(),
              role.name(),
              role.intValue(),
              userProfile.getPermissions());
      } else {
        LOG.debug("authorization:: unknown or inactive user: {}", upn);
        throw new NotAuthorizedException(
              "Je bent niet geautoriseerd om deze applicatie te gebruiken.");
      }
      LOG.debug("authorization:: user ({}) has been recognised. Added role: {}",
            userProfile.getUsername(),
            userProfile.getRoles());
      return Optional.of(userProfile);
    }

    LOG.debug("authorization:: unknown profile: {}. No role has been added",
          userProfile.getUsername());
    throw new NotAuthorizedException(
          "Je bent niet geautoriseerd om deze applicatie te gebruiken.");
  }


}
