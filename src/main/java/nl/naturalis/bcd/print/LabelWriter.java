package nl.naturalis.bcd.print;

public interface LabelWriter<LABEL extends Label> {

  void writeLabel(LABEL label);
}
