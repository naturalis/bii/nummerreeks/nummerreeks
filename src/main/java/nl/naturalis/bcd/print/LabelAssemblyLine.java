package nl.naturalis.bcd.print;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 * Sets up a thread pool, runs the label assemblers and collects the label info they produce in a
 * map keyed on the number within the number series.
 */
class LabelAssemblyLine<T extends Label, U extends LabelAssembler<T>> {

  private static final Logger LOG = LoggerFactory.getLogger(LabelAssemblyLine.class);

  private final PrintSessionBase<?, T> session;
  private final List<U> assemblers;

  LabelAssemblyLine(PrintSessionBase<?, T> session, List<U> labelAssemblers) {
    this.session = session;
    this.assemblers = labelAssemblers;
  }

  /**
   * Returns a map with page numbers as keys and lists of {@link Label} instances as values.
   *
   * @return
   */
  Map<Integer, List<T>> collectLabels() {
    LOG.trace("Running {} cell assemblers", assemblers.size());
    ExecutorService threadPool = session.getThreadPool();
    try {
      List<Future<T>> futures = threadPool.invokeAll(assemblers);
      Map<Integer, List<T>> map = new HashMap<>();
      for (Future<T> future : futures) {
        T cell = future.get();
        map.computeIfAbsent(cell.getPage(), k -> new ArrayList<>(50)).add(cell);
      }
      return map;
    } catch (Throwable e) {
      session.abort(e);
      return null;
    }
  }
}
