package nl.naturalis.bcd.print;

public enum SplitMode {
  /* Entire label text fits on 1 line */
  DONT_SPLIT,
  /* Prefix on 1st line; number on 2nd line */
  GLUE_PREFIX,
  /* Institute code on 1st line; collection code & number on 2nd line */
  SPLIT_PREFIX;
}
