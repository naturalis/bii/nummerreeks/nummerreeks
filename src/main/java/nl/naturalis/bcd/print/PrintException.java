package nl.naturalis.bcd.print;

import nl.naturalis.bcd.exception.BcdRuntimeException;

public class PrintException extends BcdRuntimeException {

  public PrintException(String message) {
    super(message);
  }

  public PrintException(Throwable cause) {
    super(cause);
  }

  public PrintException(String message, Throwable cause) {
    super(message, cause);
  }
}
