package nl.naturalis.bcd.print.std;

import java.io.IOException;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import nl.naturalis.bcd.print.LabelWriter;
import static org.apache.pdfbox.pdmodel.font.PDType1Font.HELVETICA;
import static org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject.createFromByteArray;
import static nl.naturalis.bcd.print.std.StdLabel.*;

class StdLabelWriter implements LabelWriter<StdLabel> {

  private final StdPrintSession session;
  private final PDPageContentStream pageContents;

  StdLabelWriter(StdPrintSession session, PDPageContentStream pageContents) {
    this.session = session;
    this.pageContents = pageContents;
  }

  @Override
  public void writeLabel(StdLabel label) {
    try {
      writeDataMatrix(label);
      writeText(label);
    } catch (IOException e) {
      session.abort(e);
    }
  }

  private void writeDataMatrix(StdLabel label) throws IOException {
    float x = label.getLayoutInfo(IMAGE_X);
    float y = label.getLayoutInfo(IMAGE_Y);
    float sz = label.getLayoutInfo(IMAGE_SIZE);
    PDImageXObject img = createFromByteArray(session.getTargetDocument(), label.getBytes(), "foo");
    pageContents.drawImage(img, x, y, sz, sz);
  }

  private void writeText(StdLabel label) throws IOException {
    if (label.getSecondLine() == null) {
      writeOnSingleLine(label);
    } else {
      writeOnTwoLines(label);
    }
  }

  private void writeOnSingleLine(StdLabel label) throws IOException {
    float x = label.getLayoutInfo(TEXT_X);
    float y = label.getLayoutInfo(TEXT_LINE0_Y);
    float fontSize = label.getLayoutInfo(FONT_SIZE_LINE0);
    String text = label.getFirstLine();
    PDPageContentStream pageContents = this.pageContents;
    pageContents.beginText();
    pageContents.setFont(HELVETICA, fontSize);
    pageContents.newLineAtOffset(x, y);
    pageContents.showText(text);
    pageContents.endText();
  }

  private void writeOnTwoLines(StdLabel label) throws IOException {
    float x = label.getLayoutInfo(TEXT_X);
    float yLine0 = label.getLayoutInfo(TEXT_LINE0_Y);
    float yLine1 = label.getLayoutInfo(TEXT_LINE1_Y);
    String line0 = label.getFirstLine();
    String line1 = label.getSecondLine();
    float fontSizeLine0 = label.getLayoutInfo(FONT_SIZE_LINE0);
    float fontSizeLine1 = label.getLayoutInfo(FONT_SIZE_LINE1);
    PDPageContentStream pageContents = this.pageContents;
    pageContents.beginText();
    pageContents.setFont(HELVETICA, fontSizeLine0);
    pageContents.newLineAtOffset(x, yLine0);
    pageContents.showText(line0);
    pageContents.endText();
    pageContents.beginText();
    pageContents.setFont(HELVETICA, fontSizeLine1);
    pageContents.newLineAtOffset(x, yLine1);
    pageContents.showText(line1);
    pageContents.endText();
  }
}
