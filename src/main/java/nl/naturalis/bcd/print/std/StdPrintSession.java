package nl.naturalis.bcd.print.std;

import java.util.Arrays;
import java.util.List;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.bcd.model.Layout;
import nl.naturalis.bcd.model.NumberSeries;
import nl.naturalis.bcd.print.NumberSeriesPrintSession;
import nl.naturalis.bcd.print.LabelWriter;
import nl.naturalis.bcd.print.LayoutDataFactory;
import nl.naturalis.bcd.print.shared.FontSizeComputer;
import nl.naturalis.bcd.print.shared.FontSizeInfoProvider;
import nl.naturalis.common.ArrayMethods;
import static nl.naturalis.common.ObjectMethods.ifNull;

public class StdPrintSession extends NumberSeriesPrintSession<StdLayoutData, StdLabel> {

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(StdPrintSession.class);

  private final FontSizeInfoProvider fontSizeInfoProvider;

  public StdPrintSession(NumberSeries series, Layout layout) {
    super(series, layout);
    FontSizeComputer fsc = new FontSizeComputer(this, getLayoutData().getTextWidth());
    this.fontSizeInfoProvider = fsc.getFontSizeInfoProvider();
  }

  @Override
  protected List<StdLabelAssembler> getLabelAssemblers() {
    NumberSeries ns = getNumberSeries();
    boolean suffixesOnly = getLayoutData().isSuffixesOnly();
    int init = suffixesOnly ? 0 : 1;
    String[] suffixes = ifNull(getLayoutData().getSuffixes(), ArrayMethods.EMPTY_STRING_ARRAY);
    int sz = (init + suffixes.length) * ns.getAmount();
    StdLabelAssembler[] assemblers = new StdLabelAssembler[sz];
    int i = 0;
    for (int nr = ns.getFirstNumber(); nr <= ns.getLastNumber(); ++nr) {
      if (!suffixesOnly) {
        assemblers[i] = new StdLabelAssembler(this, i, nr, null);
        ++i;
      }
      for (String s : suffixes) {
        assemblers[i] = new StdLabelAssembler(this, i, nr, s);
        ++i;
      }
    }
    return Arrays.asList(assemblers);
  }

  @Override
  protected LayoutDataFactory<StdLayoutData> getLayoutDataFactory(Layout layout) {
    return new StdLayoutDataFactory(layout);
  }

  @Override
  protected LabelWriter<StdLabel> createLabelWriter(PDPageContentStream pageContents) {
    return new StdLabelWriter(this, pageContents);
  }

  FontSizeInfoProvider getFontSizeInfoProvider() {
    return fontSizeInfoProvider;
  }
}
