package nl.naturalis.bcd.print.std;

import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.bcd.model.Layout;
import nl.naturalis.bcd.print.LayoutDataFactory;
import nl.naturalis.bcd.print.LayoutException;
import static nl.naturalis.common.check.CommonChecks.atMost;
import static nl.naturalis.common.check.CommonChecks.empty;

/**
 * A factory for {@link StdLayoutData} instances.
 *
 * @author Ayco Holleman
 */
public class StdLayoutDataFactory extends LayoutDataFactory<StdLayoutData> {

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(StdLayoutDataFactory.class);

  public static final Set<String> USER_INPUT =
      Set.of(
          "paperSize",
          "printAreaTop",
          "printAreaLeft",
          "rowCount",
          "columnCount",
          "columnWise",
          "rowHeight",
          "columnWidth",
          "labelHeight",
          "labelWidth",
          "imgSize",
          "textWidth",
          "glueAll",
          "gluePrefix",
          "numberEmphasis",
          "suffixes",
          "suffixesOnly");

  public StdLayoutDataFactory(Layout layout) {
    super(layout, StdLayoutData.class);
  }

  @Override
  protected Set<String> getUserConfigurableProperties() {
    return USER_INPUT;
  }

  protected void validate(StdLayoutData data) throws LayoutException {

    super.validate(data);

    if (Boolean.TRUE.equals(data.getSuffixesOnly())) {
      checkThat(data.getSuffixes())
          .isNot(empty(), "Tenminste 1 suffix verplicht als suffixesOnly waarde \"true\" heeft");
    }

    checkThat(data.getLabelHeight())
        .is(
            atMost(),
            data.getRowHeight(),
            "labelHeight (%smm) > rowHeight (%smm)",
            data.getLabelHeight(),
            data.getRowHeight());

    checkThat(data.getImgSize())
        .is(
            atMost(),
            data.getLabelHeight(),
            "imgSize (%smm) > labelHeight (%smm)",
            data.getImgSize(),
            data.getLabelHeight());

    checkThat(data.getLabelWidth())
        .is(
            atMost(),
            data.getColumnWidth(),
            "labelWidth (%smm) > columnWidth (%smm)",
            data.getLabelWidth(),
            data.getColumnWidth());

    double contentWidth = data.getImgSize() + data.getImgMarginRight() + data.getTextWidth();

    checkThat(contentWidth)
        .is(
            atMost(),
            data.getLabelWidth(),
            "imgSize + imgMarginRight + labelTextWidth (%smm) > labelWidth (%smm)",
            contentWidth,
            data.getLabelWidth());

    /*
     * The last column does not need to fit entirely on the page. Only the part occupied by the
     * label (which always is in the top-left of a cell) does.
     */
    double neededWidth =
        ((data.getColumnCount() - 1) * data.getColumnWidth()) + data.getLabelWidth();
    double availableWidth = data.getPageWidthMillimeters() - data.getPrintAreaLeft();
    checkThat(neededWidth)
        .is(
            atMost(),
            availableWidth,
            "Minimaal benodigde horizontale ruimte: %smm. Beschikbaar: %smm.",
            neededWidth,
            availableWidth);

    double neededHeight = ((data.getRowCount() - 1) * data.getRowHeight()) + data.getLabelHeight();
    double availableHeight = data.getPageHeightMillimeters() - data.getPrintAreaTop();
    checkThat(neededHeight)
        .is(
            atMost(),
            availableHeight,
            "Minimaal benodigde verticale ruimte: %smm. Beschikbaar: %smm.",
            neededHeight,
            availableHeight);
  }
}
