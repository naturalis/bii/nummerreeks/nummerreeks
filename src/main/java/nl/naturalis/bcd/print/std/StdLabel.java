package nl.naturalis.bcd.print.std;

import nl.naturalis.bcd.print.Label;

class StdLabel implements Label {

  static final int IMAGE_X = 0;
  static final int IMAGE_Y = 1;
  static final int IMAGE_SIZE = 2;
  static final int FONT_SIZE_LINE0 = 3;
  static final int FONT_SIZE_LINE1 = 4;
  static final int TEXT_X = 5;
  static final int TEXT_LINE0_Y = 6;
  static final int TEXT_LINE1_Y = 7;

  static final int LAYOUT_INFO_ITEM_COUNT = 8;

  static StdLabel create(
      int number,
      String suffix,
      byte[] bytes,
      int[] cellInfo,
      float[] layoutInfo,
      String line0,
      String line1) {
    return new StdLabel(number, suffix, bytes, cellInfo, layoutInfo, line0, line1);
  }

  /*
   * A single number from the number series
   */
  private int number;

  /*
   * A single suffix from "suffixes" setting in the layout configuration
   */
  private String suffix;

  /*
   * The image/jpeg bytes for the data matrix
   */
  private byte[] bytes;

  /*
   * Page, row and column of the cell
   */
  private int[] cellInfo;

  /*
   * Font sizes and positions of the components within the label
   */
  private float[] layoutInfo;

  /*
   * The entire or partial label text
   */
  private String line0;

  /*
   * The partial label text or null
   */
  private String line1;

  private StdLabel(
      int number,
      String suffix,
      byte[] bytes,
      int[] cellInfo,
      float[] layoutInfo,
      String line0,
      String line1) {
    this.number = number;
    this.suffix = suffix;
    this.bytes = bytes;
    this.cellInfo = cellInfo;
    this.layoutInfo = layoutInfo;
    this.line0 = line0;
    this.line1 = line1;
  }

  public int getCellInfo(int index) { // PAGE, ROW or COLUMN
    return cellInfo[index];
  }

  int getNumber() {
    return number;
  }

  String getSuffix() {
    return suffix;
  }

  byte[] getBytes() {
    return bytes;
  }

  float getLayoutInfo(int index) { // IMAGE_X, IMAGE_Y, etc.
    return layoutInfo[index];
  }

  String getFirstLine() {
    return line0;
  }

  String getSecondLine() {
    return line1;
  }
}
