package nl.naturalis.bcd.print;

import nl.naturalis.bcd.exception.BcdUserInputException;

public class LayoutException extends BcdUserInputException {

  public LayoutException(String message) {
    super("Ongeldige layout. " + message);
  }
}
