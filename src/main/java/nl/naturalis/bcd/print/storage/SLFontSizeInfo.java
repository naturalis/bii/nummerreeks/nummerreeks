package nl.naturalis.bcd.print.storage;

class SLFontSizeInfo {
  private double fontSizeLine0;
  private double fontSizeLine1;

  SLFontSizeInfo(double fontSize0, double fontSize1) {
    this.fontSizeLine0 = fontSize0;
    this.fontSizeLine1 = fontSize1;
  }

  double getFontSizeLine0() {
    return fontSizeLine0;
  }

  double getFontSizeLine1() {
    return fontSizeLine1;
  }

  public String toString() {
    return "{fontSizeLine0: " + fontSizeLine0 + ", fontSizeLine1: " + fontSizeLine1 + "}";
  }
}
