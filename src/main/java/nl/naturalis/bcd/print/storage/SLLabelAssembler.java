package nl.naturalis.bcd.print.storage;

import java.math.BigDecimal;
import java.util.concurrent.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.bcd.model.StorageLabel;
import nl.naturalis.bcd.print.DataMatrixGenerator;
import nl.naturalis.bcd.print.LabelAssembler;
import static java.math.RoundingMode.HALF_EVEN;
import static nl.naturalis.bcd.print.Label.COLUMN;
import static nl.naturalis.bcd.print.Label.ROW;
import static nl.naturalis.bcd.print.LabelUtil.POINTS_PER_MM;
import static nl.naturalis.bcd.print.storage.SLLabel.*;
import static nl.naturalis.common.MathMethods.getPageRowColumn;
import static nl.naturalis.common.MathMethods.getPageRowColumnCM;

/**
 * A {@link Callable} implementation that gathers all information required to print a single label.
 *
 * @author Ayco Holleman
 */
class SLLabelAssembler extends LabelAssembler<SLLabel> {

  private static final Logger LOG = LoggerFactory.getLogger(SLLabelAssembler.class);

  private final StorageLocationPrintSession session;
  private final int cellIndex;
  private final StorageLabel storageLabel;

  SLLabelAssembler(StorageLocationPrintSession session, int cellIndex, StorageLabel storageLabel) {
    this.session = session;
    this.cellIndex = cellIndex;
    this.storageLabel = storageLabel;
  }

  @Override
  public SLLabel call() throws InterruptedException {
    StorageLocationPrintSession session = this.session;
    SLLayoutData layoutData = session.getLayoutData();
    try {

      int[] cellInfo =
          layoutData.isColumnWise()
              ? getPageRowColumnCM(cellIndex, layoutData.getRowCount(), layoutData.getColumnCount())
              : getPageRowColumn(cellIndex, layoutData.getRowCount(), layoutData.getColumnCount());

      // DATA MATRIX
      byte[] bytes = DataMatrixGenerator.getBytes(storageLabel.toString());

      float[] layoutInfo = new float[LAYOUT_INFO_ITEM_COUNT];

      layoutInfo[IMAGE_X] = toPoints(layoutData.getImageX(cellInfo[COLUMN]));
      layoutInfo[IMAGE_Y] = toPoints(layoutData.getImageY(cellInfo[ROW]));
      layoutInfo[IMAGE_SIZE] = toPoints(layoutData.getImgSize());

      // LABEL TEXT
      SLFontSizeComputer fontSizeComputer = new SLFontSizeComputer(session);
      SLFontSizeInfo fontSizeInfo = fontSizeComputer.getFontSizeInfo(storageLabel);
      if (LOG.isTraceEnabled()) {
        // LOG.trace("Computed font sizes for {}: {}", storageLabel, fontSizeInfo);
      }
      layoutInfo[FONT_SIZE_LINE0] = toPoints(fontSizeInfo.getFontSizeLine0());
      layoutInfo[FONT_SIZE_LINE1] = toPoints(fontSizeInfo.getFontSizeLine1());
      layoutInfo[TEXT_X] = toPoints(layoutData.getTextX(cellInfo[COLUMN]));
      layoutInfo[TEXT_LINE0_Y] = toPoints(layoutData.getTextLine0Y(cellInfo[ROW], fontSizeInfo));
      layoutInfo[TEXT_LINE1_Y] = toPoints(layoutData.getTextLine1Y(cellInfo[ROW]));
      return SLLabel.create(storageLabel, bytes, cellInfo, layoutInfo);
    } catch (Throwable t) {
      session.abort(t);
      throw new InterruptedException();
    }
  }

  private static float toPoints(double mm) {
    return new BigDecimal(mm * POINTS_PER_MM).setScale(8, HALF_EVEN).floatValue();
  }
}
