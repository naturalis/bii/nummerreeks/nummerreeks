package nl.naturalis.bcd.print.storage;

import java.io.IOException;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import nl.naturalis.bcd.print.LabelWriter;
import static org.apache.pdfbox.pdmodel.font.PDType1Font.HELVETICA;
import static org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject.createFromByteArray;
import static nl.naturalis.bcd.print.storage.SLLabel.*;

class SLLabelWriter implements LabelWriter<SLLabel> {

  private final StorageLocationPrintSession session;
  private final PDPageContentStream pageContents;

  SLLabelWriter(StorageLocationPrintSession session, PDPageContentStream pageContents) {
    this.session = session;
    this.pageContents = pageContents;
  }

  @Override
  public void writeLabel(SLLabel label) {
    try {
      writeDataMatrix(label);
      writeText(label);
    } catch (IOException e) {
      session.abort(e);
    }
  }

  private void writeDataMatrix(SLLabel label) throws IOException {
    float x = label.getLayoutInfo(IMAGE_X);
    float y = label.getLayoutInfo(IMAGE_Y);
    float sz = label.getLayoutInfo(IMAGE_SIZE);

    PDImageXObject img = createFromByteArray(session.getTargetDocument(), label.getBytes(), "foo");
    pageContents.drawImage(img, x, y, sz, sz);
  }

  private void writeText(SLLabel label) throws IOException {
    writeLines(label);
  }

  private void writeLines(SLLabel label) throws IOException {
    float x = label.getLayoutInfo(TEXT_X);
    float yLine0 = label.getLayoutInfo(TEXT_LINE0_Y);
    float yLine1 = label.getLayoutInfo(TEXT_LINE1_Y);
    float fontSizeLine0 = label.getLayoutInfo(FONT_SIZE_LINE0);
    float fontSizeLine1 = label.getLayoutInfo(FONT_SIZE_LINE1);
    PDPageContentStream pageContents = this.pageContents;
    pageContents.beginText();
    pageContents.setFont(HELVETICA, fontSizeLine0);
    pageContents.newLineAtOffset(x, yLine0);
    pageContents.showText(label.getFirstLine());
    pageContents.endText();
    pageContents.beginText();
    pageContents.setFont(HELVETICA, fontSizeLine1);
    pageContents.newLineAtOffset(x, yLine1);
    pageContents.showText(label.getSecondLine());
    pageContents.endText();
  }
}
