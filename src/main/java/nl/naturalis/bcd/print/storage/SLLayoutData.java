package nl.naturalis.bcd.print.storage;

import nl.naturalis.bcd.model.LayoutTemplate;
import nl.naturalis.bcd.print.LayoutData;
import static nl.naturalis.bcd.print.LabelUtil.getHeightOccupiedBy;

/**
 * Contains the layout data needed to print labels for collection management. Collection management
 * uses two templates: one for specimens ({@link LayoutTemplate#STANDARD}) and one for storage
 * locations ({@link LayoutTemplate#STORAGE_LOCATION}). Although they produce visually different
 * results, they can be generated using the same input.
 *
 * @author Ayco Holleman
 */
public class SLLayoutData extends LayoutData {

  private double labelWidth;
  private double labelHeight;
  private double textWidth;

  //////////////////////////
  // COMPUTED VALUES
  //////////////////////////

  // Vertical padding within label
  double getPaddingTopBottom() {
    return (labelHeight - imgSize) / 2;
  }

  // Horizontal padding within label
  double getPaddingLeftRight() {
    return (labelWidth - imgSize - getImgMarginRight() - textWidth) / 2;
  }

  double getImageX(int column) {
    return getPrintAreaLeft() + (column * columnWidth) + getPaddingLeftRight();
  }

  /**
   * The absolute y-coordinate of the data matrix code <b>and</b> the label text block. Note that
   * contrary to HTML, which positions elements relative to the top-left corner (where top == 0),
   * PDF positions elements relative to the bottom-left of the page (where y == 0). So keep in mind
   * that this method returns the y-coordinate of the lower-left corner of the data matrix code, not
   * the upper-left corner.
   */
  double getImageY(int row) {
    return getPageHeightMillimeters()
        - printAreaTop
        - (row * rowHeight)
        - getPaddingTopBottom()
        - imgSize;
  }

  double getTextX(int column) {
    return getImageX(column) + imgSize + getImgMarginRight();
  }

  double getTextLine0Y(int row, SLFontSizeInfo fontSizeInfo) {
    // Align top of data matrix with top of text
    return getImageY(row) + imgSize - getHeightOccupiedBy(fontSizeInfo.getFontSizeLine0());
  }

  double getTextLine1Y(int row) {
    return getImageY(row);
  }

  //////////////////////////
  // GETTERS & SETTERS
  //////////////////////////

  double getLabelWidth() {
    return labelWidth;
  }

  public void setLabelWidth(double labelWidth) {
    this.labelWidth = labelWidth;
  }

  double getLabelHeight() {
    return labelHeight;
  }

  public void setLabelHeight(double labelHeight) {
    this.labelHeight = labelHeight;
  }

  double getTextWidth() {
    return textWidth;
  }

  public void setTextWidth(double textWidth) {
    this.textWidth = textWidth;
  }
}
