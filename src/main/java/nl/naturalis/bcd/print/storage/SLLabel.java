package nl.naturalis.bcd.print.storage;

import nl.naturalis.bcd.model.StorageLabel;
import nl.naturalis.bcd.print.Label;

class SLLabel implements Label {

  static final int IMAGE_X = 0;
  static final int IMAGE_Y = 1;
  static final int IMAGE_SIZE = 2;
  static final int FONT_SIZE_LINE0 = 3;
  static final int FONT_SIZE_LINE1 = 4;
  static final int TEXT_X = 5;
  static final int TEXT_LINE0_Y = 6;
  static final int TEXT_LINE1_Y = 7;

  static final int LAYOUT_INFO_ITEM_COUNT = 8;

  public static SLLabel create(
      StorageLabel storageLabel, byte[] bytes, int[] cellInfo, float[] layoutInfo) {
    return new SLLabel(storageLabel, bytes, cellInfo, layoutInfo);
  }

  private StorageLabel storageLabel;

  /*
   * The image/jpeg bytes for the data matrix
   */
  private byte[] bytes;

  /*
   * Page, row and column of the cell
   */
  private int[] cellInfo;

  /*
   * Font sizes and positions of the components within the label
   */
  private float[] layoutInfo;

  private SLLabel(
      StorageLabel storageLabel, byte[] bytes, int[] cellInfo, float[] layoutInfo) {
    this.storageLabel = storageLabel;
    this.bytes = bytes;
    this.cellInfo = cellInfo;
    this.layoutInfo = layoutInfo;
  }

  public int getCellInfo(int index) { // PAGE, ROW or COLUMN
    return cellInfo[index];
  }

  byte[] getBytes() {
    return bytes;
  }

  float getLayoutInfo(int index) { // IMAGE_X, IMAGE_Y, etc.
    return layoutInfo[index];
  }

  String getFirstLine() {
    return storageLabel.getFineGrainedLocation();
  }

  String getSecondLine() {
    return storageLabel.toString();
  }
}
