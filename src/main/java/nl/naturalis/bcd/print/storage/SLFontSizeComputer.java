package nl.naturalis.bcd.print.storage;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.bcd.model.StorageLabel;
import static nl.naturalis.bcd.print.LabelUtil.getFontSizeForHeight;
import static nl.naturalis.bcd.print.LabelUtil.getFontSizeForWidth;
import static nl.naturalis.bcd.print.LabelUtil.getHeightOccupiedBy;
import static nl.naturalis.bcd.print.LabelUtil.getRelativeStringWidth;
import static nl.naturalis.bcd.print.shared.FontSizeComputer.SHRINK;

class SLFontSizeComputer {

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(SLFontSizeComputer.class);

  private final StorageLocationPrintSession session;

  SLFontSizeComputer(StorageLocationPrintSession session) {
    this.session = session;
  }

  /*
   * NB One notable difference with the other labels: the number comes on the *first* line and the
   * rest on the second. Thus numberEmphasis applies to the fontSize of the 1 st line.
   */
  SLFontSizeInfo getFontSizeInfo(StorageLabel sl) throws IOException {
    double w0 = getRelativeStringWidth(sl.getFineGrainedLocation());
    double w1 = getRelativeStringWidth(sl.toString());
    SLLayoutData layoutData = session.getLayoutData();
    double maxWidth = layoutData.getTextWidth();
    double imgSize = layoutData.getImgSize();
    double numberEmphasis = layoutData.getNumberEmphasis();
    double maxFontSize = ((numberEmphasis / 100D) * getFontSizeForHeight(imgSize));
    double fontSize0 = getFontSizeForWidth(w0, maxWidth, maxFontSize);
    double leftOver = imgSize - getHeightOccupiedBy(fontSize0);
    maxFontSize = getFontSizeForHeight(leftOver);
    double fontSize1 = getFontSizeForWidth(w1, maxWidth, maxFontSize);
    fontSize0 *= Math.min(SHRINK, (fontSize0 + fontSize1) / imgSize);
    fontSize1 *= Math.min(SHRINK, (fontSize0 + fontSize1) / imgSize);
    return new SLFontSizeInfo(fontSize0, fontSize1);
  }
}
