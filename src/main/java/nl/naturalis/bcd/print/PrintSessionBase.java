package nl.naturalis.bcd.print;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.bcd.model.Layout;
import nl.naturalis.common.ExceptionMethods;
import static java.util.concurrent.TimeUnit.SECONDS;

public abstract class PrintSessionBase<LAYOUT_DATA extends LayoutData, LABEL extends Label>
    implements PrintSession {

  private static final Logger LOG = LoggerFactory.getLogger(PrintSessionBase.class);

  // Use guava's dummy ExecutorService to make sure it's not
  // because of multi-threading mysteries that your code sucks:
  // MoreExecutors.newDirectExecutorService();
  protected ExecutorService threadPool;

  protected final PDDocument targetDocument;

  protected final LAYOUT_DATA layoutData;

  public PrintSessionBase(Layout layout) {
    this.targetDocument = new PDDocument();
    this.layoutData = getLayoutDataFactory(layout).getLayoutData();
  }

  protected abstract LayoutDataFactory<LAYOUT_DATA> getLayoutDataFactory(Layout layout);

  protected abstract List<? extends LabelAssembler<LABEL>> getLabelAssemblers();

  protected abstract LabelWriter<LABEL> createLabelWriter(PDPageContentStream pageContents);

  public ExecutorService getThreadPool() {
    if (threadPool == null) {
      int minPoolSize = 2;
      int maxPoolSize = Integer.MAX_VALUE;
      BlockingQueue<Runnable> jobQueue = new LinkedBlockingQueue<>();
      threadPool = new ThreadPoolExecutor(minPoolSize, maxPoolSize, 3, SECONDS, jobQueue);
    }
    return threadPool;
  }

  public void printLabels(OutputStream out) throws IOException {
    StopWatch stopwatch = StopWatch.createStarted();
    try {
      LabelAssemblyLine<LABEL, ? extends LabelAssembler<LABEL>> runner =
          new LabelAssemblyLine<>(this, getLabelAssemblers());
      Map<Integer, List<LABEL>> cells = runner.collectLabels();
      for (int page = 0; page < cells.size(); ++page) {
        try (PageWriter<LABEL> pageWriter = new PageWriter<>(this)) {
          pageWriter.writeLabels(cells.get(page));
        }
      }
      targetDocument.save(out);
    } finally {
      LOG.info("Printing labels took {}", stopwatch.formatTime());
    }
  }

  public <T> T abort(Throwable t) {
    LOG.error(ExceptionMethods.getDetailedMessage(t));
    if (threadPool != null && !threadPool.isTerminated()) {
      try {
        threadPool.shutdownNow();
      } catch (Throwable t2) {
        LOG.error(t2.toString());
      }
    }
    if (targetDocument != null) {
      try {
        targetDocument.close();
      } catch (Throwable t3) {
        LOG.error(t3.toString());
      }
    }
    throw ExceptionMethods.wrap(t, PrintException::new);
  }

  public LAYOUT_DATA getLayoutData() {
    return layoutData;
  }

  public PDDocument getTargetDocument() {
    return targetDocument;
  }

  @Override
  public void close() throws IOException {
    if (targetDocument != null) {
      targetDocument.close();
    }
  }
}
