package nl.naturalis.bcd.print;

import java.io.OutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.bcd.model.Layout;
import nl.naturalis.bcd.model.StorageLocation;
import nl.naturalis.common.ExceptionMethods;

public class StorageLocationPrintService {

  private static final Logger LOG = LoggerFactory.getLogger(StorageLocationPrintService.class);

  private final StorageLocation storageLocation;
  private final Layout layout;

  public StorageLocationPrintService(StorageLocation storageLocation, Layout layout) {
    this.storageLocation = storageLocation;
    this.layout = layout;
  }

  public void printLabels(OutputStream out) {
    try {
      print(out);
    } catch (Exception e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  private void print(OutputStream out) throws Exception {

    LOG.info("Printing labels for {} using layout {}", storageLocation, layout.getLayoutName());

    try (PrintSession session = PrintSession.newPrintSession(storageLocation, layout)) {
      session.printLabels(out);
    }
  }
}
