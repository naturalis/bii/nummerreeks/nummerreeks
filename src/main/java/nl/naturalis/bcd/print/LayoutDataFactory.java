package nl.naturalis.bcd.print;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nl.naturalis.bcd.print.stdplus.StdPlusLayoutDataFactory;
import nl.naturalis.common.invoke.InvalidAssignmentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import nl.naturalis.bcd.json.BcdModule;
import nl.naturalis.bcd.model.Layout;
import nl.naturalis.bcd.print.lab.LabLayoutDataFactory;
import nl.naturalis.bcd.print.std.StdLayoutDataFactory;
import nl.naturalis.bcd.print.storage.SLLayoutDataFactory;
import nl.naturalis.common.ExceptionMethods;
import nl.naturalis.common.Pair;
import nl.naturalis.common.PrintMethods;
import nl.naturalis.common.check.Check;
import nl.naturalis.common.invoke.BeanWriter;

import static nl.naturalis.common.ClassMethods.simpleClassName;
import static nl.naturalis.common.ObjectMethods.isEmpty;
import static nl.naturalis.common.StringMethods.implode;
import static nl.naturalis.common.StringMethods.rtrim;
import static nl.naturalis.common.check.CommonChecks.empty;
import static nl.naturalis.common.check.CommonChecks.inRangeClosed;

public abstract class LayoutDataFactory<T extends LayoutData> {

  private static final Logger LOG = LoggerFactory.getLogger(LayoutDataFactory.class);

  private static final String ERR_NON_SETTINGS =
      "Een of meer instellingen bestaan niet of kunnen niet gebruikt worden "
          + "bij deze layout: %s. Geldige instellingen: %s";
  private static final String ERR_MISSING_SETTINGS = "Een of meer ontbrekende instellingen: %s.";
  private static final String ERR_BAD_EMPHASIS = "numberEmphasis moet tussen 1 en 99 liggen";
  private static final String ERR_EMPTY_SUFFIX = "Blanco suffix niet toegestaan";

  public static void validate(Layout layout) {
    LayoutDataFactory<?> ldf;
    switch (layout.getTemplate()) {
      case LABORATORY:
        ldf = new LabLayoutDataFactory(layout);
        break;
      case STANDARD:
        ldf = new StdLayoutDataFactory(layout);
        break;
      case STD_PLUS:
        ldf = new StdPlusLayoutDataFactory(layout);
        break;
      case STORAGE_LOCATION:
      default:
        ldf = new SLLayoutDataFactory(layout);
        break;
    }
    // Will throw LayoutException with invalid user config
    ldf.getLayoutData();
  }

  protected static Check<Double, LayoutException> checkThat(double property) {
    return Check.on(LayoutException::new, property);
  }

  protected static Check<Object, LayoutException> checkThat(Object property) {
    return Check.on(LayoutException::new, property);
  }

  private final Layout layout;
  private final Class<T> layoutDataClass;

  public LayoutDataFactory(Layout layout, Class<T> layoutDataClass) {
    this.layout = layout;
    this.layoutDataClass = layoutDataClass;
  }

  public T getLayoutData() {
    LOG.debug("Retrieving user configuration for layout \"{}\"", layout.getLayoutName());
    Map<String, Object> config;
    try {
      config = BcdModule.readJson(layout.getJsonConfig());
    } catch (JsonProcessingException e) {
      // Shouldn't happen b/c already validated in front-end
      e.printStackTrace();
      throw ExceptionMethods.uncheck(e);
    }
    if (LOG.isTraceEnabled()) {
      LOG.trace("\n" + PrintMethods.print(config));
    }
    checkMissingSettings(config);
    checkNonSettings(config);
    // Implicit check of "paperSize" setting:
    PaperSize.parse(config.get("paperSize"));
    // Massage value of "suffixes" setting
    Object suffixes = config.get("suffixes");
    if (isEmpty(suffixes)) {
      config.put("suffixes", null);
    } else if (suffixes.getClass() == String.class) {
      // Allow user to skip array notation for single suffix
      config.put("suffixes", new String[] {suffixes.toString()});
    } else if (!(suffixes instanceof List)) {
      String err =
          "Ongeldige waarde voor suffixes: "
              + suffixes
              + ". Voorbeelden van geldige waarden zijn (bijvoorbeeld): "
              + "\".lat\" of [\".dors\", \".lat\"] of [\".lat\"] of []";
      throw new LayoutException(err);
    } else {
      @SuppressWarnings("rawtypes")
      List l = (List) suffixes;
      String[] suffixArray = new String[l.size()];
      for (int i = 0; i < l.size(); ++i) {
        Check.notNull(l.get(i), "suffix");
        suffixArray[i] = l.get(i).toString();
      }
    }
    try {
      T layoutData = layoutDataClass.getDeclaredConstructor().newInstance();
      BeanWriter<T> beanWriter = BeanWriter.getTolerantWriter(layoutDataClass);
      beanWriter.transfer(config, layoutData);
      if (LOG.isTraceEnabled()) {
        LOG.trace("\n" + PrintMethods.print(layoutData));
      }
      LOG.debug("Validating configuration");
      validate(layoutData);
      return layoutData;
    } catch (InvalidAssignmentException e) {
      String fmt = "Ongeldige waarde voor %s: %s. Verwacht een waarde van het type %s";
      String msg =
          String.format(
              fmt, e.getPropertyName(), e.getValue(), simpleClassName(e.getPropertyType()));
      throw new LayoutException(msg);
    } catch (Throwable e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  protected abstract Set<String> getUserConfigurableProperties();

  protected void validate(T layoutData) throws LayoutException {
    checkThat(layoutData.getNumberEmphasis())
        .is(inRangeClosed(), Pair.of(1D, 99D), ERR_BAD_EMPHASIS);
    if (layoutData.getSuffixes() != null) {
      for (int i = 0; i < layoutData.getSuffixes().length; ++i) {
        String s = rtrim(layoutData.getSuffixes()[i], ' ');
        Check.on(LayoutException::new, s).isNot(empty(), ERR_EMPTY_SUFFIX);
        layoutData.getSuffixes()[i] = s;
      }
    }
  }

  private void checkMissingSettings(Map<String, Object> cfg) {
    Set<String> required;
    required = new HashSet<>(getUserConfigurableProperties());
    required.removeAll(cfg.keySet());
    // "suffixes" & "suffixesOnly" are optionally present for some templates & forbidden
    // for others. Can't validate their absence or presence in the base class.
    required.remove("suffixes");
    required.remove("suffixesOnly");
    checkThat(required).is(empty(), ERR_MISSING_SETTINGS, implode(required));
  }

  private void checkNonSettings(Map<String, Object> cfg) {
    Set<String> provided = new HashSet<>(cfg.keySet());
    provided.removeAll(getUserConfigurableProperties());
    provided.remove("suffixes");
    provided.remove("suffixesOnly");
    checkThat(provided)
        .is(empty(), ERR_NON_SETTINGS, implode(provided), implode(getUserConfigurableProperties()));
  }
}
