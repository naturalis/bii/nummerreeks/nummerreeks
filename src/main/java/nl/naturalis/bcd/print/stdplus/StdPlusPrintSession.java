package nl.naturalis.bcd.print.stdplus;

import nl.naturalis.bcd.model.Layout;
import nl.naturalis.bcd.model.NumberSeries;
import nl.naturalis.bcd.print.LabelWriter;
import nl.naturalis.bcd.print.LayoutDataFactory;
import nl.naturalis.bcd.print.NumberSeriesPrintSession;
import nl.naturalis.bcd.print.shared.FontSizeComputer;
import nl.naturalis.bcd.print.shared.FontSizeInfoProvider;
import nl.naturalis.common.ArrayMethods;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

import static nl.naturalis.common.ObjectMethods.ifNull;

public class StdPlusPrintSession extends NumberSeriesPrintSession<StdPlusLayoutData, StdPlusLabel> {

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(StdPlusPrintSession.class);

  private final FontSizeInfoProvider fontSizeInfoProvider;

  public StdPlusPrintSession(NumberSeries series, Layout layout) {
    super(series, layout);
    FontSizeComputer fsc = new FontSizeComputer(this, getLayoutData().getTextWidth());
    this.fontSizeInfoProvider = fsc.getFontSizeInfoProvider();
  }

  @Override
  protected List<StdPlusLabelAssembler> getLabelAssemblers() {
    NumberSeries ns = getNumberSeries();
    boolean suffixesOnly = getLayoutData().isSuffixesOnly();
    int init = suffixesOnly ? 0 : 1;
    String[] suffixes = ifNull(getLayoutData().getSuffixes(), ArrayMethods.EMPTY_STRING_ARRAY);
    int sz = (init + suffixes.length) * ns.getAmount();
    StdPlusLabelAssembler[] assemblers = new StdPlusLabelAssembler[sz];
    int i = 0;
    for (int nr = ns.getFirstNumber(); nr <= ns.getLastNumber(); ++nr) {
      if (!suffixesOnly) {
        assemblers[i] = new StdPlusLabelAssembler(this, i, nr, null);
        ++i;
      }
      for (String s : suffixes) {
        assemblers[i] = new StdPlusLabelAssembler(this, i, nr, s);
        ++i;
      }
    }
    return Arrays.asList(assemblers);
  }

  @Override
  protected LayoutDataFactory<StdPlusLayoutData> getLayoutDataFactory(Layout layout) {
    return new StdPlusLayoutDataFactory(layout);
  }

  @Override
  protected LabelWriter<StdPlusLabel> createLabelWriter(PDPageContentStream pageContents) {
    return new StdPlusLabelWriter(this, pageContents);
  }

  FontSizeInfoProvider getFontSizeInfoProvider() {
    return fontSizeInfoProvider;
  }
}
