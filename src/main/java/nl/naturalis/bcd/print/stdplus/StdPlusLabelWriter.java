package nl.naturalis.bcd.print.stdplus;

import nl.naturalis.bcd.print.LabelWriter;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import static nl.naturalis.bcd.print.stdplus.StdPlusLabel.*;

import java.io.IOException;

import static org.apache.pdfbox.pdmodel.font.PDType1Font.HELVETICA;
import static org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject.createFromByteArray;

class StdPlusLabelWriter implements LabelWriter<StdPlusLabel> {

  private final StdPlusPrintSession session;
  private final PDPageContentStream pageContents;

  StdPlusLabelWriter(StdPlusPrintSession session, PDPageContentStream pageContents) {
    this.session = session;
    this.pageContents = pageContents;
  }

  @Override
  public void writeLabel(StdPlusLabel label) {
    try {
      writeDataMatrix(label);
      writeText(label);
      writeFreeText(label);
    } catch (IOException e) {
      session.abort(e);
    }
  }

  private void writeDataMatrix(StdPlusLabel label) throws IOException {
    float x = label.getLayoutInfo(IMAGE_X);
    float y = label.getLayoutInfo(IMAGE_Y);
    float sz = label.getLayoutInfo(IMAGE_SIZE);
    PDImageXObject img = createFromByteArray(session.getTargetDocument(),
          label.getBytes(),
          "foo");
    pageContents.drawImage(img, x, y, sz, sz);
  }

  private void writeText(StdPlusLabel label) throws IOException {
    if (label.getSecondLine() == null) {
      writeOnSingleLine(label);
    } else {
      writeOnTwoLines(label);
    }
  }

  private void writeFreeText(StdPlusLabel label) throws IOException {
    float x = label.getLayoutInfo(FREE_TEXT_X);
    float y = label.getLayoutInfo(FREE_TEXT_Y);
    float fontSize = label.getLayoutInfo(FONT_SIZE_FREE_TEXT);
    String text = label.getFreeText();
    PDPageContentStream pageContents = this.pageContents;
    pageContents.beginText();
    pageContents.setFont(HELVETICA, fontSize);
    pageContents.newLineAtOffset(x, y);
    pageContents.showText(text);
    pageContents.endText();
  }

  private void writeOnSingleLine(StdPlusLabel label) throws IOException {
    float x = label.getLayoutInfo(TEXT_X);
    float y = label.getLayoutInfo(TEXT_LINE0_Y);
    float fontSize = label.getLayoutInfo(FONT_SIZE_LINE0);
    String text = label.getFirstLine();
    PDPageContentStream pageContents = this.pageContents;
    pageContents.beginText();
    pageContents.setFont(HELVETICA, fontSize);
    pageContents.newLineAtOffset(x, y);
    pageContents.showText(text);
    pageContents.endText();
  }

  private void writeOnTwoLines(StdPlusLabel label) throws IOException {
    float x = label.getLayoutInfo(TEXT_X);
    float yLine0 = label.getLayoutInfo(TEXT_LINE0_Y);
    float yLine1 = label.getLayoutInfo(TEXT_LINE1_Y);
    String line0 = label.getFirstLine();
    String line1 = label.getSecondLine();
    float fontSizeLine0 = label.getLayoutInfo(FONT_SIZE_LINE0);
    float fontSizeLine1 = label.getLayoutInfo(FONT_SIZE_LINE1);
    PDPageContentStream pageContents = this.pageContents;
    pageContents.beginText();
    pageContents.setFont(HELVETICA, fontSizeLine0);
    pageContents.newLineAtOffset(x, yLine0);
    pageContents.showText(line0);
    pageContents.endText();
    pageContents.beginText();
    pageContents.setFont(HELVETICA, fontSizeLine1);
    pageContents.newLineAtOffset(x, yLine1);
    pageContents.showText(line1);
    pageContents.endText();
  }
}
