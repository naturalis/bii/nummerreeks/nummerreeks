package nl.naturalis.bcd.print.stdplus;

import nl.naturalis.bcd.print.LayoutData;
import nl.naturalis.bcd.print.shared.FontSizeInfo;

import static nl.naturalis.bcd.print.LabelUtil.getHeightOccupiedBy;

public class StdPlusLayoutData extends LayoutData {

  private double labelWidth;
  private double labelHeight;
  private double textWidth;

  private String freeText;
  private double freeTextFontSize;
  // Space between data matrix code and free text line:
  private double freeTextMarginTop;

  //////////////////////////
  // COMPUTED VALUES
  //////////////////////////

  // Vertical padding within label
  double getPaddingTopBottom() {
    return (labelHeight - imgSize - freeTextFontSize - freeTextMarginTop) / 2;
  }

  // Horizontal padding within label
  double getPaddingLeftRight() {
    return (labelWidth - imgSize - getImgMarginRight() - textWidth) / 2;
  }

  double getImageX(int column) {
    return getPrintAreaLeft() + (column * columnWidth) + getPaddingLeftRight();
  }

  /**
   * The absolute y-coordinate of the data matrix code <b>and</b> the label text block.
   * Note that contrary to HTML, which positions elements relative to the top-left corner
   * (where top == 0), PDF positions elements relative to the bottom-left of the page
   * (where y == 0). So keep in mind that this method returns the y-coordinate of the
   * lower-left corner of the data matrix code, not the upper-left corner.
   */
  double getImageY(int row) {
    return getPageHeightMillimeters()
          - printAreaTop
          - (row * rowHeight)
          - getPaddingTopBottom()
          - imgSize;
  }

  double getTextX(int column) {
    return getImageX(column) + imgSize + getImgMarginRight();
  }

  double getTextLine0Y(int row, FontSizeInfo fontSizeInfo) {
    // Align top of data matrix with top of text
    return getImageY(row) + imgSize - getHeightOccupiedBy(fontSizeInfo.getFontSizeLine0());
  }

  double getTextLine1Y(int row) {
    return getImageY(row);
  }

  double getFreeTextX(int column) {
    return getImageX(column);
  }

  double getFreeTextY(int row) {
    return getPageHeightMillimeters()
          - printAreaTop
          - (row * rowHeight)
          - getPaddingTopBottom()
          - imgSize
          - freeTextMarginTop
          - freeTextFontSize;
  }


  //////////////////////////
  // GETTERS & SETTERS
  //////////////////////////

  double getLabelWidth() {
    return labelWidth;
  }

  public void setLabelWidth(double labelWidth) {
    this.labelWidth = labelWidth;
  }

  double getLabelHeight() {
    return labelHeight;
  }

  public void setLabelHeight(double labelHeight) {
    this.labelHeight = labelHeight;
  }

  double getTextWidth() {
    return textWidth;
  }

  public void setTextWidth(double textWidth) {
    this.textWidth = textWidth;
  }

  public String getFreeText() {
    return freeText;
  }

  public void setFreeText(String freeText) {
    this.freeText = freeText;
  }

  public double getFreeTextFontSize() {
    return freeTextFontSize;
  }

  public void setFreeTextFontSize(double freeTextFontSize) {
    this.freeTextFontSize = freeTextFontSize;
  }

  public double getFreeTextMarginTop() {
    return freeTextMarginTop;
  }

  public void setFreeTextMarginTop(double freeTextMarginTop) {
    this.freeTextMarginTop = freeTextMarginTop;
  }


}
