package nl.naturalis.bcd.print.stdplus;

import nl.naturalis.bcd.print.Label;

class StdPlusLabel implements Label {

  static final int IMAGE_X = 0;
  static final int IMAGE_Y = 1;
  static final int IMAGE_SIZE = 2;
  static final int FONT_SIZE_LINE0 = 3;
  static final int FONT_SIZE_LINE1 = 4;
  static final int TEXT_X = 5;
  static final int TEXT_LINE0_Y = 6;
  static final int TEXT_LINE1_Y = 7;
  static final int FREE_TEXT_X = 8;
  static final int FREE_TEXT_Y = 9;
  static final int FONT_SIZE_FREE_TEXT = 10;

  // Watch out! This constant is used to create a float array with the specified length.
  // It should be set to the number of constants defined above. So one more than the value
  // of the last of the above constants.
  static final int LAYOUT_INFO_ITEM_COUNT = 11;

  static StdPlusLabel create(
        int number,
        String suffix,
        byte[] bytes,
        int[] cellInfo,
        float[] layoutInfo,
        String line0,
        String line1,
        String freeText) {
    return new StdPlusLabel(number,
          suffix,
          bytes,
          cellInfo,
          layoutInfo,
          line0,
          line1,
          freeText);
  }

  /*
   * A single number from the number series
   */
  private int number;

  /*
   * A single suffix from "suffixes" setting in the layout configuration
   */
  private String suffix;

  /*
   * The image/jpeg bytes for the data matrix
   */
  private byte[] bytes;

  /*
   * Page, row and column of the cell
   */
  private int[] cellInfo;

  /*
   * Font sizes and positions of the components within the label
   */
  private float[] layoutInfo;

  /*
   * The entire or partial label text
   */
  private String line0;

  /*
   * The partial label text or null
   */
  private String line1;

  /*
   * The free text line along the bottom of the label
   */
  private String freeText;

  private StdPlusLabel(
        int number,
        String suffix,
        byte[] bytes,
        int[] cellInfo,
        float[] layoutInfo,
        String line0,
        String line1,
        String freeText) {
    this.number = number;
    this.suffix = suffix;
    this.bytes = bytes;
    this.cellInfo = cellInfo;
    this.layoutInfo = layoutInfo;
    this.line0 = line0;
    this.line1 = line1;
    this.freeText = freeText;
  }

  public int getCellInfo(int index) { // PAGE, ROW or COLUMN
    return cellInfo[index];
  }

  int getNumber() {
    return number;
  }

  String getSuffix() {
    return suffix;
  }

  byte[] getBytes() {
    return bytes;
  }

  float getLayoutInfo(int index) { // IMAGE_X, IMAGE_Y, etc.
    return layoutInfo[index];
  }

  String getFirstLine() {
    return line0;
  }

  String getSecondLine() {
    return line1;
  }

  String getFreeText() {
    return freeText;
  }
}
