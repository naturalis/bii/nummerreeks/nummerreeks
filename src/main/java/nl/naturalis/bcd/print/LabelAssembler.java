package nl.naturalis.bcd.print;

import java.util.concurrent.Callable;

public abstract class LabelAssembler<T extends Label> implements Callable<T> {}
