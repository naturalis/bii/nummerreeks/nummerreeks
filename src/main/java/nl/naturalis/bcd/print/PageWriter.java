package nl.naturalis.bcd.print;

import java.io.IOException;
import java.util.List;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PageWriter<LABEL extends Label> implements AutoCloseable {

  @SuppressWarnings("unused")
  private final Logger LOG = LoggerFactory.getLogger(PageWriter.class);

  private final PrintSessionBase<?, LABEL> session;
  private final PDPageContentStream pageContents;

  public PageWriter(PrintSessionBase<?, LABEL> session) {
    this.session = session;
    PDPageContentStream tmp;
    try {
      PDPage page = new PDPage(session.getLayoutData().getPaperSize().getRectangle());
      session.getTargetDocument().addPage(page);
      tmp = new PDPageContentStream(session.getTargetDocument(), page);
    } catch (IOException e) {
      tmp = null;
      session.abort(e);
    }
    this.pageContents = tmp;
  }

  public void writeLabels(List<LABEL> cells) {
    LabelWriter<LABEL> labelWriter = session.createLabelWriter(pageContents);
    cells.forEach(labelWriter::writeLabel);
  }

  @Override
  public void close() {
    try {
      pageContents.close();
    } catch (IOException e) {
      session.abort(e);
    }
  }
}
