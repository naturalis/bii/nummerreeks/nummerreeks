package nl.naturalis.bcd.print.lab;

import java.io.IOException;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import nl.naturalis.bcd.print.LabelWriter;
import static org.apache.pdfbox.pdmodel.font.PDType1Font.HELVETICA;
import static org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject.createFromByteArray;
import static nl.naturalis.bcd.print.Label.ROW;
import static nl.naturalis.bcd.print.lab.LabLabel.*;
import static nl.naturalis.common.ObjectMethods.n2e;

public class LabLabelWriter implements LabelWriter<LabLabel> {

  private final LabPrintSession session;
  private final PDPageContentStream pageContents;

  public LabLabelWriter(LabPrintSession session, PDPageContentStream pageContents) {
    this.session = session;
    this.pageContents = pageContents;
  }

  public void writeLabel(LabLabel label) {
    try {
      if (label.getCellInfo(ROW) == 0) {
        printHeader(label);
      }
      writeDataMatrix(label);
      writeVoucherText(label);
      writeSubsample(label);
    } catch (IOException e) {
      session.abort(e);
    }
  }

  private void printHeader(LabLabel label) throws IOException {
    float y = label.getLayoutInfo(HEADER_Y);
    float xVoucherHeader = label.getLayoutInfo(VOUCHER_HEADER_X);
    float xSubsampleHeader = label.getLayoutInfo(SUBSAMPLE_HEADER_X);
    float fontSize = label.getLayoutInfo(HEADER_FONT_SIZE);
    PDPageContentStream pageContents = this.pageContents;
    pageContents.beginText();
    pageContents.setFont(HELVETICA, fontSize);
    pageContents.newLineAtOffset(xVoucherHeader, y);
    pageContents.showText("voucher");
    pageContents.endText();
    pageContents.beginText();
    pageContents.setFont(HELVETICA, fontSize);
    pageContents.newLineAtOffset(xSubsampleHeader, y);
    pageContents.showText("subsample");
    pageContents.endText();
  }

  private void writeDataMatrix(LabLabel label) throws IOException {
    float x = label.getLayoutInfo(IMAGE_X);
    float y = label.getLayoutInfo(IMAGE_Y);
    float sz = label.getLayoutInfo(IMAGE_SIZE);

    PDImageXObject img = createFromByteArray(session.getTargetDocument(), label.getBytes(), "foo");
    pageContents.drawImage(img, x, y, sz, sz);
  }

  private void writeVoucherText(LabLabel label) throws IOException {
    if (label.getVoucherSecondLine() == null) {
      writeVoucherOnSingleLine(label);
    } else {
      writeVoucherOnTwoLines(label);
    }
  }

  private void writeVoucherOnSingleLine(LabLabel label) throws IOException {
    float x = label.getLayoutInfo(VOUCHER_TEXT_X);
    float y = label.getLayoutInfo(VOUCHER_TEXT_LINE0_Y);
    float fontSize = label.getLayoutInfo(VOUCHER_FONT_SIZE_LINE0);
    String text = label.getVoucherFirstLine();
    PDPageContentStream pageContents = this.pageContents;
    pageContents.beginText();
    pageContents.setFont(HELVETICA, fontSize);
    pageContents.newLineAtOffset(x, y);
    pageContents.showText(text);
    pageContents.endText();
  }

  private void writeVoucherOnTwoLines(LabLabel label) throws IOException {
    float x = label.getLayoutInfo(VOUCHER_TEXT_X);
    float yLine0 = label.getLayoutInfo(VOUCHER_TEXT_LINE0_Y);
    float yLine1 = label.getLayoutInfo(VOUCHER_TEXT_LINE1_Y);
    String line0 = label.getVoucherFirstLine();
    String line1 = label.getVoucherSecondLine();
    float fontSizeLine0 = label.getLayoutInfo(VOUCHER_FONT_SIZE_LINE0);
    float fontSizeLine1 = label.getLayoutInfo(VOUCHER_FONT_SIZE_LINE1);
    PDPageContentStream pageContents = this.pageContents;
    pageContents.beginText();
    pageContents.setFont(HELVETICA, fontSizeLine0);
    pageContents.newLineAtOffset(x, yLine0);
    pageContents.showText(line0);
    pageContents.endText();
    pageContents.beginText();
    pageContents.setFont(HELVETICA, fontSizeLine1);
    pageContents.newLineAtOffset(x, yLine1);
    pageContents.showText(line1);
    pageContents.endText();
  }

  private void writeSubsample(LabLabel label) throws IOException {
    float x = label.getLayoutInfo(SUBSAMPLE_X);
    float yLineAbove = label.getLayoutInfo(SUBSAMPLE_LINE_ABOVE_Y);
    float yText = label.getLayoutInfo(SUBSAMPLE_TEXT_Y);
    float yLineBelow = label.getLayoutInfo(SUBSAMPLE_LINE_BELOW_Y);
    float xLineEnd = label.getLayoutInfo(SUBSAMPLE_LINE_END_X);
    float fontSize = label.getLayoutInfo(SUBSAMPLE_FONT_SIZE);
    PDPageContentStream pageContents = this.pageContents;
    pageContents.moveTo(x, yLineAbove);
    pageContents.lineTo(xLineEnd, yLineAbove);
    pageContents.setLineWidth(label.getLayoutInfo(SUBSAMPLE_LINE_WIDTH));
    pageContents.stroke();
    pageContents.beginText();
    pageContents.setFont(HELVETICA, fontSize);
    pageContents.newLineAtOffset(x, yText);
    pageContents.showText(session.getPrefixWithDot() + label.getNumber() + n2e(label.getSuffix()));
    pageContents.endText();
    pageContents.moveTo(x, yLineBelow);
    pageContents.lineTo(xLineEnd, yLineBelow);
    pageContents.setLineWidth(label.getLayoutInfo(SUBSAMPLE_LINE_WIDTH));
    pageContents.stroke();
  }
}
