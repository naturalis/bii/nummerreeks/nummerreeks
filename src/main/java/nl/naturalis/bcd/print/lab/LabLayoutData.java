package nl.naturalis.bcd.print.lab;

import nl.naturalis.bcd.print.LayoutData;
import nl.naturalis.bcd.print.shared.FontSizeInfo;
import static nl.naturalis.bcd.print.LabelUtil.*;

/**
 * Contains the layout data needed to print labels for the laboratory.
 *
 * @author Ayco Holleman
 */
public class LabLayoutData extends LayoutData {

  private double headerRowHeight;
  private double headerFontSize;

  private double labelHeight;

  private double voucherColumnWidth;
  private double voucherLabelWidth;
  private double voucherTextWidth;

  private double subsampleLabelWidth;
  private double subsampleTextWidth;

  //////////////////////////
  // COMPUTED VALUES
  //////////////////////////

  double getHeaderY() {
    return getPageHeightMillimeters() - printAreaTop - headerFontSize;
  }

  double getVoucherPaddingTopBottom() {
    return (labelHeight - imgSize) / 2;
  }

  // Horizontal padding within label
  double getVoucherPaddingLeftRight() {
    return (voucherLabelWidth - imgSize - getImgMarginRight() - voucherTextWidth) / 2;
  }

  double getImageX(int column) {
    return printAreaLeft + (column * columnWidth) + getVoucherPaddingLeftRight();
  }

  double getImageY(int row) {
    return getPageHeightMillimeters()
        - printAreaTop
        - headerRowHeight
        - (row * rowHeight)
        - getVoucherPaddingTopBottom()
        - imgSize;
  }

  double getVoucherTextX(int column) {
    return getImageX(column) + imgSize + getImgMarginRight();
  }

  double getVoucherTextLine0Y(int row, FontSizeInfo fontSizeInfo) {
    // Align top of data matrix with top of text
    return getVoucherTextLine1Y(row) + imgSize - getHeightOccupiedBy(fontSizeInfo.getFontSizeLine0());
  }

  double getVoucherTextLine1Y(int row) {
    return getImageY(row);
  }

  public double getSubsampleLineWidth() {
    // Let's not bother the user with yet another user-configurable setting
    return 0.080 * imgSize;
  }

  // The spcae between the label text and the lines above & below it
  public double getSubsampleTextMarginTB() {
    // Let's not bother the user with yet another user-configurable setting
    return 0.092 * imgSize;
  }

  // Horizontal padding within subsample label
  double getSubsamplePaddingLeftRight() {
    return (subsampleLabelWidth - subsampleTextWidth) / 2;
  }

  // The x coordinate for the subsample text *and* the lines above/beneath it
  double getSubsampleX(int column) {
    return printAreaLeft
        + (column * columnWidth)
        + voucherColumnWidth
        + getSubsamplePaddingLeftRight();
  }

  // The y coordinate for the line to draw above the text
  double getSubsampleLineAboveY(int row) {
    // Align the line with the upper side of the data matrix
    return getImageY(row) + imgSize;
  }

  double getSubsampleTextY(int row, double fontSize) {
    return getSubsampleLineAboveY(row) - getSubsampleTextMarginTB() - getHeightOccupiedBy(fontSize);
  }

  double getSubsampleLineBelowY(int row, double fontSize) {
    return getSubsampleTextY(row, fontSize) - getSubsampleTextMarginTB();
  }

  //////////////////////////
  // GETTERS & SETTERS
  //////////////////////////

  public double getHeaderRowHeight() {
    return headerRowHeight;
  }

  public void setHeaderRowHeight(double headerRowHeight) {
    this.headerRowHeight = headerRowHeight;
  }

  public double getHeaderFontSize() {
    return headerFontSize;
  }

  public void setHeaderFontSize(double headerFontSize) {
    this.headerFontSize = headerFontSize;
  }

  public double getLabelHeight() {
    return labelHeight;
  }

  public void setLabelHeight(double labelHeight) {
    this.labelHeight = labelHeight;
  }

  public double getVoucherColumnWidth() {
    return voucherColumnWidth;
  }

  public void setVoucherColumnWidth(double voucherColumnWidth) {
    this.voucherColumnWidth = voucherColumnWidth;
  }

  public double getVoucherLabelWidth() {
    return voucherLabelWidth;
  }

  public void setVoucherLabelWidth(double voucherLabelWidth) {
    this.voucherLabelWidth = voucherLabelWidth;
  }

  public double getVoucherTextWidth() {
    return voucherTextWidth;
  }

  public void setVoucherTextWidth(double voucherTextWidth) {
    this.voucherTextWidth = voucherTextWidth;
  }

  public double getSubsampleLabelWidth() {
    return subsampleLabelWidth;
  }

  public void setSubsampleLabelWidth(double subsampleLabelWidth) {
    this.subsampleLabelWidth = subsampleLabelWidth;
  }

  public double getSubsampleTextWidth() {
    return subsampleTextWidth;
  }

  public void setSubsampleTextWidth(double subsampleTextWidth) {
    this.subsampleTextWidth = subsampleTextWidth;
  }
}
