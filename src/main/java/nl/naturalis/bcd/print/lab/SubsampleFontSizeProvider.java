package nl.naturalis.bcd.print.lab;

import java.util.Map;
import static nl.naturalis.common.ObjectMethods.n2e;

public class SubsampleFontSizeProvider {

  private final Map<String, double[]> fontSizesPerSuffix;
  private final int offset;

  SubsampleFontSizeProvider(Map<String, double[]> fontSizesPerSuffix, int offset) {
    this.fontSizesPerSuffix = Map.copyOf(fontSizesPerSuffix);
    this.offset = offset;
  }

  public double getFontSize(int number, String suffix) {
    int x = (int) Math.log10(number);
    return fontSizesPerSuffix.get(n2e(suffix))[x - offset];
  }
}
