package nl.naturalis.bcd.print.lab;

import nl.naturalis.bcd.print.Label;

public class LabLabel implements Label {

  static final int VOUCHER_HEADER_X = 0;
  static final int SUBSAMPLE_HEADER_X = 1;
  static final int HEADER_Y = 2;
  static final int HEADER_FONT_SIZE = 3;
  static final int IMAGE_X = 4;
  static final int IMAGE_Y = 5;
  static final int IMAGE_SIZE = 6;
  static final int VOUCHER_FONT_SIZE_LINE0 = 7;
  static final int VOUCHER_FONT_SIZE_LINE1 = 8;
  static final int VOUCHER_TEXT_X = 9;
  static final int VOUCHER_TEXT_LINE0_Y = 10;
  static final int VOUCHER_TEXT_LINE1_Y = 11;
  static final int SUBSAMPLE_FONT_SIZE = 12;
  static final int SUBSAMPLE_X = 13;
  static final int SUBSAMPLE_LINE_ABOVE_Y = 14;
  static final int SUBSAMPLE_TEXT_Y = 15;
  static final int SUBSAMPLE_LINE_BELOW_Y = 16;
  static final int SUBSAMPLE_LINE_END_X = 17;
  static final int SUBSAMPLE_WIDTH = 18;
  static final int SUBSAMPLE_LINE_WIDTH = 19;

  static final int LAYOUT_INFO_ITEM_COUNT = 20;

  static LabLabel create(
      int number,
      String suffix,
      byte[] bytes,
      int[] cellInfo,
      float[] layoutInfo,
      String line0,
      String line1) {
    return new LabLabel(number, suffix, bytes, cellInfo, layoutInfo, line0, line1);
  }

  /*
   * A single number from the number series
   */
  private int number;

  /*
   * A single suffix from "suffixes" setting in the layout configuration
   */
  private String suffix;
  /*
   * The image/jpeg bytes for the data matrix
   */
  private byte[] bytes;

  /*
   * Page, row and column of the cell
   */
  private int[] cellInfo;

  /*
   * Font sizes and positions of the components within the label
   */
  private float[] layoutInfo;

  /*
   * The entire or partial label text
   */
  private String voucherLine0;

  /*
   * The partial label text or null
   */
  private String voucherLine1;

  private LabLabel(
      int number,
      String suffix,
      byte[] bytes,
      int[] cellInfo,
      float[] layoutInfo,
      String voucherLine0,
      String voucherLne1) {
    this.number = number;
    this.suffix = suffix;
    this.bytes = bytes;
    this.cellInfo = cellInfo;
    this.layoutInfo = layoutInfo;
    this.voucherLine0 = voucherLine0;
    this.voucherLine1 = voucherLne1;
  }

  public int getCellInfo(int index) { // PAGE, ROW or COLUMN
    return cellInfo[index];
  }

  int getNumber() {
    return number;
  }

  String getSuffix() {
    return suffix;
  }

  byte[] getBytes() {
    return bytes;
  }

  float getLayoutInfo(int index) { // IMAGE_X, IMAGE_Y, etc.
    return layoutInfo[index];
  }

  String getVoucherFirstLine() {
    return voucherLine0;
  }

  String getVoucherSecondLine() {
    return voucherLine1;
  }
}
