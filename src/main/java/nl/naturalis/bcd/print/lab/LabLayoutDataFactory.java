package nl.naturalis.bcd.print.lab;

import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.bcd.model.Layout;
import nl.naturalis.bcd.print.LayoutDataFactory;
import nl.naturalis.bcd.print.LayoutException;
import static nl.naturalis.common.check.CommonChecks.atLeast;
import static nl.naturalis.common.check.CommonChecks.atMost;

/**
 * A factory for {@link LabLayoutData} instances.
 *
 * @author Ayco Holleman
 */
public class LabLayoutDataFactory extends LayoutDataFactory<LabLayoutData> {

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(LabLayoutDataFactory.class);

  public static final Set<String> USER_INPUT =
      Set.of(
          "paperSize",
          "printAreaTop",
          "printAreaLeft",
          "headerRowHeight",
          "headerFontSize",
          "rowCount",
          "columnCount",
          "columnWise",
          "rowHeight",
          "columnWidth",
          "labelHeight",
          "imgSize",
          "voucherColumnWidth",
          "voucherLabelWidth",
          "voucherTextWidth",
          "subsampleLabelWidth",
          "subsampleTextWidth",
          "glueAll",
          "gluePrefix",
          "numberEmphasis",
          "suffixes",
          "suffixesOnly");

  public LabLayoutDataFactory(Layout layout) {
    super(layout, LabLayoutData.class);
  }

  @Override
  protected Set<String> getUserConfigurableProperties() {
    return USER_INPUT;
  }

  protected void validate(LabLayoutData data) throws LayoutException {

    super.validate(data);

    // GLOBAL LAYOUT CHECKS

    /*
     * The last column does not need to fit entirely on the page. Only the part occupied by the
     * ^^^subsample^^^ label does.
     */
    double neededWidth =
        ((data.getColumnCount() - 1) * data.getColumnWidth())
            + data.getVoucherColumnWidth()
            + data.getSubsampleLabelWidth();
    double availableWidth = data.getPageWidthMillimeters() - data.getPrintAreaLeft();
    checkThat(neededWidth)
        .is(
            atMost(),
            availableWidth,
            "Minimaal benodigde horizontale ruimte: %smm. Beschikbaar: %smm.",
            neededWidth,
            availableWidth);

    double neededHeight = ((data.getRowCount() - 1) * data.getRowHeight()) + data.getLabelHeight();
    double availableHeight = data.getPageHeightMillimeters() - data.getPrintAreaTop();
    checkThat(neededHeight)
        .is(
            atMost(),
            availableHeight,
            "Minimaal benodigde verticale ruimte: %smm. Beschikbaar: %smm.",
            neededHeight,
            availableHeight);

    // HEADER CHECKS

    checkThat(data.getHeaderFontSize())
        .is(
            atMost(),
            data.getHeaderRowHeight(),
            "headerFontSize (%smm) > headerRowHeight (%smm)",
            data.getHeaderFontSize(),
            data.getHeaderRowHeight());

    // VOUCHER CHECKS

    checkThat(data.getLabelHeight())
        .is(
            atMost(),
            data.getRowHeight(),
            "labelHeight (%smm) > rowHeight (%smm)",
            data.getLabelHeight(),
            data.getRowHeight());

    checkThat(data.getImgSize())
        .is(
            atMost(),
            data.getLabelHeight(),
            "imgSize (%smm) > labelHeight (%smm)",
            data.getImgSize(),
            data.getLabelHeight());

    checkThat(data.getVoucherColumnWidth())
        .is(
            atMost(),
            data.getColumnWidth(),
            "voucherColumnWidth (%smm) > columnWidth (%smm)",
            data.getVoucherColumnWidth(),
            data.getColumnWidth());

    checkThat(data.getVoucherLabelWidth())
        .is(
            atMost(),
            data.getVoucherColumnWidth(),
            "voucherLabelWidth (%smm) > voucherColumnWidth (%smm)",
            data.getVoucherLabelWidth(),
            data.getColumnWidth());

    double voucherContentWidth =
        data.getImgSize() + data.getImgMarginRight() + data.getVoucherTextWidth();

    checkThat(voucherContentWidth)
        .is(
            atMost(),
            data.getVoucherLabelWidth(),
            "imgSize + imgMarginRight + labelTextWidth (%smm) > labelWidth (%smm)",
            voucherContentWidth,
            data.getVoucherLabelWidth());

    // SUBSAMPLE CHECKS

    double subsampleColWidth = data.getColumnWidth() - data.getVoucherColumnWidth();

    checkThat(subsampleColWidth)
        .is(
            atLeast(),
            data.getSubsampleLabelWidth(),
            "subsampleLabelWidth (%smm) > columnWidth - voucherColumnWidth (%smm)",
            data.getSubsampleLabelWidth(),
            subsampleColWidth);

    checkThat(data.getSubsampleLabelWidth())
        .is(
            atLeast(),
            data.getSubsampleTextWidth(),
            "subsampleTextWidth (%smm) > subsampleLabelWidth (%smm)",
            data.getSubsampleTextWidth(),
            data.getSubsampleLabelWidth());
  }
}
