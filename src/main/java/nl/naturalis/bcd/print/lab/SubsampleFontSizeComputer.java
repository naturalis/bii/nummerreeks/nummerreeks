package nl.naturalis.bcd.print.lab;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.bcd.model.NumberSeries;
import nl.naturalis.common.ExceptionMethods;
import static nl.naturalis.bcd.print.LabelUtil.LABEL_FONT_DIGIT_WIDTH;
import static nl.naturalis.bcd.print.LabelUtil.getFontSizeForHeight;
import static nl.naturalis.bcd.print.LabelUtil.getFontSizeForWidth;
import static nl.naturalis.bcd.print.LabelUtil.getRelativeStringWidth;
import static nl.naturalis.common.ObjectMethods.n2e;
import static nl.naturalis.common.StringMethods.EMPTY;
import static nl.naturalis.common.StringMethods.rpad;

public class SubsampleFontSizeComputer {

  private static final Logger LOG = LoggerFactory.getLogger(SubsampleFontSizeComputer.class);

  private LabPrintSession session;

  public SubsampleFontSizeComputer(LabPrintSession session) {
    this.session = session;
  }

  public SubsampleFontSizeProvider getFontSizeProvider() {
    int offset = (int) Math.log10(session.getNumberSeries().getFirstNumber());
    Map<String, double[]> fontSizes = new HashMap<>();
    try {
      fontSizes.put(EMPTY, computeFontSizes(null));
      if (session.getLayoutData().getSuffixes() != null) {
        for (String s : session.getLayoutData().getSuffixes()) {
          fontSizes.put(s, computeFontSizes(s));
        }
      }
      return new SubsampleFontSizeProvider(fontSizes, offset);
    } catch (IOException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  private double[] computeFontSizes(String suffix) throws IOException {

    NumberSeries series = session.getNumberSeries();

    int x = (int) Math.log10(series.getFirstNumber());
    int y = (int) Math.log10(series.getLastNumber());

    double[] fontSizes = new double[y - x + 1];
    for (int i = x; i <= y; ++i) {
      fontSizes[i - x] = computeFontSize(i + 1, suffix);
      if (LOG.isDebugEnabled()) {
        String fmt = "%s[%d digits]%s ";
        String s = String.format(fmt, session.getPrefixWithDot(), i + 1, n2e(suffix));
        s = rpad(s, 32, '.', ": ") + fontSizes[i - x];
        LOG.debug(s);
      }
    }
    return fontSizes;
  }

  private double computeFontSize(int numDigits, String suffix) throws IOException {

    double wPrefix = getRelativeStringWidth(session.getPrefixWithDot());
    double wNumber = numDigits * LABEL_FONT_DIGIT_WIDTH;
    double wSuffix = suffix == null ? 0 : getRelativeStringWidth(suffix);
    double wText = wPrefix + wNumber + wSuffix;

    LabLayoutData layoutData = session.getLayoutData();

    double maxWidth = layoutData.getSubsampleTextWidth();

    double imgSize = layoutData.getImgSize();
    double lineWidth = layoutData.getSubsampleLineWidth();
    double margin = layoutData.getSubsampleTextMarginTB();

    double maxFontSize = (imgSize - (2 * lineWidth) - (2 * margin));
    if (isAllCaps(suffix)) {
      maxFontSize = getFontSizeForHeight(maxFontSize);
    }

    return getFontSizeForWidth(wText, maxWidth, maxFontSize);
  }

  private static boolean isAllCaps(String suffix) {
    if (suffix == null) {
      return true;
    }
    return suffix.codePoints().noneMatch(Character::isLowerCase);
  }
}
