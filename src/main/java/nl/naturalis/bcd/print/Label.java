package nl.naturalis.bcd.print;

public interface Label {

  int PAGE = 0;
  int ROW = 1;
  int COLUMN = 2;

  /**
   * Returns either the page, the row, or the column of the cell that the label is in,
   *
   * @param pageRowColumn One of PAGE, ROW or COLUMN.
   * @return
   */
  int getCellInfo(int pageRowColumn);

  default int getPage() {
    return getCellInfo(PAGE);
  }
}
