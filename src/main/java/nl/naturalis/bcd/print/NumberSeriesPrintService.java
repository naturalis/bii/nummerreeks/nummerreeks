package nl.naturalis.bcd.print;

import java.io.OutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.bcd.model.Layout;
import nl.naturalis.bcd.model.NumberSeries;
import nl.naturalis.common.ExceptionMethods;

public class NumberSeriesPrintService {

  private static final Logger LOG = LoggerFactory.getLogger(NumberSeriesPrintService.class);

  private final NumberSeries series;
  private final Layout layout;

  public NumberSeriesPrintService(NumberSeries series, Layout layout) {
    this.series = series;
    this.layout = layout;
  }

  public void printLabels(OutputStream out) {
    try {
      print(out);
    } catch (Exception e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  private void print(OutputStream out) throws Exception {
    LOG.info("Printing labels for {} using layout {}", series, layout);
    try (PrintSession session = PrintSession.newPrintSession(series, layout)) {
      session.printLabels(out);
    }
  }
}
