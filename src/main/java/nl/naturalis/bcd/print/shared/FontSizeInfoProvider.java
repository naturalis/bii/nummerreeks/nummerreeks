package nl.naturalis.bcd.print.shared;

import java.util.Map;
import static nl.naturalis.common.ObjectMethods.n2e;

public class FontSizeInfoProvider {

  private final Map<String, FontSizeInfo[]> fontSizeInfoPerSuffix;
  private final int offset;

  FontSizeInfoProvider(Map<String, FontSizeInfo[]> fsInfoPerSuffix, int offset) {
    this.fontSizeInfoPerSuffix = Map.copyOf(fsInfoPerSuffix);
    this.offset = offset;
  }

  public FontSizeInfo getFontSizeInfo(int number, String suffix) {
    FontSizeInfo[] info = fontSizeInfoPerSuffix.get(n2e(suffix));
    int x = (int) Math.log10(number);
    return info[x - offset];
  }
}
