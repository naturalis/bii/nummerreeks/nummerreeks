package nl.naturalis.bcd.print.shared;

import nl.naturalis.bcd.print.SplitMode;

public class FontSizeInfo {
  private final double fontSizeLine0;
  private final double fontSizeLine1;
  private final SplitMode splitMode;

  FontSizeInfo(double fontSize, SplitMode splitMode) {
    this.fontSizeLine0 = fontSize;
    this.fontSizeLine1 = Double.NaN;
    this.splitMode = splitMode;
  }

  FontSizeInfo(double fontSize0, double fontSize1, SplitMode splitMode) {
    this.fontSizeLine0 = fontSize0;
    this.fontSizeLine1 = fontSize1;
    this.splitMode = splitMode;
  }

  public double getFontSizeLine0() {
    return fontSizeLine0;
  }

  public double getFontSizeLine1() {
    return fontSizeLine1;
  }

  public SplitMode getSplitMode() {
    return splitMode;
  }

  public String toString() {
    return "{fontSizeLine0: "
        + fontSizeLine0
        + ", fontSizeLine1: "
        + fontSizeLine1
        + ", splitMode: "
        + splitMode
        + "}";
  }
}
