package nl.naturalis.bcd.print;

import java.io.IOException;
import java.io.OutputStream;
import nl.naturalis.bcd.model.Layout;
import nl.naturalis.bcd.model.NumberSeries;
import nl.naturalis.bcd.model.StorageLocation;
import nl.naturalis.bcd.print.lab.LabPrintSession;
import nl.naturalis.bcd.print.std.StdPrintSession;
import nl.naturalis.bcd.print.stdplus.StdPlusPrintSession;
import nl.naturalis.bcd.print.storage.StorageLocationPrintSession;
import nl.naturalis.common.check.Check;

interface PrintSession extends AutoCloseable {

  static PrintSession newPrintSession(NumberSeries series, Layout layout) {
    switch (layout.getTemplate()) {
      case STANDARD:
        return new StdPrintSession(series, layout);
      case LABORATORY:
        return new LabPrintSession(series, layout);
      case STD_PLUS:
        return new StdPlusPrintSession(series, layout);
      default:
        return Check.fail(
            "Printing number series using template %s is not supported", layout.getTemplate());
    }
  }

  static PrintSession newPrintSession(StorageLocation storageLocation, Layout layout) {
    switch (layout.getTemplate()) {
      case STORAGE_LOCATION:
        return new StorageLocationPrintSession(storageLocation, layout);
      default:
        return Check.fail(
            "Printing storage location labels using template %s is not supported",
            layout.getTemplate());
    }
  }

  void printLabels(OutputStream out) throws IOException;
}
