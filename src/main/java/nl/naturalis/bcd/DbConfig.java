package nl.naturalis.bcd;

import java.util.function.Function;
import nl.naturalis.bcd.exception.BcdConfigurationException;
import nl.naturalis.common.check.Check;

import static nl.naturalis.common.check.CommonChecks.*;

public class DbConfig {

  private static final String URL_TEMPLATE = "jdbc:mariadb://%s:%s/%s";

  public String host;
  public String port;
  public String user;
  public String password;
  public String name;
  public int poolSize;

  public String getJdbcUrl() throws BcdConfigurationException {
    Check.on(invalidConfig(), host).isNot(empty(), "Database host not configured");
    Check.on(invalidConfig(), port).is(validPort(), "Database port invalid or not configured");
    Check.on(invalidConfig(), name).isNot(empty(), "Database name not configured");
    return String.format(URL_TEMPLATE, host, port, name);
  }

  private static Function<String, BcdConfigurationException> invalidConfig() {
    return BcdConfigurationException::new;
  }
}
