# Mimics build on gitlab

# Mimic without gitlab cache feature:
rm -rf make/.m2/repository
mkdir make/.m2/repository

echo 'Starting maven build'
mvn clean install -Dcheckstyle.skip --settings make/.m2/settings.xml