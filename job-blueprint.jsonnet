local job(tag) =
{
  image: {
    name: "docker.io/aquasec/trivy:latest",
    entrypoint: [""]
  },
  before_script: [
    "apk add curl",
    "wget --header \"PRIVATE-TOKEN: $TRIVYIGNORE_ACCESS_TOKEN\" \"https://gitlab.com/api/v4/projects/41791412/repository/files/trivyignore/raw?ref=main\" --output-document=.trivyignore",
  ],
  script: [
    "TAG=" + tag,
    "trivy --version",
    "trivy clean --all --cache-dir .trivycache/",
    "trivy image --download-db-only",
    "trivy image --exit-code 0 --no-progress --cache-dir .trivycache/ --timeout 15m --format template --template \"@/contrib/junit.tpl\" --output ${CI_PROJECT_DIR}/junit-report.xml ${CI_REGISTRY_IMAGE}:${TAG}",
    "trivy image --exit-code 0 --no-progress --cache-dir .trivycache/ --timeout 15m ${CI_REGISTRY_IMAGE}:${TAG}",
    "trivy image --exit-code 1 --no-progress --cache-dir .trivycache/ --timeout 15m --severity CRITICAL ${CI_REGISTRY_IMAGE}:${TAG} || exit_code=$?",
    |||
      if [ $exit_code -ne 0 ]; then 
        PIPELINE_URI=${CI_PROJECT_URL}/-/pipelines/${CI_PIPELINE_ID}
        echo -e "Job failed because CRITICAL vulnerabilities have been found!\n"
        MSG="WARNING: docker image \`nummerreeks:${TAG}\` has [CRITICAL vulnerabilities](${PIPELINE_URI})!"
        curl -H "Content-Type: application/json" --data '{ "text": "'"$MSG"'" }' ${MM_WEBHOOK}
      fi;
    |||,
    "exit $exit_code",
  ],
  cache: {
    paths: [
      ".trivycache",
    ]
  },
  artifacts: {
    when: "always",
    paths: [
      "junit-report.xml"
    ],
    reports: {
      junit: [
        "junit-report.xml"
      ]
    }
  }
};
local tags = {{{TAGS}}};
{
  ['tag/' + x]: job(x)
  for x in tags
}
